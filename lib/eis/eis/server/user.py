import logging
from typing import Optional, Dict, cast, TYPE_CHECKING
from uuid import UUID

from eis import EISEntityId
from eis.constants import SERVER
from eis.input import InputMethod
from eis.server.input import RemoteInputMethod
from eis.user import EISUser, PeerUserBase
from satnet.entity import entity

if TYPE_CHECKING:
    # noinspection PyUnresolvedReferences
    from eis.server.session import EISRemoteSession
    # noinspection PyUnresolvedReferences
    from eis.server.editor import ServerEditor

logger = logging.getLogger(__name__)


if SERVER:

    @entity(id=EISEntityId.USER)
    class RemoteEISUser(EISUser['ServerEditor', 'EISRemoteSession', 'RemoteInputMethod']):
        """
        Remote EIS user
        """

        def __init__(self) -> None:
            super().__init__()

        @property
        def session(self) -> 'EISRemoteSession':
            """
            Session that owns this remote user.
            :return: EISRemoteSession
            """
            assert self._session
            return self._session

        @session.setter
        def session(self, value: 'EISRemoteSession') -> None:
            if self._session != value:
                self._session = value

    @entity(id=EISEntityId.PEER_USER)
    class PeerUser(PeerUserBase):
        """
        Server version of a Peer User
        This is only used to create a VO for network transport to the client
        where it will be deserialized as a full featured Peer User.
        """

        def __init__(self, user: Optional[EISUser] = None) -> None:
            super().__init__()

            if user is not None:
                self._uuid = user.uuid
                self._input_methods = {
                    im[0]: im[1] for im in
                    [(input_method.uuid, cast(RemoteInputMethod, input_method).to_peer())
                     for input_method in user.input_methods.values()]
                    if im[1] is not None
                } if user.input_methods else {}
