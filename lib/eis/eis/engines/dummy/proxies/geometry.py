from eis.engine import Engine, proxy
from eis.engines.dummy.engine import DummyEngine
from eis.engines.dummy.proxies.base import DummyGraphProxyBase
from eis.graph.geometry import Geometry, GeometryProxy


@proxy(DummyEngine, Geometry)
class DummyGeometryProxy(GeometryProxy, DummyGraphProxyBase):
    def __init__(self, engine: Engine, proxied: Geometry) -> None:
        super().__init__(engine=engine, proxied=proxied)
