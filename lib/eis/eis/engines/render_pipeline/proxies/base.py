from typing import Generic, Optional, TypeVar

from eis.engines.render_pipeline.engine import RenderPipelineEngine
from panda3d import core

N = TypeVar('N', bound=core.PandaNode)


class RenderPipelineGraphProxyBase(Generic[N]):

    def __init__(self, engine: RenderPipelineEngine) -> None:
        assert isinstance(engine, RenderPipelineEngine)
        self._render_pipeline = engine
        self._node: Optional[N] = None

    @property
    def render_pipeline(self) -> RenderPipelineEngine:
        return self._render_pipeline

    @property
    def node(self) -> Optional[N]:
        return self._node
