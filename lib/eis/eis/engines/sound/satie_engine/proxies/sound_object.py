import logging
from typing import Any, Generic, List, TypeVar

from eis.editor_support.satie_manager import SatieManager
from eis.editor_support.switcher_manager import SwitcherManager
from eis.engine import Engine, proxy
from eis.engines.sound.satie_engine.engine import SatieEngine
from eis.engines.sound.proxies.sound_object import SoundEngineSoundObjectProxy
from eis.graph.sound_object import SoundObject
from satmath.matrix44 import Matrix44
from satmath.vector3 import Vector3

O = TypeVar('O', bound='SatieSoundObject')

logger = logging.getLogger(__name__)


@proxy(SatieEngine, SoundObject)
class SatieEngineSoundObjectProxy(SoundEngineSoundObjectProxy):
    def __init__(self, engine: Engine, proxied: O) -> None:
        assert isinstance(engine, SatieEngine)
        SoundEngineSoundObjectProxy.__init__(self, engine=engine, proxied=proxied)
        self._group = proxied.group
        self._plugin = proxied.plugin
        self._uuid = str(proxied.uuid)
        self._path = proxied.path
        self._audio_handle = None
        self._create_sound(proxied.matrix_world_with_offset)

    def _create_sound(self, matrix: Matrix44) -> None:
        switcher_manager = SwitcherManager()
        self._audio_handle = switcher_manager.new_audio(self._path)

        if self._audio_handle is None or self._plugin is None:
            return

        # register callback to switcher manager
        switcher_manager.link_audio(handle=self._audio_handle)
        SatieManager().new_source(uuid=self._uuid,
                                  plugin=self._plugin,
                                  group=self._group,
                                  matrix=matrix,
                                  audio_handle=self._audio_handle)

    def _remove_sound(self) -> None:
        if self._audio_handle is not None:
            switcher_manager = SwitcherManager()
            switcher_manager.unlink_audio(self._audio_handle)
            switcher_manager.close_audio(self._audio_handle)
            self._audio_handle = None

        SatieManager().remove_source(self._uuid)

    def remove(self) -> None:
        self._remove_sound()
        super().remove()

    def dispose(self) -> None:
        self._remove_sound()
        super().dispose()

    def activate_property(self, name: str, value: Any) -> None:
        super().activate_property(name, value)
        SatieManager().satie.node_set(str(self._uuid), name, value)

    def set_matrix(self, matrix: Matrix44, matrix_offset: Matrix44) -> None:
        super().set_matrix(matrix, matrix_offset)
        SatieManager().set_source_location(self._uuid, matrix)
