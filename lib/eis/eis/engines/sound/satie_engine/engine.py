import logging
from typing import Any, Dict, Optional

from eis.editor_support.satie_manager import SatieManager
from eis.engine import register_engine
from eis.engines.sound.engine import SoundEngine
from eis.engines.sound.object import SoundEngineObject
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3

logger = logging.getLogger(__name__)


@register_engine("Satie")
class SatieEngine(SoundEngine):
    """
    Satie sound engine
    """

    def __init__(self, config: Optional[Dict[str, Any]] = None) -> None:
        super().__init__(config=config)

    def initialize(self) -> None:
        logger.info("Initialized Satie sound engine")

    def _update_position(self, matrix: Matrix44) -> None:
        SatieManager().update_editor_matrix(matrix)

    def get_jack_address(self) -> str:
        return "SuperCollider:in_"
