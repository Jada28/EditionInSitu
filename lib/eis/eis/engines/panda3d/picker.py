import logging
from typing import Dict, List, Optional, TYPE_CHECKING, Tuple
from uuid import UUID

from panda3d.core import CollisionHandlerQueue, CollisionNode, CollisionRay, CollisionTraverser, GeomNode, LVector3f, NodePath  # type: ignore

from eis.constants import DEBUG_PICKER
from eis.graph.material import Material
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from eis.graph.primitives.shape import Shape
from eis.picker import Picker
from satlib.language import fancy_namer
from satmath.matrix44 import Matrix44  # type: ignore
from satmath.quaternion import Quaternion  # type: ignore
from satmath.vector3 import Vector3  # type: ignore
from satmath.vector4 import Vector4  # type: ignore

if TYPE_CHECKING:
    from eis.client.editor import ClientEditor
    from eis.engines.panda3d.engine import Panda3DEngine

logger = logging.getLogger(__name__)


class Panda3DPicker(Picker['Panda3DEngine']):
    """
    Panda3D picker
    """

    def __init__(self, engine: 'Panda3DEngine', editor: 'ClientEditor') -> None:
        super().__init__(engine=engine, editor=editor)
        self._manual_ray = False

        self._collision_handlers: Dict[NodePath, CollisionHandlerQueue] = {}
        self._traversers: Dict[NodePath, CollisionTraverser] = {}

        self._ray = CollisionRay()
        self._ray.set_origin(*self._origin)
        self._ray.set_direction(*self._direction)

        # Create traversers for the scene and for the GUI
        self._setup_traverser(self._engine.base.render)
        self._setup_traverser(self._engine._gui_render)

        # The immersive space still gets its own traverser
        # to be able to correct for the user not being at the center
        self._imm_space_model: Optional[Model] = None
        self._imm_space_model_np: Optional[NodePath] = None
        self._imm_space_ray = CollisionRay()
        self._imm_space_ray.set_origin(LVector3f(0.0, 0.0, 0.0))
        self._imm_space_ray.set_direction(*self._direction)
        self._imm_space_collision_node = CollisionNode(
            "immersive_space_collision_node_{}".format(fancy_namer.generate_name()))
        self._imm_space_collision_node.set_from_collide_mask(GeomNode.get_default_collide_mask())
        self._imm_space_collision_node.add_solid(self._imm_space_ray)
        self._imm_space_collision_node_np: Optional[NodePath] = None
        self._imm_space_collision_handler = CollisionHandlerQueue()
        self._imm_space_traverser = CollisionTraverser()

        self._skip_first = True

        """DEBUG"""
        if DEBUG_PICKER:
            self._ray_shape = Shape(
                points=[(0.0, 0.0, 0.0), (0.0, 1000.0, 0.0)],
                width=2.0,
                material=Material(color=(0.0, 0.0, 1.0, 1.0), shading_model=Material.ShadingModel.EMISSIVE)
            )
            ray_model = Model()
            ray_model.root.add_child(self._ray_shape)
            self._editor.engine.add_model(model=ray_model)

            self._imm_ray_shape = Shape(
                points=[(0.0, 0.0, 0.0), (0.0, 1000.0, 0.0)],
                width=2.0,
                material=Material(color=(0.0, 1.0, 0.0, 1.0), shading_model=Material.ShadingModel.EMISSIVE)
            )
            imm_ray_model = Model()
            imm_ray_model.root.add_child(self._imm_ray_shape)
            self._editor.engine.add_model(model=imm_ray_model)

    def _setup_traverser(self, root_nodepath: NodePath) -> None:
        """
        Setup a new traverser for the given root node.
        This method fills the self._collision_handlers and self._traversers
        dictionnaries, with the root_nodepath as key.

        :param root_nodepath: Root node
        """
        collision_node = CollisionNode("collision_node_{}".format(fancy_namer.generate_name()))
        collision_node.set_from_collide_mask(GeomNode.get_default_collide_mask())
        collision_node.add_solid(self._ray)
        collision_node_np = root_nodepath.attach_new_node(collision_node)

        collision_handler = CollisionHandlerQueue()
        traverser = CollisionTraverser()
        traverser.add_collider(collision_node_np, collision_handler)

        if DEBUG_PICKER:
            collision_node_np.show()
            traverser.showCollisions(root_nodepath)

        self._collision_handlers[root_nodepath] = collision_handler
        self._traversers[root_nodepath] = traverser

    @property
    def manual(self) -> bool:
        return self._manual_ray

    @manual.setter
    def manual(self, manual: bool) -> None:
        self._manual_ray = manual

    @property
    def picker_ray(self) -> CollisionRay:
        return self._ray

    @property
    def imm_space_ray(self) -> CollisionRay:
        return self._imm_space_ray

    @property
    def ray_vector(self) -> Vector3:
        return self._ray_vector.copy()

    def update_rays(self) -> None:
        if self._imm_space_model != self._editor.immersive_space_model:
            # Cleanup
            self._imm_space_model_np = None
            self._imm_space_collision_node_np = None
            self._imm_space_traverser.clear_colliders()

            # Setup the new collision
            if self._editor.immersive_space_model is not None:
                self._imm_space_model = self._editor.immersive_space_model
                self._imm_space_model_np = self._engine.convert_model(self._imm_space_model)

            if self._imm_space_model_np is not None:
                self._imm_space_collision_node_np = self._imm_space_model_np.attach_new_node(
                    self._imm_space_collision_node)
                if DEBUG_PICKER:
                    self._imm_space_collision_node_np.show()
                self._imm_space_traverser.clear_colliders()
                self._imm_space_traverser.add_collider(
                    self._imm_space_collision_node_np, self._imm_space_collision_handler)

        if self._imm_space_model_np is not None:
            if not self._manual_ray:
                self._imm_space_ray.set_origin(LVector3f(*(self._editor.matrix.inverse.mul_vector3(self._origin))))
                self._imm_space_ray.set_direction(
                    LVector3f(*(self._editor.matrix.inverse.mul_vector4(Vector4((*self._direction, 0.0)))).xyz))

            self._imm_space_traverser.traverse(self._imm_space_model_np)
            impacts = self._imm_space_collision_handler.get_entries()

            if impacts:
                impact = impacts[0]
                impact_position = Vector3([*impact.get_surface_point(self._imm_space_model_np)])

                self._ray_vector = impact_position  # We're just using the length for now, so this is ok

                self._origin_corrected = self._editor.location
                self._direction_corrected = self._editor.rotation.mul_vector3(impact_position).normalized

                self._ray.set_origin(LVector3f(*self._origin_corrected))
                self._ray.set_direction(LVector3f(*self._direction_corrected))

                self._cursor_matrix = \
                    Matrix44.from_translation(impact_position) * \
                    Matrix44.from_quaternion(Quaternion.from_vector_track_up(impact_position))

        else:
            self._ray.set_origin(LVector3f(*self._origin))
            self._ray.set_direction(LVector3f(*self._direction))
            self._origin_corrected = self._origin
            self._direction_corrected = self._direction

    def _pick(self, object3d: Optional[Object3D] = None) -> List[Tuple[UUID, Vector3]]:
        if object3d is self._editor.gui_layer:
            root_nodepath = self._engine._gui_render
        else:
            root_nodepath = self._engine.base.render

        if object3d is not None:
            nodepath = root_nodepath.find("**/=uuid={}".format(str(object3d.uuid)))
        else:
            nodepath = root_nodepath

        if nodepath.nodes:
            self._traversers[root_nodepath].traverse(nodepath)
        else:
            return []

        if DEBUG_PICKER:
            # Update 3D Models
            if self._ray_shape:
                self._ray_shape.points = [
                    self._engine.eis_vector(self._ray.get_origin()),
                    self._engine.eis_vector(self._ray.get_origin()) + self._engine.eis_vector(self._ray.get_direction())
                ]

            if self._imm_ray_shape:
                mat = self._engine.engine_matrix(self._editor.matrix)
                self._imm_ray_shape.points = [
                    self._engine.eis_vector(mat.xform_point(self._imm_space_ray.get_origin())),
                    self._engine.eis_vector(mat.xform_point(self._imm_space_ray.get_direction()))
                ]

        self._collision_handlers[root_nodepath].sort_entries()
        impacts = self._collision_handlers[root_nodepath].get_entries()

        return [
            (
                UUID(impact.get_into_node_path().get_tag("uuid")),
                Vector3(impact.get_surface_point(root_nodepath))
            )
            for impact in impacts
        ]
