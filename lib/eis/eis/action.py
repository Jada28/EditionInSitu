from enum import IntEnum, unique
from typing import TYPE_CHECKING

from satnet.action import Action

if TYPE_CHECKING:
    # noinspection PyUnresolvedReferences
    from eis.server.session import EISRemoteSession


@unique
class ActionId(IntEnum):
    """
    EIS Action Ids
    """
    TRANSACTION = 0x01  # RESERVED
    LOAD_MODEL = 0x02

    CHANGE_PROPERTY = 0x20
    ADD_MODEL = 0x21
    ADD_OBJECT3D = 0x22
    REMOVE_OBJECT3D = 0x23
    ADD_BEHAVIOR = 0x24
    REMOVE_BEHAVIOR = 0x25
    ADD_MATERIAL = 0x26
    REMOVE_MATERIAL = 0x27
    ADD_TEXTURE = 0x28
    REMOVE_TEXTURE = 0x29
    ADD_MESH = 0x2A
    REMOVE_MESH = 0x2B

    TRANSFORM_OBJECT = 0x40
    SET_MATERIAL = 0x41

    ADD_KEYFRAME = 0x50
    REMOVE_KEYFRAME = 0x51
    LOOP_ANIMATION = 0x52


class EISAction(Action['EISRemoteSession']):
    """ Helper wrapper to save on imports and generic type declarations in concrete eis actions """
    pass
