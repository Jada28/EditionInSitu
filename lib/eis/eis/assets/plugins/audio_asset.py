import math
import matplotlib.pyplot as plt
import os
import pysndfile

from typing import Any, Optional
from PIL import Image

from eis.assets.file_asset import FileAsset
from eis.graph.material import Material, MaterialTexture
from eis.graph.object_3d import Object3D
from eis.graph.sound_objects.file_sound_object import FileSoundObject
from eis.graph.texture import Texture
from satmath.euler import Euler
from satmath.matrix44 import Matrix44

import io


class AudioAsset(FileAsset):
    def __init__(self, data: Any, path: str) -> None:
        super().__init__(data=data, path=path)
        self._sound_object: Optional['SatieSwitcherObject'] = None

    @classmethod
    def create_asset(cls, file: str) -> Optional['AudioAsset']:
        if type(file) is not str:
            return None
        _, file_extension = os.path.splitext(file.lower())
        if file_extension in [".wav"]:
            return cls(data=None, path=file)
        return None

    def _make_asset_object(self, scale: float, shape: Object3D) -> Object3D:
        super()._make_asset_object(scale=scale, shape=shape)
        # Make an image with the waveform of the sound file
        data = pysndfile.sndio.read(self._path)
        framerate = pysndfile.sndio.get_info(self._path)[0]
        channels = data[0][0].size
        self._width = 640
        self._height = 480 * channels
        image = Image.new('RGB', (self._width, self._height))
        color = 'black'
        linewidth = 0.4

        plt.figure(1)

        for i in range(0, channels):
            plt.axis('off')
            buf = io.BytesIO()
            if channels == 1:
                mono_data = data[0][:framerate]  # Trace the waveform for only the first second of the file
            else:
                mono_data = data[0][:, i][:framerate]
            outline = plt.plot(mono_data, color=color)
            plt.setp(outline, linewidth=linewidth)
            plt.savefig(buf, format='png', transparent=False)
            image.paste(Image.open(buf).rotate(180), (0, 480 * i))
            plt.clf()

        plt.close()

        # Create 3d object that will represent the sound
        texture = Texture(
            name=self._path,
            data=image.convert('RGBA').tobytes(),
            format='rgba',
            size_x=self._width,
            size_y=self._height,
            component_type=Texture.ComponentType.UNSIGNED_BYTE,
            compression=Texture.Compression.NONE
        )

        material = Material(
            color=(1, 1, 1, 1.0),
            refraction=0.0,
            roughness=1.0,
            metallic=0.0,
            material_textures=[MaterialTexture(texture=texture)]
        )

        shape.name = self._path
        shape.material = material

        shape.interactive = True
        shape.matrix = Matrix44.from_euler(Euler((-math.pi / 2.0, 0.0, 0.0)))

        shape.scale *= scale

        self._sound_object = FileSoundObject(name=self._path, path=self._path)

        shape.add_child(self._sound_object)

        return shape

    def _instantiate_behaviors(self) -> None:
        super()._instantiate_behaviors()

        # self._sound_object will be none if the object is created from the cache
        if self._sound_object is None:
            assert(self._object3D)
            for child in self._object3D.children:
                if type(child) == FileSoundObject:
                    self._sound_object = child
                    self._sound_object.set_property(name="gate", value=0)

    def on_hovered(self) -> None:
        super().on_hovered()
        if self._sound_object is not None:
            self._sound_object.set_property(name="gate", value=1)

    def on_unhovered(self) -> None:
        super().on_unhovered()
        if self._sound_object is not None:
            self._sound_object.set_property(name="gate", value=0)

    def object_3d_copy(self) -> Object3D:
        assert(self._object3D)
        return self._object3D.copy()
