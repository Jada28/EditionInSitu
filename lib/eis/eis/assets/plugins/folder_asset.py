import math
import os

from typing import Any, Optional

from eis.assets.file_asset import FileAsset
from eis.graph.material import Material, MaterialTexture
from eis.graph.object_3d import Object3D
from eis.graph.primitives.box import Box
from eis.graph.text import Text
from eis.graph.texture import Texture
from eis.utils.color import Colors
from satmath.euler import Euler
from satmath.matrix44 import Matrix44


class FolderAsset(FileAsset):
    def __init__(self, data: Any, path: str) -> None:
        super().__init__(data=data, path=path)

    @classmethod
    def create_asset(cls, file: str) -> Optional['FolderAsset']:

        if type(file) is not str:
            return None

        # Check if the name of the file is a subfolder
        if os.path.isdir(file):
            return cls(None, file)
        return None

    def _make_asset_object(self, scale: float, shape: Object3D) -> Object3D:
        super()._make_asset_object(scale=scale, shape=shape)
        # Define the text and scale used for the folder name
        path = os.path.basename(self._path)
        text_scale = 0.12 * scale
        if path == "..":
            path = "←"
            text_scale = 0.4 * scale
        # If the folder name is too long, lower the scale accordingly (to fit in the square)
        elif len(path) > 12:
            text_scale = 1.44 * scale / len(path)

        folder_name = Text(
            text=path,
            text_color=Colors.white,
            text_scale=text_scale,
            font="SourceSansPro-Bold.ttf",
            align=Text.Align.CENTER,
            shadow_x=0.0,
            shadow_y=0.0
        )
        folder_name.managed = True

        # Rotate the folder text to face the user
        text_rotation_matrix = Matrix44.from_euler(Euler((math.pi / 2.0, 0.0, 0.0)))

        # Center the text vertically and place it slightly in front of the plane
        text_rotation_matrix[3][1] = text_scale / 4.5
        # 0.5 so that it isn't in the cube, and an additional 0.01 for the text to be in front of the cube
        text_rotation_matrix[3][2] = -0.51 * scale

        folder_name.matrix = text_rotation_matrix

        # Simple black rectangle
        texture = Texture()
        material = Material(
            name=self._path + "_material",
            material_textures=[MaterialTexture(texture=texture)],
            color=(0.0, 0.0, 0.0, 1.0)
        )

        object3D = Box(name=self._path, matrix=Matrix44.identity(), material=material)
        object3D.add_child(folder_name)

        object3D.interactive = True
        object3D.matrix = Matrix44.from_euler(Euler((-math.pi / 2.0, 0.0, 0.0)))

        object3D.depth *= scale
        object3D.width *= scale
        object3D.height *= scale

        return object3D
