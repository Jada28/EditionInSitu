import logging
from typing import Any, Dict, Optional, List, TYPE_CHECKING
from uuid import UUID

from eis import EISEntityId
from eis.constants import CLIENT
from eis.entity import Sync
from eis.graph.animation_curve import AnimationCurve
from eis.graph.behavior import Behavior, behavior
from eis.graph.scene import Scene

if TYPE_CHECKING:
    from eis.graph.base import GraphBase
    from eis.graph.model import Model

logger = logging.getLogger(__name__)


@behavior(id=EISEntityId.BEHAVIOR_ANIMATION)
class AnimationBehavior(Behavior):

    """
    Behavior which records keyframes and can replay them at will
    """

    def curves_id_changed(self, sync: Sync, previous_value: Dict[str, UUID], value: Dict[str, UUID]) -> None:
        self._curves.clear()
        self._curves_id = value
        self._update_curves()

    def looping_changed(self, sync: Sync, previous_value: bool, value: bool) -> None:
        self._looping = value

    curves_id = Sync[Dict[str, UUID]]('_curves_id', {}, on_changed=curves_id_changed)
    looping = Sync[bool]('_looping', False, on_changed=looping_changed)

    def __init__(
        self,
        curves: Optional[Dict[str, AnimationCurve]] = None,
        curves_id: Optional[Dict[str, UUID]] = None
    ) -> None:
        super().__init__()
        self._curves = curves or {}
        self._curves_id = curves_id or {}
        # uuids of _curves have priority over _curves_id
        # this will overwrite the uuid provided by the argument curves_id
        for attribute, curve in self._curves.items():
            self._curves_id[attribute] = curve.uuid
        self._looping = False
        self._writing = False

    def initialize(self):
        self._previous_timeline_position = -1.0
        self._keyframe_times: List[float] = []
        self._last_curve_timestamp = 0.0
        self._selected_keyframe: Optional[float] = None
        self._delay_before_loop: Optional[float] = None

    @property
    def curves(self) -> Dict[str, AnimationCurve]:
        return self._curves

    @property
    def last_curve_timestamp(self) -> float:
        return self._keyframe_times[-1] if self._keyframe_times else 0.0

    @property
    def keyframe_times(self) -> List[float]:
        return self._keyframe_times

    @property
    def selected_keyframe(self) -> Optional[float]:
        return self._selected_keyframe

    def _update_curves(self) -> None:
        if not self._owners:
            return

        curves_to_update = {attribute: curve for (attribute, curve) in self._curves_id.items() if (
            attribute not in self._curves or self._curves[attribute] is None)}
        for owner in self._owners:
            for attribute, curve_id in curves_to_update.items():
                curve = owner.get_animation_curve_by_uuid(curve_id)
                if curve is None:
                    logger.warning("Could not retrieve animation curve id \"{}\"".format(curve_id))
                else:
                    curve.on_sync_changed = self.on_sync_changed
                    self._curves[attribute] = curve

    def _update_keyframe_times(self):
        """
        Check if a keyframe was modified by checking its timestamp.
        If modified, update the keyframe times list
        """
        update_times = False
        # Check if the _last_curve_timestamp has changed, and if so, update times
        for curve in self._curves.values():
            if curve is not None:
                if curve.timestamp > self._last_curve_timestamp:
                    update_times = True
                    self._last_curve_timestamp = curve.timestamp

        if update_times:
            self._keyframe_times = self._get_keyframe_times()

    def _update_selected_keyframe(self, timeline_position: float):
        # Check for keyframe selection
        closest_time = None
        for time in self._keyframe_times:
            time_gap = abs(timeline_position - time)
            if time_gap < 1.0:
                if closest_time is None or abs(timeline_position - closest_time) > time_gap:
                    closest_time = time

        self._selected_keyframe = closest_time

    def can_control(self, graphbase: 'GraphBase') -> bool:
        from eis.graph.object_3d import Object3D
        from eis.graph.texture import Texture
        if isinstance(graphbase, Object3D) \
                or isinstance(graphbase, Texture):
            return True
        return False

    def step(self, now: float, dt: float) -> None:
        scene: Optional[Scene] = None
        if self._scene is not None and self._scene.editor is not None:
            scene = self._scene
        elif self._graphbase is not None:
            for owner in self._graphbase.owners.keys():
                if owner.root.scene and owner.root.scene.editor is not None:
                    scene = owner.root.scene

        if not CLIENT or self._graphbase is None or scene is None:
            return

        self._update_curves()

        if self._delay_before_loop is None:
            self._delay_before_loop = scene.editor.config.get('timeline.delay_before_loop')

        graphbase = self._graphbase

        timeline = scene.editor.timeline
        timeline_position = timeline.time

        self._update_keyframe_times()
        self._update_selected_keyframe(timeline_position=timeline_position)

        if not timeline.running and self._previous_timeline_position == timeline_position:
            return

        if self._writing:
            return

        self._previous_timeline_position = timeline_position

        for attribute, curve in self._curves.items():
            if curve is None:
                continue
            if not hasattr(graphbase, attribute) or curve.empty:
                continue
            curve.looping = self._looping
            curve.loop_delay = self._delay_before_loop
            current_value = curve.get_value_at(time=timeline_position)

            if current_value is not None:
                setattr(graphbase, attribute, current_value)

    def add_keyframe(self, attribute: str) -> bool:
        """
        Add a new keyframes to the object given the object's attribute
        :param attribute: str - Attribute
        :return: bool - Return True if the keyframe has been added successfully
        """
        if self._graphbase is None or self._scene is None or self._scene.editor is None:
            return False

        value = getattr(self._graphbase, attribute, None)
        if value is None:
            return False
        time = self._scene.editor.timeline.time

        self.add_timed_keyframe_with_value(attribute=attribute, time=time, value=value)
        return True

    def add_timed_keyframe_with_value(self, attribute: str, time: float, value: Any) -> None:
        """
        Add a new keyframe given an attribute, time and value
        :param attribute: str - Attribute name
        :param time: float - Keyframe time
        :param value: Any - Keyframe value
        """

        if attribute not in self._curves:
            self._curves[attribute] = AnimationCurve(interpolation=AnimationCurve.InterpolationType.LINEAR)
            self._curves[attribute].on_sync_changed = self.on_sync_changed
            for owner in self._owners:
                owner.add_animation_curve(self._curves[attribute])

            curves_id = self._curves_id
            curves_id[attribute] = self._curves[attribute].uuid
            self.curves_id = curves_id

        self._curves[attribute].add_keyframe(time=time, value=value)

    def set_interpolation_for_attribute(self, attribute: str, interpolation_type: AnimationCurve.InterpolationType):
        """
        Set the interpolation type for the given attribute
        :param attribute: str - Attribute name
        :param interpolation_type: InterpolationType - Interpolation type
        """
        if attribute in self._curves:
            self._curves[attribute].interpolation = interpolation_type

    def remove_keyframe(self, attribute: str, time: float) -> bool:
        """
        Remove a keyframe given an attribute and a time
        :param attribute: str - Attribute namew
        :param time: float - Keyframe time
        :return: bool - Return True if the attribute has an animation curve
        """
        if attribute not in self._curves:
            return False

        success = self._curves[attribute].remove_keyframe(time)
        return success

    def get_keyframe_for_all_attributes(self, time: float) -> Dict[str, Any]:
        """
        Get the value of the keyframes given a time for all the curves of the object
        :param time: float - Keyframe time
        :return Dict[str, Any] - Dict of the attribute (the curves name) with the value of the keyframe at time t
        """
        keyframes: Dict[str, Any] = {}
        for attribute, curve in self._curves.items():
            if curve is not None:
                keyframe = curve.get_keyframe_at(time)
                if keyframe is not None:
                    keyframes[attribute] = keyframe[1]
        return keyframes

    def remove_keyframe_for_all_attributes(self, time: float) -> None:
        """
        Remove a keyframe given a time for all the curves of the object
        :param time: float - Keyframe time
        """
        for curve in self._curves.values():
            if curve is not None:
                curve.remove_keyframe(time)

    def _get_keyframe_times(self) -> List[float]:
        times: List[float] = []
        for curve in self._curves.values():
            if curve is not None:
                for keyframe in curve.keyframes:  # type: ignore
                    if keyframe[0] not in times:
                        times.append(keyframe[0])
        times.sort()
        return times

    def added_to_model(self, model: 'Model') -> None:
        super().added_to_model(model=model)

        for curve in self._curves.values():
            curve.added_to_model(model)

        self._update_curves()

    def removed_from_model(self, model: 'Model') -> None:
        super().removed_from_model(model=model)

        for curve in self._curves.values():
            curve.removed_from_model(model)

    def add_owner(self, owner: 'Model') -> bool:
        if super().add_owner(owner=owner):
            for curve in [curve for curve in self._curves.values() if curve is not None]:
                owner.add_animation_curve(curve)
            self._update_curves()
            return True
        return False

    def remove_owner(self, owner: 'Model') -> bool:
        if super().remove_owner(owner=owner):
            for curve in self._curves.values():
                owner.remove_animation_curve(curve)
            return True
        return False

    @property
    def writing(self):
        return self._writing

    @writing.setter
    def writing(self, value: bool) -> None:
        if self._writing != value:
            self._writing = value
