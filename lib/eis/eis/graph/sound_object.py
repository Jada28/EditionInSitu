from typing import Optional, List, Any, Callable, Dict, Generic, Type, TypeVar
from abc import abstractmethod
from eis import EISEntityId
from eis.graph.base import GraphProxyBase
from eis.constants import CLIENT
from eis.graph.object_3d import Object3D
from satnet.entity import entity

from satmath.matrix44 import Matrix44  # type: ignore
from satmath.vector3 import Vector3  # type: ignore

O = TypeVar('O', bound='SoundObject[Any]')

# Proxy for the soundEngine specifically
class SoundObjectProxy(Generic[O], GraphProxyBase[O]):

    def __init__(self, engine: 'Engine', proxied: O) -> None:
        super().__init__(engine=engine, proxied=proxied)

    @property
    @abstractmethod
    def bound_box(self) -> List[Vector3]:
        raise NotImplementedError

    @abstractmethod
    def add_child(self, child: O) -> None:
        pass

    @abstractmethod
    def gui(self, gui: bool) -> None:
        pass

    @abstractmethod
    def remove_child(self, child: O) -> None:
        pass

    @abstractmethod
    def remove(self) -> None:
        pass

    @abstractmethod
    def dispose(self) -> None:
        pass

    @abstractmethod
    def set_matrix(self, matrix: Matrix44, matrix_offset: Matrix44) -> None:
        pass

    def activate_property(self, name: str, value: Any) -> None:
        pass

P = TypeVar('P', bound=SoundObjectProxy[Any])

@entity(id=EISEntityId.SOUND_OBJECT)
class SoundObject(Object3D):

    _fields = ['_properties', '_step_callbacks']

    def __init__(
            self,
            name: Optional[str] = None,
            properties: Optional[Dict[str, Any]] = None,  # Currently only for Satie, though vaRays might use them eventually
            step_callbacks: Optional[List[Callable]] = None,
            group: Optional[str] = "default",  # For satie, vaRays doesn't have groups
            plugin: Optional[str] = "MonoIn",  # For satie, vaRays doesn't use plugins
            *args,
            **kwargs: Any
    ) -> None:
        super().__init__(
            name=name,
            *args,
            **kwargs)

        self._step_callbacks: List[Callable] = step_callbacks
        self._group: Optional[str] = group
        self._plugin: Optional[str] =  plugin
        self._properties: Dict[str, Any] = properties
        self._sound_proxy: Optional[GraphProxyBase[Any]] = None

    @property
    def group(self) -> Optional[str]:
        return self._group

    @property
    def plugin(self) -> Optional[str]:
        return self._plugin

    def initialize(self) -> None:
        """
        Initializes the object and its hierarchy
        Needs to be called with constructing or after deserializing

        :return: None
        """
        super().initialize()
        self._last_matrix_world_with_offset = self.matrix_world_with_offset

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)
        position_changed = False
        if self._sound_proxy is not None:
            if self._last_matrix_world_with_offset != self.matrix_world_with_offset:
                self._last_matrix_world_with_offset = self.matrix_world_with_offset
                self._update_sound_spatialization()

        if self._step_callbacks is not None:
            [fun() for fun in self._step_callbacks]

    def _update_sound_spatialization(self) -> None:
        self._sound_proxy.set_matrix(self.matrix_world_with_offset, Matrix44.identity())

    def _instantiate_sound(self) -> None:
        """
        Instantiates and enables the playback of the sound
        Should be extended in every child
        The super() should be called at the END of the redifiniton in order to activate the properties on an activated sound

        :return: None
        """
        if self._properties is not None:
            for name, value in self._properties.items():
                self._activate_property(name, value)

        if not self._sound_proxy:
            self.update_sound_proxy(engine=self._scene.editor.machine.sound_engine)

    def _remove_sound(self) -> None:
        """
        Disables the playback of the sound
        Should be extended in every child

        :return: None
        """
        if self._sound_proxy is not None:
            self._sound_proxy.dispose()
            self._sound_proxy = None

    def added_to_editor(self) -> None:
        super().added_to_editor()
        if CLIENT:
            if self._sound_proxy is None:
                self._instantiate_sound()
            if self._sound_proxy is not None:
                self._update_sound_spatialization()

    def removed_from_editor(self) -> None:
        super().removed_from_editor()
        if CLIENT:
            self._remove_sound()

    def set_property(self, name: str, value: Any) -> None:
        """
        Sets a property of the sound
        These properties can include volume, panning, sound effects and others

        :return: None
        """
        if self._properties is not None and name in self._properties and self._properties[name] == value:
            return
        if self._properties is None:
            self._properties = {}
        self._properties[name] = value
        if self._sound_proxy is not None:
            self._activate_property(name, value)

    def _activate_property(self, name: str, value: Any) -> None:
        if self._sound_proxy is not None:
            self._sound_proxy.activate_property(name, value)

    def get_property(self, name: str) -> Any:
        if self._properties is not None:
            return self._properties[name]

    @abstractmethod
    def update_sound_proxy(self, engine: 'Engine') -> None:
        raise NotImplementedError

    def add_to_step_callbacks(self, func: Callable) -> None:
        if self._step_callbacks is None:
            self._step_callbacks = []
        self._step_callbacks.append(func)

    def remove_from_step_callbacks(self, func: Callable) -> None:
        if self._step_callbacks is not None and func in self._step_callbacks:
            self._step_callbacks.remove(func)

    def _copy(self, graph_type: Type['SoundObject'], *args: Any, **kwargs: Any) -> 'SoundObject':
        return super()._copy(
            graph_type,
            *args,
            step_callbacks=self._step_callbacks,
            properties=self._properties,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['SoundObject'], *args: Any, **kwargs: Any) -> 'SoundObject':
        return super()._copy_shared(
            graph_type,
            *args,
            step_callbacks=self._step_callbacks,
            properties=self._properties,
            **kwargs
        )
