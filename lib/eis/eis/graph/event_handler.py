import logging
from abc import abstractmethod
from enum import IntEnum, unique
from typing import Any, Callable, Dict, List, Optional, Tuple, Type, TypeVar

from eis.entity import SyncEntity
from eis.graph.behavior import Behavior
from satnet.entity import entity
from satnet.serialization import Serializable

logger = logging.getLogger(__name__)


def event_handler(id: int, fields: Optional[List[str]] = None) -> Callable:
    """
    Decorator for registering the event_handler
    :param id: int - Id of the event_handler
    :param fields: Optional[List[str]] - Fields to serialize
    """

    def deco(cls: Type['EventHandler']) -> Callable:
        EventHandler.register(cls)
        return entity(id, fields)(cls)

    return deco


class EventHandler(SyncEntity):
    event_handlers = {}  # type: Dict[str, Type['EventHandler']]

    _fields = ['transition_behaviors', '_trigger_behavior',
               '_trigger_kwargs', '_untrigger_behavior', '_untrigger_kwargs']

    @unique
    class Transition(IntEnum):
        TRIGGER = 1
        UNTRIGGER = 2

    def __init__(self,
                 parent: Optional[Behavior] = None,
                 trigger_behavior: Optional[str] = None,
                 trigger_kwargs: Optional[Dict[str, Any]] = None,
                 untrigger_behavior: Optional[str] = None,
                 untrigger_kwargs: Optional[Dict[str, Any]] = None) -> None:
        super().__init__()
        self._parent = parent
        self._trigger_behavior = trigger_behavior
        self._untrigger_behavior = untrigger_behavior
        self._trigger_kwargs = trigger_kwargs
        self._untrigger_kwargs = untrigger_kwargs
        self._transition_behaviors = {
            EventHandler.Transition.TRIGGER: (trigger_behavior, trigger_kwargs or {}),
            EventHandler.Transition.UNTRIGGER: (untrigger_behavior, untrigger_kwargs or {})
        }
        self._triggered = False

    def __del__(self):
        self.remove_behavior(EventHandler.Transition.TRIGGER)
        self.remove_behavior(EventHandler.Transition.UNTRIGGER)

    @classmethod
    def register(cls, event_handler_type: Type['EventHandler']) -> None:
        """
        Register an EventHandler class
        :param event_handler_type: Type[EventHandler] - EventHandler class to register
        :return: None
        """
        if event_handler_type.__name__ not in cls.event_handlers:
            cls.event_handlers[event_handler_type.__name__] = event_handler_type
        else:
            raise Exception("An event handler typed \"{}\" has already been registered".format(event_handler_type))

    def initialize(self) -> None:
        super().initialize()
        self._triggered = False

    @property
    def parent(self) -> Optional[Behavior]:
        return self._parent

    @parent.setter
    def parent(self, value: Behavior) -> None:
        self._parent = value

    @property
    def transition_behaviors(self) -> Dict['EventHandler.Transition', Tuple[Optional[str], Dict[str, Any]]]:
        return self._transition_behaviors

    @transition_behaviors.setter
    def transition_behaviors(self, value: Dict['EventHandler.Transition', Tuple[Optional[str], Dict[str, Any]]]) -> None:
        self._transition_behaviors = value

    def add_behavior(self, transition: Transition) -> None:
        if not self._parent or not self._parent.object3d:
            return

        on_transition = self._transition_behaviors.get(transition)
        if not on_transition or not on_transition[0]:
            return

        assert(on_transition[0] in Behavior.behaviors)
        behavior_type = Behavior.behaviors.get(on_transition[0])
        if behavior_type is None:
            return

        kwargs = on_transition[1] if len(on_transition) == 2 else {}
        # mypy wrongfully considers that a Behavior has no args, except derived Behavior types can
        self._parent.object3d.add_behavior(behavior_type(**kwargs))  # type: ignore

    def remove_behavior(self, transition: Transition) -> None:
        if not self._parent or not self._parent.object3d:
            return

        behavior_type: Optional[Type[Behavior]] = None
        if transition == EventHandler.Transition.TRIGGER:
            if self._trigger_behavior:
                behavior_type = Behavior.behaviors.get(self._trigger_behavior)
        elif transition == EventHandler.Transition.UNTRIGGER:
            if self._untrigger_behavior:
                behavior_type = Behavior.behaviors.get(self._untrigger_behavior)

        if behavior_type is not None:
            self._parent.object3d.remove_behavior_by_type(behavior_type)

    def on_triggered(self) -> None:
        self._triggered = True
        self.remove_behavior(EventHandler.Transition.UNTRIGGER)
        self.add_behavior(EventHandler.Transition.TRIGGER)

    def on_untriggered(self) -> None:
        self._triggered = False
        self.remove_behavior(EventHandler.Transition.TRIGGER)
        self.add_behavior(EventHandler.Transition.UNTRIGGER)

    @abstractmethod
    def step(self, now: float, dt: float) -> None:
        """
        Step
        :param now: float
        :param dt: float
        :return: None
        """
