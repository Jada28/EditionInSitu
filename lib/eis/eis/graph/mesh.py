import numpy
from enum import Enum, unique
from typing import Any, List, Optional, Tuple, TYPE_CHECKING, Type

from eis import EISEntityId
from eis.entity import Sync
from eis.graph.base import GraphProxyBase, SharedGraphEntity
from eis.graph.geometry import Geometry
from satmath.vector3 import Vector3
from satnet.entity import entity

if TYPE_CHECKING:
    from eis.graph.model import Model
    from eis.graph.object_3d import Object3D


class MeshProxy(GraphProxyBase['Mesh']):
    pass


@entity(id=EISEntityId.MESH)
class Mesh(SharedGraphEntity[MeshProxy]):

    @unique
    class Culling(str, Enum):
        DEFAULT = "default"
        NONE = "none"
        FRONT = "front"
        BACK = "back"

    _fields = ['_geometries']

    culling = Sync[Culling]('_culling', Culling.DEFAULT, on_changed=SharedGraphEntity.sync_invalidate)

    def __init__(
            self,
            name: Optional[str] = None,
            geometries: Optional[List[Geometry]] = None,
            culling: Optional[Culling] = None,
    ) -> None:
        super().__init__(name=name)

        self._geometries: List[Geometry] = geometries or []
        self._culling: Mesh.Culling = culling or Mesh.Culling.DEFAULT

    def __str__(self) -> str:
        return "\033[0;33m" + super().__str__()

    def initialize(self):
        super().initialize()

        self._objects3d: List['Object3D'] = []
        for geometry in self._geometries:
            geometry.mesh = self

    def trace(self, level: int = 0) -> str:
        indent = "    " * level
        ret = indent + "{}\n".format(self)
        for geometry in self._geometries:
            ret += geometry.trace(level=level + 1)
        return ret

    @property
    def bound_box(self) -> Tuple[Vector3, Vector3]:
        bbox = (Vector3(), Vector3())
        for geometry in self._geometries:
            geom_bbox = geometry.bound_box
            bbox = (
                numpy.minimum(bbox[0], geom_bbox[0]),
                numpy.maximum(bbox[1], geom_bbox[1])
            )

        return bbox

    @property
    def geometries(self) -> List[Geometry]:
        return self._geometries

    @property
    def objects3d(self) -> List['Object3D']:
        return self._objects3d

    def add_geometry(self, geometry: Geometry) -> None:
        assert geometry
        self._geometries.append(geometry)
        geometry.mesh = self
        for model in self._owners.keys():
            geometry.added_to_model(model)

        # Call added_to_object3d for geoms
        for object3d in self._objects3d:
            geometry.added_to_object3d(object3d)

    def remove_geometry(self, geometry: Geometry) -> None:
        assert geometry
        self._geometries.remove(geometry)
        geometry.mesh = None
        for model in self._owners.keys():
            geometry.removed_from_model(model)

        # Call removed_form_object3d for geoms
        for object3d in self._objects3d:
            geometry.removed_from_object3d(object3d)

    def added_to_object3d(self, object3d: 'Object3D') -> None:
        if object3d not in self._objects3d:
            self._objects3d.append(object3d)

        # Call it for the geom as well
        for geom in self._geometries:
            geom.added_to_object3d(object3d)

    def removed_from_object3d(self, object3d: 'Object3D') -> None:
        if object3d in self._objects3d:
            self._objects3d.remove(object3d)

        # Call it for the geom as well
        for geom in self._geometries:
            geom.removed_from_object3d(object3d)

    def added_to_model(self, model: 'Model') -> None:
        super().added_to_model(model)

        for geometry in self._geometries:
            # Geometry could have been serialized, so that's why we set mesh here also
            geometry.mesh = self
            geometry.added_to_model(model)

        # Register ourselves last so that dependencies are already registered
        model.add_mesh(self)

    def removed_from_model(self, model: 'Model') -> None:
        super().removed_from_model(model)

        model.remove_mesh(self)
        for geometry in self._geometries:
            geometry.removed_from_model(model)

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)

        for geometry in self._geometries:
            geometry.step(now=now, dt=dt)

    def _copy(self, graph_type: Type['Mesh'], *args: Any, **kwargs: Any) -> 'Mesh':
        return super()._copy(
            graph_type,
            *args,
            geometries=[geometry.copy() for geometry in self._geometries if not geometry.managed],
            culling=self._culling,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Mesh'], *args: Any, **kwargs: Any) -> 'Mesh':
        return super()._copy_shared(
            graph_type,
            *args,
            geometries=[geometry.copy_shared() for geometry in self._geometries if not geometry.managed],
            culling=self._culling,
            **kwargs
        )

    ...
