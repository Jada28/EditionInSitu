import logging
from typing import Any, Callable, Dict, List, Optional, Type, TYPE_CHECKING

from eis.graph.base import SharedGraphEntity, UniqueGraphEntity
from satnet.entity import entity

if TYPE_CHECKING:
    from eis.graph.base import GraphBase
    from eis.graph.event_handler import EventHandler
    from eis.graph.model import Model
    from eis.graph.scene import Scene

logger = logging.getLogger(__name__)


def behavior(id: int, fields: Optional[List[str]] = None) -> Callable:
    """
    Decorator for registering the behavior
    :param id: int - Id of the behavior
    :param fields: Optional[List[str]] - Fields to serialize
    """

    def deco(cls: Type['Behavior']) -> Callable:
        Behavior.register(cls)
        return entity(id, fields)(cls)

    return deco


class Behavior(SharedGraphEntity):
    '''
    Behaviors allow for modifying automatically an entity at runtime
    '''

    behaviors: Dict[str, Type['Behavior']] = dict()

    _fields = ['_event_handlers']  # type: ignore

    def __init__(self) -> None:
        super().__init__()
        self._graphbase: Optional['GraphBase'] = None
        self._event_handlers: Dict[str, 'EventHandler'] = {}
        self._scene: Optional['Scene'] = None

    def __str__(self) -> str:
        return "{}\033[0;0m(\033[1;30m{}\033[0;0m)".format(self.__class__.__name__, self.uuid)

    def trace(self, level: int = 0) -> str:
        indent = "    " * level
        ret = indent + "{}\n".format(self)
        return ret

    def initialize(self) -> None:
        super().initialize()
        for _, handler in self._event_handlers.items():
            handler.parent = self

    @classmethod
    def register(cls, behavior_type: Type['Behavior']) -> None:  # type: ignore
        """
        Register a Behavior class
        :param behavior_type: Type[Behavior] - Behavior class to register
        :return: None
        """
        if behavior_type.__name__ not in cls.behaviors:
            cls.behaviors[behavior_type.__name__] = behavior_type
        else:
            raise Exception("A behavior typed \"{}\" has already been registered".format(behavior_type))

    @property
    def graphbase(self) -> Optional['GraphBase']:
        return self._graphbase

    @graphbase.setter
    def graphbase(self, value: Optional['GraphBase']) -> None:
        if self._graphbase != value:
            if self._graphbase:
                self.removed_from_object()
            self._graphbase = value
            self._scene = value.scene if value is not None and hasattr(value, 'scene') else None
            if self._graphbase:
                self.added_to_object()

    @property
    def event_handlers(self) -> Dict[str, 'EventHandler']:
        return self._event_handlers

    @property
    def scene(self) -> Optional['Scene']:
        return self._scene

    @scene.setter
    def scene(self, scene: Optional['Scene']) -> None:
        if scene != self._scene:
            if self._scene is not None:
                self.removed_from_scene()
            self._scene = scene
            if self._scene is not None:
                self.added_to_scene()

    def can_control(self, graphbase: 'GraphBase') -> bool:
        '''
        Check that this behavior can control the given GraphBase derived object.
        This should be overridden in derived class to control support for other
        types than Object3D
        :param graphbase: GraphBased derived object
        :return: True if the behavior can control the object, False otherwise
        '''
        from eis.graph.object_3d import Object3D
        if isinstance(graphbase, Object3D):
            return True
        return False

    def added_to_object(self) -> None:
        '''
        Callback called when the behavior is added to a GraphBase
        :return: None
        '''
        if isinstance(self._graphbase, UniqueGraphEntity):
            if self._graphbase.model is not None:
                self.added_to_model(self._graphbase.model)
        elif isinstance(self._graphbase, SharedGraphEntity):
            for owner in self._graphbase.owners.keys():
                self.added_to_model(owner)

    def removed_from_object(self) -> None:
        '''
        Callback called when the behavior is removed from a GraphBase
        :return: None
        '''
        if isinstance(self._graphbase, UniqueGraphEntity):
            if self._graphbase.model is not None:
                self.removed_from_model(self._graphbase.model)
        elif isinstance(self._graphbase, SharedGraphEntity):
            for owner in self._graphbase.owners.keys():
                self.removed_from_model(owner)

    def added_to_model(self, model: 'Model') -> None:
        super().added_to_model(model=model)
        model.add_behavior(self)

    def removed_from_model(self, model: 'Model') -> None:
        super().removed_from_model(model=model)
        model.remove_behavior(self)

    def added_to_scene(self) -> None:
        pass

    def removed_from_scene(self) -> None:
        pass

    def step(self, now: float, dt: float) -> None:
        """
        Step
        :param now: float
        :param dt: float
        :return: None
        """
        for _, handler in self._event_handlers.items():
            handler.step(now=now, dt=dt)

    def _copy_shared(self, graph_type: Type['Behavior'], *args: Any, **kwargs: Any):  # type: ignore
        return super()._copy_shared(
            graph_type,
            *args,
            **kwargs
        )
