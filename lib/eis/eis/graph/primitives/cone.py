import math
from typing import Any, Optional, Type
from uuid import UUID

from eis import EISEntityId
from eis.graph.material import Material
from eis.graph.primitives import PrimitiveSignature
from eis.graph.primitives.cylinder import Cylinder
from satmath.matrix44 import Matrix44
from satnet.entity import entity


@entity(id=EISEntityId.PRIMITIVE_CONE)
class Cone(Cylinder):
    """
    Inspired by: https://github.com/mrdoob/three.js/blob/dev/src/geometries/ConeGeometry.js
    """

    def __init__(
            self,
            *args: Any,
            name: Optional[str] = None,
            matrix: Optional[Matrix44] = None,
            material: Optional[Material] = None,
            material_id: Optional[UUID] = None,

            radius: float = 0.5,
            radius_top: float = 0.0,
            radius_bottom: float = 0.5,
            height: float = 1.0,
            radial_segments: int = 32,
            height_segments: int = 1,
            open_ended: bool = False,
            theta_start: float = 0.0,
            theta_length: float = 2.0 * math.pi,
            **kwargs
    ) -> None:
        parameters = {
            'radius': radius,
            'height': height,
            'radial_segments': radial_segments,
            'height_segments': height_segments,
            'open_ended': open_ended,
            'theta_start': theta_start,
            'theta_length': theta_length
        }
        kwargs['signature'] = PrimitiveSignature(primitive_type=EISEntityId.PRIMITIVE_CONE, parameters=parameters)

        super().__init__(
            *args,
            name=name,
            matrix=matrix,
            material=material,
            material_id=material_id,

            radius_top=0.0,
            radius_bottom=radius,
            height=height,
            radial_segments=radial_segments,
            height_segments=height_segments,
            open_ended=open_ended,
            theta_start=theta_start,
            theta_length=theta_length,
            **kwargs
        )

    @property
    def radius(self) -> float:
        return self.radius_bottom

    @radius.setter
    def radius(self, value: float) -> None:
        self.radius_bottom = value

    def _copy(self, graph_type: Type['Cone'], *args: Any, **kwargs: Any) -> 'Cone':
        return super()._copy(
            graph_type,
            *args,
            radius=self._radius_bottom,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Cone'], *args: Any, **kwargs: Any) -> 'Cone':
        return super()._copy_shared(
            graph_type,
            *args,
            radius=self._radius_bottom,
            **kwargs
        )
