import logging
from typing import Optional, List, TYPE_CHECKING

from eis.command import CommandId, EISServerCommand
from eis.constants import CLIENT, SERVER
from satnet.command import command

if TYPE_CHECKING:
    from eis.client.session import EISLocalSession
    if CLIENT:
        from eis.client.session import Peer
    elif SERVER:
        from eis.server.session import Peer

logger = logging.getLogger(__name__)


@command(id=CommandId.READY)
class ReadyCommand(EISServerCommand):
    """
    Ready command
    Tells the client to reset its state.
    """

    _fields = ['peers']

    def __init__(self, peers: Optional[List['Peer']] = None) -> None:
        super().__init__()
        self.peers = peers

    def handle(self, session: 'EISLocalSession') -> None:
        logger.debug("Server ready!")
        if self.peers:
            for peer in self.peers:
                session.add_peer(peer)
        session.ready()
