from enum import IntEnum, unique
from typing import Generic, TypeVar, TYPE_CHECKING

from satnet.request import ClientRequest, ClientResponse, ServerRequest, ServerResponse

if TYPE_CHECKING:
    # noinspection PyUnresolvedReferences
    from eis.server.session import EISRemoteSession
    # noinspection PyUnresolvedReferences
    from eis.client.session import EISLocalSession


@unique
class RequestId(IntEnum):
    """
    EIS Request Ids
    """
    SHOW_ME_WHAT_YOU_GOT = 0x01
    PING = 0x02
    OPEN_SCENE = 0x04
    LOAD_SCENE = 0x03
    GET_SCENES = 0x05
    GET_GLTF_SCENES = 0x06
    GET_TIMELINE_POSITION = 0x07


@unique
class ResponseId(IntEnum):
    """
    EIS Response Ids
    """
    GET_SCHWIFTY = 0x01
    PONG = 0x02
    SCENE_OPENED = 0x03
    SCENE_LOADED = 0x04
    GOT_SCENES = 0x05
    GOT_GLTF_SCENES = 0x06
    SET_TIMELINE_POSITION = 0x07


CR = TypeVar('CR', bound='ClientRequest')
SR = TypeVar('SR', bound='ServerRequest')


class EISClientRequest(ClientRequest['EISRemoteSession']):
    """ Helper wrapper to save on imports and generic type declarations in concrete eis server requests """
    pass


class EISClientResponse(Generic[SR], ClientResponse['EISRemoteSession', SR]):
    """ Helper wrapper to save on imports and generic type declarations in concrete eis server responses """
    pass


class EISServerRequest(ServerRequest['EISLocalSession']):
    """ Helper wrapper to save on imports and generic type declarations in concrete eis server requests """
    pass


class EISServerResponse(Generic[CR], ServerResponse['EISLocalSession', CR]):
    """ Helper wrapper to save on imports and generic type declarations in concrete eis server responses """
    pass
