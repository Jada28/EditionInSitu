import math
from typing import Dict, List, Optional, Any

from eis.graph.material import Material
from eis.graph.object_3d import Object3D

from eis.graph.primitives.shapes.arrow import Arrow
from eis.graph.primitives.shape import Shape

from satmath.vector3 import Vector3


class Scale(Shape):
    # Don't sync points, we manage them internally
    Shape.points.enabled = False

    def __init__(
            self,
            *args: Any,
            length: float = 1.0,
            height: float = 1.0,
            spikes: List[tuple] = [],
            **kwargs: Any
    ) -> None:
        super().__init__(*args, **kwargs)

        self._length = length
        self._height = height
        self._spikes = spikes

    def update(self) -> None:
        self.points = [(0, self._height, 0), (0, 0, 0)]
        for spike in self._spikes:
            self.points += [(spike[0], 0, 0), (spike[0], spike[1], 0), (spike[0], 0, 0)]
        self.points += [(self._length, 0, 0), (self._length, self._height, 0)]
        super().update()


class Timeline(Object3D):

    def __init__(self, length: float = 16.00, height: float = 0.5, duration: float = 20.00, looping: bool = False, delay_before_loop: float = 5.0) -> None:

        super().__init__()
        self._length = length
        self._height = height
        self._duration = duration
        self._looping = looping

        self.location = Vector3((0.0, 4.0, 0.0))

        # Draw the time pointer
        self._arrow = Arrow(material=Material())
        self._arrow.arrow_length = 0.5
        self._arrow.arrow_width = 0.2
        self._arrow.head_length_ratio = 1.0
        self._arrow.head_width_ratio = 1.0
        self._arrow.location = Vector3((0.0, self._height / 2 + 0.3, 0.0))
        self._arrow.color = (100.0, 0, 0, 1.0)
        self._arrow.z_rotation = math.pi

        self.add_child(self._arrow)

        self._keyframe_objects: Dict[float, Object3D] = {}

        self._selected_keyframe: Optional[Object3D] = None

        self._last_keyframe_time = 0  # Used for the minor_keyframe, represents the time of the latest keyframe
        self._delay_before_loop = delay_before_loop

        # Time-spikes
        possible_thresholds = [1, 2, 5, 10, 30, 60, 120, 240, 480, 960, 1920]
        possible_subthresholds = [1, 2, 5, 10, 30, 60, 120, 240, 480, 960, 1920]

        big_gap = medium_gap = small_gap = 1

        # Calculate what will be the time gaps between big, medium and small "spikes" on the timeline scale
        previous_threshold = possible_thresholds[0]
        for threshold in possible_thresholds:
            if duration / threshold < 1.5:
                big_gap = previous_threshold
                break
            previous_threshold = threshold

        previous_threshold = possible_subthresholds[0]
        for threshold in possible_subthresholds:
            if big_gap / threshold < 2:
                medium_gap = previous_threshold
                break
            previous_threshold = threshold

        previous_threshold = possible_subthresholds[0]
        for threshold in possible_subthresholds:
            if medium_gap / threshold < 5:
                small_gap = previous_threshold
                break
            previous_threshold = threshold

        i = 0

        f = self._length / self._duration

        spikes: List[tuple] = []

        # Fill the spikes list with the data necessary to draw the scale (in chronological order)
        while(True):
            big_time = big_gap * i
            if big_time < duration:
                if i != 0:
                    spikes.append([big_time * f, self._height * 0.75])
                j = 0
                while(True):
                    medium_time = big_time + j * medium_gap
                    if medium_gap * j < big_gap and medium_time < duration:
                        if j != 0:
                            spikes.append([medium_time * f, self._height * 0.5])
                        k = 0
                        while(True):
                            small_time = medium_time + k * small_gap
                            if small_gap * k < medium_gap and small_time < duration:
                                if k != 0:
                                    spikes.append([small_time * f, self._height * 0.25])
                                k += 1
                            else:
                                break
                        j += 1
                    else:
                        break
                i += 1
            else:
                break

        self._background = Scale(material=Material(), length=self._length, height=self._height, spikes=spikes)
        self._background.color = (0.0, 0.0, 0.0, 1.0)
        self._background.location = Vector3((-self._length / 2, -self._height / 2, 0.0))
        self.add_child(self._background)

        self._recording = False

    def update(self):
        super().update()

    def _draw_keyframe(self, time: float) -> Object3D:
        keyframe = Arrow(material=Material())
        keyframe.arrow_length = 0.5
        keyframe.arrow_width = 0.2
        keyframe.head_length_ratio = 1.0
        keyframe.head_width_ratio = 1.0
        keyframe.location = Vector3((time * self._length / self._duration -
                                     self._length / 2, -self._height / 2 - 0.3, 0.0))
        keyframe.color = (0.8, 0.8, 0.8, 1.0)
        return keyframe

    def _draw_minor_keyframe(self, time: float) -> Object3D:
        keyframe = Arrow(material=Material())
        keyframe.arrow_length = 0.5
        keyframe.arrow_width = 0.2
        keyframe.head_length_ratio = 1.0
        keyframe.head_width_ratio = 1.0
        keyframe.location = Vector3((time * self._length / self._duration, 0.0, 0.0))
        keyframe.color = (0.8, 0.8, 0.8, 0.6)
        return keyframe

    def remove_keyframes(self) -> None:
        for keyframe in self._keyframe_objects:
            self.remove_child(keyframe)
        self._keyframe_objects = {}

    def adjust_keyframes(self, times: List[float]) -> None:
        diff1 = list(set(self._keyframe_objects.keys()) - set(times))
        diff2 = list(set(times) - set(self._keyframe_objects.keys()))
        minlength = min(len(diff1), len(diff2))

        # Move the moving keyframes
        for i in range(minlength):
            self._keyframe_objects[diff2[i]] = self._keyframe_objects[diff1[i]]
            del self._keyframe_objects[diff1[i]]
            self._keyframe_objects[diff2[i]].location = Vector3(
                (diff2[i] - self._length / 2, -self._height / 2 - 0.3, 0.0))
        # Delete removed keyframes
        for i in range(len(diff1) - minlength):
            self.remove_child(self._keyframe_objects[diff1[minlength + i - 1]])
            del self._keyframe_objects[diff1[minlength + i - 1]]

        # Adjust the minor keyframes if the last keyframe has changed
        if times and max(times) != self._last_keyframe_time:
            self._last_keyframe_time = max(times)
            if self._looping:
                # Adjust current minor keyframes
                for time, keyframe in self._keyframe_objects.items():
                    i = 1
                    ended = False
                    for child in keyframe.children.copy():
                        if not ended:
                            new_time = (self._last_keyframe_time + self._delay_before_loop) * i
                            if new_time + time > self._duration:
                                ended = True
                        if ended:
                            child.remove()
                        else:
                            child.location = Vector3((new_time * self._length / self._duration, 0.0, 0.0))
                        i += 1
                    if not ended:
                        while(True):
                            new_time = (self._last_keyframe_time + self._delay_before_loop) * i
                            if new_time + time > self._duration:
                                break
                            minor_keyframe = self._draw_minor_keyframe(new_time)
                            keyframe.add_child(minor_keyframe)
                            i += 1

        # Add the new keyframes
        for i in range(len(diff2) - minlength):
            new_time = diff2[minlength + i - 1]
            new_keyframe = self._draw_keyframe(new_time)
            if self._looping:
                j = 1
                while(True):
                    minor_time = (self._last_keyframe_time + self._delay_before_loop) * j
                    if minor_time + new_time > self._duration:
                        break
                    minor_keyframe = self._draw_minor_keyframe(minor_time)
                    new_keyframe.add_child(minor_keyframe)
                    j += 1
            self.add_child(new_keyframe)
            self._keyframe_objects[diff2[minlength + i - 1]] = new_keyframe

    def loop(self) -> None:
        for time, keyframe in self._keyframe_objects.items():
            i = 1
            while(True):
                new_time = (self._last_keyframe_time + self._delay_before_loop) * i
                if new_time + time > self._duration:
                    break
                minor_keyframe = self._draw_minor_keyframe(new_time)
                keyframe.add_child(minor_keyframe)
                i += 1
        self._looping = True

    def unloop(self) -> None:
        for keyframe in self._keyframe_objects.values():
            for child in keyframe.children.copy():
                child.remove()
        self._looping = False

    def clear(self) -> None:
        if self._keyframe_objects:
            for keyframe in self._keyframe_objects.values():
                self.remove_child(keyframe)
            self._keyframe_objects = {}

    def select_keyframe(self, time: float) -> None:
        keyframe_object = self._keyframe_objects.get(time)
        if keyframe_object is not None:
            keyframe_object.color = (5.0, 0.0, 10.0, 1.0)
            self._selected_keyframe = keyframe_object
            for child in keyframe_object.children:
                child.color = (5.0, 0.0, 10.0, 0.6)

    def unselect_keyframe(self) -> None:
        if self._selected_keyframe is not None:
            self._selected_keyframe.color = (0.8, 0.8, 0.8, 1.0)
            for child in self._selected_keyframe.children:
                child.color = (0.8, 0.8, 0.8, 0.6)
            self._selected_keyframe = None

    def change_cursor_position(self, x: float):
        self._arrow.location = Vector3((x * self._length / self._duration -
                                        self._length / 2, self._height / 2 + 0.3, 0.0))

    def on_opening(self):
        pass

    def on_opened(self):
        pass

    def on_closing(self):
        pass

    def on_closed(self):
        pass

    def recording(self, recording: bool) -> None:
        if recording is not self._recording:
            self._background.color = (0.0, 8.0, 0.0, 1.0) if recording else (0.0, 0.0, 0.0, 1.0)
            self._recording = recording
