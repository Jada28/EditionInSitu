from abc import ABCMeta
from typing import Callable, Optional, TYPE_CHECKING

from eis.graph.object_3d import Object3D

if TYPE_CHECKING:
    from eis.client.editor import ClientEditor
    from eis.client.input import LocalInputMethod
    from eis.client.user import LocalEISUser
    from eis.editor_support.menu_manager import UserMenuData
    # noinspection PyUnresolvedReferences
    from eis.client.states.base_eis_state import BaseEISState
    # noinspection PyUnresolvedReferences
    from eis.editor_support.menu_manager import MenuManager


class Menu(Object3D, metaclass=ABCMeta):

    def __init__(self, name: str = "Menu") -> None:
        super().__init__(name=name)

        self._manager: Optional['MenuManager'] = None
        self._state: Optional['BaseEISState'] = None

        self._just_opened = False
        self._close_callback: Optional[Callable[[], None]] = None
        self._user: Optional['LocalEISUser'] = None

    @property
    def editor(self) -> Optional['ClientEditor']:
        return self._manager.editor if self._manager is not None else None

    @editor.setter
    def editor(self, value: Optional['ClientEditor']) -> None:
        pass

    @property
    def user(self) -> Optional['LocalEISUser']:
        return self._user

    @user.setter
    def user(self, value: Optional['LocalEISUser']) -> None:
        if self._user != value:
            self._user = value

    @property
    def just_opened(self) -> bool:
        return self._just_opened

    @just_opened.setter
    def just_opened(self, just_open: bool) -> None:
        if self._just_opened != just_open:
            self._just_opened = just_open

    @property
    def close_callback(self) -> Optional[Callable[[], None]]:
        return self._close_callback

    @close_callback.setter
    def close_callback(self, value: Optional[Callable[[], None]]) -> None:
        if self._close_callback != value:
            self._close_callback = value

    def on_quick_action(self, input: 'LocalInputMethod') -> None:
        self._just_opened = False

    def on_opening(self, input: 'LocalInputMethod') -> None:
        pass

    def on_opened(self, input: 'LocalInputMethod') -> None:
        pass

    def on_closing(self, user_menu_data: 'UserMenuData', callback: Callable[['UserMenuData'], None]) -> None:
        callback(user_menu_data)

    def on_closed(self) -> None:
        pass

    def update_menu(self) -> None:
        pass

    def close(self):
        if self._manager is not None:
            self._manager.close_menu()

    def update_menu_components(self) -> None:
        pass

    def scale(self, scale: float) -> None:
        for child in self._children:
            child.scale = ((scale, scale, 1.0))
