from typing import Any, Callable, Dict, Optional

from eis.client.input import LocalInputMethod
from eis.display.components.menus.menu import Menu
from eis.display.components.menus.radial.radial_button import RadialButton
from eis.graph.object_3d import Object3D
from eis.editor_support.menu_manager import UserMenuData


class RadialButtonPosition():
    __slots__ = 'index', 'section'

    def __init__(self, index: int, section: int) -> None:
        self.index = index
        self.section = section


class RadialMenu(Menu):
    """
    A radial menu, which can be divided in equally-size sections
    Each section can have any number of buttons, which can differ from
    other sections.
    """

    def __init__(
        self,
        name: str = "Radial Menu",
        inner_radius: float = 1.00,
        outer_radius: float = 3.00,
        sections: int = 1
    ):
        super().__init__(name=name)

        self._inner_radius = inner_radius
        self._outer_radius = outer_radius
        self._section_count = sections

        self._item_position: Dict[RadialButton, RadialButtonPosition] = dict()
        self._item_section_count: Dict[int, int] = dict()
        self._items_changed_flag = True
        self._last_hovered: Optional[RadialButton] = None

    @property
    def last_hovered(self) -> Optional[RadialButton]:
        return self._last_hovered

    def add_item(self, *args: Any, **kwargs: Any) -> RadialButton:
        section = kwargs.get('section', 0)
        return self.add_item_at(self._item_section_count.get(section, 0), *args, **kwargs)

    def add_item_at(
            self,
            index: int,
            label: str,
            callback: Optional[Callable[[Object3D, LocalInputMethod], None]] = None,
            section: int = 0,
            toggle: bool = False,
            data: Any = None
    ) -> RadialButton:
        item_button = RadialButton(
            label=label,
            toggle=toggle,
            data=data,
            on_click=callback,
            on_hover=self._item_hover,
            inner_radius=self._inner_radius,
            outer_radius=self._outer_radius
        )
        self.add_child_at(item_button, 0)
        self._item_position[item_button] = RadialButtonPosition(index, section)
        self._item_section_count[section] = self._item_section_count.get(section, 0) + 1
        self._items_changed_flag = True
        self.invalidate()
        return item_button

    def remove_item(self, item) -> None:
        self.remove_child(item)
        self._item_section_count[self._item_position[item].section] -= 1
        self._item_position.pop(item)
        self._items_changed_flag = True
        self.invalidate()

    def remove_all_items(self) -> None:
        self.remove_all_children()
        self._items_changed_flag = True

    def reset_selection(self) -> None:
        for item in self.children:
            item.highlighted = False

    def _item_hover(self, target: Object3D, input: LocalInputMethod, hover: bool) -> None:
        if hover and self._last_hovered is not target:
            self.reset_selection()
            self._last_hovered = target
            self._last_hovered.highlighted = True
        elif not hover and self._last_hovered is target:
            self._last_hovered = None
            self.reset_selection()

    def update(self) -> None:
        if self._items_changed_flag:
            self._items_changed_flag = False
            self._last_hovered = None
            for item in self.children:
                assert(isinstance(item, RadialButton))
                item_position = self._item_position[item]
                item.index = item_position.index
                item.section = item_position.section
                item.total_for_section = self._item_section_count[item.section]
                item.section_count = self._section_count

        super().update()

    def on_opening(self, input: LocalInputMethod) -> None:
        super().on_opening(input)
        for item in self.children:
            item.on_opening()

    def on_opened(self, input: LocalInputMethod) -> None:
        super().on_opened(input)
        for item in self.children:
            item.on_opened()

    def on_quick_action(self, input: 'LocalInputMethod') -> None:
        super().on_quick_action(input)
        if self._last_hovered:
            self._last_hovered.do_action(input)

    def on_closing(self, user_menu_data: UserMenuData, callback: Callable[[], None]) -> None:
        for item in self.children:
            item.on_closing()
        super().on_closing(user_menu_data, callback)

    def on_closed(self) -> None:
        super().on_closed()
        for item in self.children:
            item.on_closed()
