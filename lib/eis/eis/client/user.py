import logging

from typing import Any, Dict, List, TYPE_CHECKING

from rx import Observable  # type: ignore
from rx.subjects import Subject  # type: ignore

from eis import EISEntityId
from eis.client.input import LocalInputMethod
from eis.constants import CLIENT
from eis.input import InputMethod
from eis.inputs.support.buttons import ButtonStateEnum
from eis.user import EISUser, PeerUserBase
from satmath.euler import Euler  # type: ignore
from satmath.quaternion import Quaternion   # type: ignore
from satmath.vector3 import Vector3  # type: ignore
from satmath.vector4 import Vector4  # type: ignore
from satnet.entity import entity

if TYPE_CHECKING:
    # noinspection PyUnresolvedReferences
    from eis.client.session import EISLocalSession
    # noinspection PyUnresolvedReferences
    from eis.client.editor import ClientEditor

logger = logging.getLogger(__name__)


if CLIENT:

    @entity(id=EISEntityId.USER)
    class LocalEISUser(EISUser['ClientEditor', 'EISLocalSession', 'LocalInputMethod']):
        """
        Local EIS user
        Handles stuff like user input.
        """

        def __init__(self, config: Dict[str, Any] = {}) -> None:
            super().__init__(config=config)

            """
            These are all the individual simple actions a user can take, as subjects
            They will be mapped and merged into internally usable streams later but they are
            available as individual subjects in order to connect input methods to them.
            """

            # Boolean movement subjects
            self._free_direction_picker = Subject()
            self._free_move_direction = Subject()
            self._free_move_forward = Subject()
            self._free_move_backward = Subject()
            self._constrained_move_forward = Subject()
            self._constrained_move_backward = Subject()
            self._constrained_move_left = Subject()
            self._constrained_move_right = Subject()
            self._constrained_move_up = Subject()
            self._constrained_move_down = Subject()
            self._increase_speed = Subject()
            self._decrease_speed = Subject()

            # The movement is a combination of a constrained part from discrete inputs and
            # a free move part from continuous inputs.

            # Combined movement subjects into one movement direction (Vector3) stream
            self._constrained_move_direction = Observable.merge([
                self._constrained_move_forward.map(lambda move: Vector3([0, 1 if move else -1, 0])),
                self._constrained_move_backward.map(lambda move: Vector3([0, -1 if move else 1, 0])),
                self._constrained_move_left.map(lambda move: Vector3([-1 if move else 1, 0, 0])),
                self._constrained_move_right.map(lambda move: Vector3([1 if move else -1, 0, 0])),
                self._constrained_move_up.map(lambda move: Vector3([0, 0, 1 if move else -1])),
                self._constrained_move_down.map(lambda move: Vector3([0, 0, -1 if move else 1]))
            ])

            # Combined movement subjects into one movement speed (float) stream
            self._constrained_move_speed = Observable.merge([
                self._constrained_move_forward,
                self._constrained_move_backward,
                self._constrained_move_left,
                self._constrained_move_right,
                self._constrained_move_up,
                self._constrained_move_down
            ]).map(lambda down: 1.0 if down else -1.0)

            # Global direction of the free move component (forward or backward only)
            self._free_move_direction = Observable.merge([
                self._free_move_forward.map(lambda move: Vector3([0, 1 if move else -1, 0])),
                self._free_move_backward.map(lambda move: Vector3([0, -1 if move else 1, 0]))
            ])

            self._free_move_speed = Observable.merge([
                self._free_move_forward,
                self._free_move_backward
            ]).map(lambda down: 1.0 if down else -1.0)

            # Prepare the rotation stream
            self._rotate = Subject()

            # Movement streams

            def normalize_if_has_length(vector: Vector3) -> Vector3:
                return vector.normalized if vector.length != 0 else vector.copy()

            def make_move_vector(dir: Vector3, speed: float) -> Vector3:
                d = dir.copy()
                if d.length != 0:
                    d.length = speed
                return d

            # Move stream is a combination of direction and speed
            self._constrained_move = Observable.combine_latest([
                self._constrained_move_direction.scan(lambda x, y: x + y).map(normalize_if_has_length),
                self._constrained_move_speed.scan(lambda x, y: x + y).map(lambda x: min(x, 1.0))
            ], make_move_vector)

            # Computation of the final Vector3 for the free move direction by
            # combining the picker's information and the global direction.
            # the value has to be multiplied by the inverse rotation of the editor
            # because later in the pipeline, it will be re-multiplied
            # by the editor rotation (in fly state)
            free_move_direction = Observable.combine_latest([
                self._free_move_direction.scan(lambda x, y: x + y),
                self._free_direction_picker
            ], lambda move_dir, picker_dir: Vector3(self._editor.rotation.inverse.mul_vector4(picker_dir.mul_vector4(Vector4((*move_dir, 0.0)))).xyz) if self._editor is not None else Vector3())

            # Combine free move direction and speed
            self._free_move = Observable.combine_latest([
                free_move_direction.map(lambda x: x).map(normalize_if_has_length),
                self._free_move_speed.scan(lambda x, y: x + y).map(lambda x: min(x, 1.0))
            ], make_move_vector)

            # Combination of the constrained move and the free move for the complete move
            self._move = Observable.combine_latest([
                self._constrained_move.map(lambda x: x).start_with(Vector3()),
                self._free_move.map(lambda x: x).start_with(Vector3())
            ], lambda x, y: x + y)

            # State actions
            self._select = Subject()
            self._hold = Subject()
            self._confirm = Subject()
            self._help = Subject()
            self._recall = Subject()
            self._alternative_select = Subject()
            self._quick_translate = Subject()
            self._quick_rotate = Subject()
            self._quick_scale = Subject()
            self._menu = Subject()
            self._undo = Subject()
            self._reset = Subject()
            self._slow_down = Subject()
            self._speed_up = Subject()
            self._navigate = Subject()
            self._redo = Subject()
            self._axis_x = Subject()
            self._axis_y = Subject()
            self._axis_z = Subject()
            self._axis_w = Subject()
            self._engage = Subject()
            self._hands = Subject()
            self._drag = Subject()
            self._distance = Subject()
            self._toggle_timeline = Subject()

        @property
        def move(self) -> Subject:
            return self._move

        @property
        def rotate(self) -> Subject:
            return self._rotate

        @property
        def select(self) -> Subject:
            return self._select

        @property
        def hold(self) -> Subject:
            return self._hold

        @property
        def confirm(self) -> Subject:
            return self._confirm

        @property
        def help(self) -> Subject:
            return self._help

        @property
        def recall(self) -> Subject:
            return self._recall

        @property
        def alternative_select(self) -> Subject:
            return self._alternative_select

        @property
        def quick_translate(self) -> Subject:
            return self._quick_translate

        @property
        def quick_rotate(self) -> Subject:
            return self._quick_rotate

        @property
        def quick_scale(self) -> Subject:
            return self._quick_scale

        @property
        def engage(self) -> Subject:
            return self._engage

        @property
        def hands(self) -> Subject:
            return self._hands

        @property
        def menu(self) -> Subject:
            return self._menu

        @property
        def undo(self) -> Subject:
            return self._undo

        @property
        def reset(self) -> Subject:
            return self._reset

        @property
        def slow_down(self) -> Subject:
            return self._slow_down

        @property
        def speed_up(self) -> Subject:
            return self._speed_up

        @property
        def navigate(self) -> Subject:
            return self._navigate

        @property
        def redo(self) -> Subject:
            return self._redo

        @property
        def axis_x(self) -> Subject:
            return self._axis_x

        @property
        def axis_y(self) -> Subject:
            return self._axis_y

        @property
        def axis_z(self) -> Subject:
            return self._axis_z

        @property
        def axis_w(self) -> Subject:
            return self._axis_w

        @property
        def drag(self) -> Subject:
            return self._drag

        @property
        def distance(self) -> Subject:
            return self._distance

        @property
        def toggle_timeline(self) -> Subject:
            return self._toggle_timeline

        def dispose(self) -> None:
            self._constrained_move_direction.dispose()
            self._constrained_move_speed.dispose()
            self._rotate.dispose()
            self._select.dispose()
            self._hold.dispose()
            self._confirm.dispose()
            self._help.dispose()
            self._recall.dispose()
            self._alternative_select.dispose()
            self._quick_translate.dispose()
            self._quick_rotate.dispose()
            self._quick_scale.dispose()
            self._menu.dispose()
            self._undo.dispose()
            self._reset.dispose()
            self._slow_down.dispose()
            self._speed_up.dispose()
            self._navigate.dispose()
            self._redo.dispose()
            self._axis_x.dispose()
            self._axis_y.dispose()
            self._axis_z.dispose()
            self._axis_w.dispose()
            self._engage.dispose()
            self._drag.dispose()
            self._distance.dispose()
            self._toggle_timeline.dispose()

            super().dispose()

        def add_input(self, input: InputMethod) -> None:
            assert isinstance(input, LocalInputMethod)
            super().add_input(input)
            self.map_events(input)

        def map_events(self, input: LocalInputMethod) -> None:
            # Gets a quaternion giving the necessary rotation to apply to go towards the picked direction.
            if input.picker_subject:
                input.picker_subject.map(lambda direction: direction).subscribe(self._free_direction_picker)

            input.button_subject.filter(lambda btn: btn[0] == '+' and btn[1] ==
                                        ButtonStateEnum.PRESSED).map(lambda btn: 1).subscribe(self._increase_speed)
            input.button_subject.filter(lambda btn: btn[0] == '-' and btn[1] ==
                                        ButtonStateEnum.PRESSED).map(lambda btn: -1).subscribe(self._decrease_speed)

            input.button_subject \
                .filter(lambda e: e[0] == 'vive_primary_trackpad') \
                .filter(lambda e: e[1] != ButtonStateEnum.UP) \
                .map(lambda e: (input, e[1] in [ButtonStateEnum.TOUCHED, ButtonStateEnum.TOUCHING])) \
                .subscribe(self._engage)

            input.common_subject \
                .filter(lambda e: e[0] == 'distance') \
                .subscribe(self._distance)

            # Map keys to internal move actions
            def map_movement(buttons: List[str], action: Subject) -> None:
                input.button_subject \
                    .filter(lambda e: e[0] in buttons and (e[1] == ButtonStateEnum.PRESSED or e[1] == ButtonStateEnum.RELEASED)) \
                    .map(lambda e: e[1] == ButtonStateEnum.PRESSED).subscribe(action)

            # Map mouse to "head" rotation
            # Note that for RenderPipeline, top left is (-1, -1), and bottom right is (1, 1)
            # We need to negate the delta in x because a counter-clockwise rotation is represented
            # by a negative angle in our frame of reference
            def map_head_rotation(buttons: List[str], action: Subject) -> None:
                paired_mouse_pos = input.mouse_subject.distinct_until_changed().pairwise()
                input.button_subject \
                    .filter(lambda btn: btn[0] in buttons) \
                    .map(lambda btn: btn[1] == ButtonStateEnum.PRESSED) \
                    .switch_map(lambda down: paired_mouse_pos if down else Observable.never()) \
                    .map(lambda t: (t[1][0] - t[0][0], t[1][1] - t[0][1])) \
                    .map(lambda xy: Quaternion.from_z_rotation(-xy[0] * 0.05) * Quaternion.from_x_rotation(xy[1] * 0.05)).subscribe(action)

            # Map vive to "head" rotation
            def map_vive_head_rotation(buttons: List[str], action: Subject) -> None:
                paired_tracker_pos = input.tracker_subject.filter(
                    lambda e: e[0] == 'vive_secondary').distinct_until_changed().pairwise()
                input.button_subject \
                    .filter(lambda btn: btn[0] in buttons) \
                    .map(lambda btn: btn[1] == ButtonStateEnum.PRESSED or btn[1] == ButtonStateEnum.DOWN) \
                    .switch_map(lambda down: paired_tracker_pos if down else Observable.never()) \
                    .map(lambda t, i: Quaternion.from_euler(Euler([Euler.from_matrix(t[0][1] * t[1][1].inverse)[0], 0.0, Euler.from_matrix(t[0][1] * t[1][1].inverse)[2]]))) \
                    .subscribe(action)

            # Map joystick states
            def map_axis(analogs: List[str], selected_axis: int, action: Subject) -> None:
                input.analog_subject \
                    .filter(lambda e: e[0] in analogs and e[1] == selected_axis) \
                    .map(lambda e: (input, e[2])) \
                    .subscribe(action)

            # Map HTC states
            def map_htc_axis(analog: List[str], selected_axis: int, action: Subject) -> None:
                Observable.combine_latest([
                    input.analog_subject.filter(lambda e: e[0] in analog and e[1] == selected_axis),
                    input.button_subject.filter(lambda e: e[0] in analog + '_trackpad')
                ], lambda button, pad: (input, button[2], 1 if pad[1] == ButtonStateEnum.UP else 2 if pad[1] == ButtonStateEnum.DOWN else 3 if pad[1] == ButtonStateEnum.TOUCHING else 4 if pad[1] == ButtonStateEnum.UNTOUCHED else 0)) \
                    .subscribe(action)

            def map_htc_horizontal(analog: List[List[str]], action: Subject) -> None:
                map_htc_axis(analog=analog[0], selected_axis=0, action=action)

            def map_htc_vertical(analog: List[List[str]], action: Subject) -> None:
                map_htc_axis(analog=analog[0], selected_axis=1, action=action)

            # Maps an axis with the keyboard. Expects two buttons (negative and positive))
            def map_keyboard_axis(buttons: List[str], action: Subject) -> None:
                input.button_subject \
                    .filter(lambda e: e[0] in buttons and (e[1] == ButtonStateEnum.PRESSED or ButtonStateEnum.RELEASED)) \
                    .map(lambda e: (input, int(e[0] == buttons[0]) - int(e[0] == buttons[1]) if e[1] == ButtonStateEnum.PRESSED else 0, 5)) \
                    .subscribe(action)

            # Subscribes if a section of the trackpad is pressed, between min and max (always between -1 and 1)
            def map_pressed_htc(analog: str, selected_axis: int, min: float, max: float, action: Subject) -> None:
                Observable.with_latest_from([
                    input.button_subject.filter(lambda e: e[0] == analog +
                                                '_trackpad' and e[1] == ButtonStateEnum.DOWN),
                    input.analog_subject.filter(lambda e: e[0] == analog and e[1] == selected_axis),
                ], lambda button, pad: (input, pad[2])) \
                    .filter(lambda e: e[1] > min and e[1] < max) \
                    .map(lambda e: input) \
                    .subscribe(action)

            # Format for strings in analog: "vive_secondary/primary_trackpad_direction"
            def map_pressed_htc_section(analog: List[str], action: Subject) -> None:
                for this_analog in analog:
                    split = this_analog.split("_trackpad")
                    if split[1] == "":
                        map_pressed_htc(split[0], 1, -1, 1, action)
                    elif split[1] == "_left":
                        map_pressed_htc(split[0], 0, -1, -0.3, action)
                    elif split[1] == "_right":
                        map_pressed_htc(split[0], 0, 0.3, 1, action)
                    elif split[1] == "_down":
                        map_pressed_htc(split[0], 1, -1, -0.3, action)
                    elif split[1] == "_up":
                        map_pressed_htc(split[0], 1, 0.3, 1, action)

            # Subscribes if the analog of the vive is pressed(1) or unpressed/touched(0)
            def map_toggle_htc(analog: List[str], action: Subject) -> None:
                input.button_subject \
                    .filter(lambda e: e[0] in [i + '_trackpad' for i in analog]) \
                    .map(lambda e: (input, e[1] == ButtonStateEnum.DOWN)) \
                    .subscribe(action)

            # Action Mapping Helper
            def map_action(buttons: List[str], action: Subject) -> None:
                input.button_subject \
                    .filter(lambda e: e[0] in buttons and e[1] == ButtonStateEnum.PRESSED) \
                    .map(lambda e: input) \
                    .subscribe(action)

            # subscribes if any of the buttons are pressed/released and returns whether it has been pressed or released
            def map_toggle_action(buttons: List[str], action: Subject) -> None:
                input.button_subject \
                    .filter(lambda e: e[0] in buttons and (e[1] == ButtonStateEnum.PRESSED or e[1] == ButtonStateEnum.RELEASED)) \
                    .map(lambda e: (input, e[1] == ButtonStateEnum.PRESSED)) \
                    .subscribe(action)

            def map_double_click_action(buttons: List[str], action: Subject) -> None:
                input.button_subject \
                    .filter(lambda e: e[0] in buttons and e[1] == ButtonStateEnum.PRESSED) \
                    .buffer(buffer_openings=input.button_subject.filter(lambda e: e[0] in buttons and e[1] == ButtonStateEnum.PRESSED).debounce(500)) \
                    .filter(lambda e: len(e) == 2) \
                    .subscribe(action)

            # Will not subscribe double-clicks, but only single clicks (delay should be the same as the double click for the same buttons)
            def map_single_click_action(buttons: List[str], action: Subject) -> None:
                input.button_subject \
                    .filter(lambda e: e[0] in buttons and e[1] == ButtonStateEnum.PRESSED) \
                    .buffer(buffer_openings=input.button_subject.filter(lambda e: e[0] in buttons and e[1] == ButtonStateEnum.PRESSED).debounce(500)) \
                    .filter(lambda e: len(e) == 1) \
                    .subscribe(action)

            def map_long_toggle_action(buttons: List[str], action: Subject) -> None:
                input.button_subject \
                    .filter(lambda e: e[0] in buttons and (e[1] == ButtonStateEnum.PRESSED)) \
                    .delay(self._config.get('action.long_toggle_delay', 500)) \
                    .take_until(input.button_subject.filter(lambda e: e[0] in buttons and e[1] == ButtonStateEnum.RELEASED)) \
                    .map(lambda e: (input, e[1] == ButtonStateEnum.PRESSED)).repeat() \
                    .subscribe(action)

                input.button_subject \
                    .filter(lambda e: e[0] in buttons and (e[1] == ButtonStateEnum.RELEASED)) \
                    .map(lambda e: (input, e[1] == ButtonStateEnum.PRESSED)) \
                    .subscribe(action)

            # For mimicking shift+click with primary grab and primary trigger
            def map_toggle_combination_without_action(buttons: List[List[str]], action: Subject) -> None:
                Observable.with_latest_from([
                    input.button_subject.filter(lambda e: e[0] in [i[0] for i in buttons] and (
                        e[1] == ButtonStateEnum.PRESSED)),
                    input.button_subject.filter(lambda e: e[0] in [i[1] for i in buttons])
                    .start_with((buttons[0][1], ButtonStateEnum.RELEASED)),
                ], lambda button, shift: (button, shift)) \
                    .filter(lambda e: e[1][1] == ButtonStateEnum.UP or e[1][1] == ButtonStateEnum.RELEASED) \
                    .map(lambda e: (input, e[0][1] == ButtonStateEnum.PRESSED)) \
                    .subscribe(action)

                input.button_subject \
                    .filter(lambda e: e[0] in [i[0] for i in buttons] and (e[1] == ButtonStateEnum.RELEASED)) \
                    .map(lambda e: (input, False)) \
                    .subscribe(action)

            def map_toggle_combination_with_action(buttons: List[List[str]], action: Subject) -> None:
                Observable.with_latest_from([
                    input.button_subject.filter(lambda e: e[0] in [i[0] for i in buttons] and (
                        e[1] == ButtonStateEnum.PRESSED or e[1] == ButtonStateEnum.RELEASED)),
                    input.button_subject.filter(lambda e: e[0] in [i[1] for i in buttons]),
                ], lambda button, shift: (button, shift)) \
                    .filter(lambda e: e[1][1] == ButtonStateEnum.DOWN or e[1][1] == ButtonStateEnum.PRESSED) \
                    .map(lambda e: (input, e[0][1] == ButtonStateEnum.PRESSED)) \
                    .subscribe(action)

            mapping_dict = input.mapping_dict
            for key in mapping_dict:
                if mapping_dict[key] is not None:
                    if not hasattr(self, str("_" + key)):
                        logger.warning("The subject " + str("_" + key) + " does not exist.")
                        continue
                    if not mapping_dict[key][0] in locals():
                        logger.warning("The local function " + mapping_dict[key][0] + " does not exist.")
                        continue
                    locals()[mapping_dict[key][0]](mapping_dict[key][1], getattr(self, str("_" + key)))

    @entity(id=EISEntityId.PEER_USER)
    class PeerUser(PeerUserBase):
        """
        Client-side peer user
        Represents a user of a remote session
        """

        def __init__(self) -> None:
            super().__init__()
