import logging

from typing import Any, Callable, Dict, List, Optional, Tuple

from rx.core import Disposable  # type: ignore
from transitions import EventData  # type: ignore

from eis.client.client import EISClient, EISLocalSession
from eis.client.editor import ClientEditor
from eis.client.input import LocalInputMethod
from eis.client.states.base_state import BaseState
from eis.client.user import LocalEISUser
from eis.display.components.cursor import Cursor
from eis.display.components.menus.menu import Menu
from eis.graph.object_3d import Object3D
from satmath.vector3 import Vector3

logger = logging.getLogger(__name__)


class BaseEISState(BaseState):
    def __init__(self, *args: Any, recallable: bool = False, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self._recallable = recallable
        self._subscriptions: List[Disposable] = []

        """
        Default menu for the state
        Shows up when the user presses the menu button on the primary controller
        """
        self._menu = None

        """
        Default Cursor for the state
        """
        self._cursor: Optional[Callable[[], Cursor]] = None

        """
        Help data for the state

            self._help_data = {
                'primary': {
                    'menu': "Open/Close Menu",
                    'trigger': "Select object",
                    'grab': "Recall last state"
                },
                'secondary': {...}
            }

        """
        self._help_data = {}

        # Selected by input method
        self._selected_by_input_method: Dict[LocalInputMethod, bool] = {}

        # Syntax: ("user subject", "_local_method (without self.)", "help description (None disables the help)")
        self._inputs_mapping = [("menu", "_toggle_menu", "Toggle the menu"),
                                ("select", "_select_menu", None),
                                ("help", "_help", None)]

    @property
    def client(self) -> EISClient:
        return self._machine.client

    @property
    def session(self) -> EISLocalSession:
        return self._machine.client.session

    @property
    def editor(self) -> ClientEditor:
        return self._machine.editor

    @property
    def recallable(self) -> bool:
        return self._recallable

    @property
    def menu(self) -> Optional[Callable[[], Menu]]:
        return self._menu

    @menu.setter
    def menu(self, value: Optional[Callable[[], Menu]]) -> None:
        if self._menu != value:
            self._menu = value

    def enter(self, event: EventData) -> None:
        super().enter(event)
        for user in self.editor.users.values():
            self._subscriptions.extend(self._map_input(user))

        # In case there is already one defined
        self.editor.cursor_manager.cursor = self._cursor

    def exit(self, event: EventData) -> None:
        super().exit(event)

        # Disconnect picking streams
        for subscription in self._subscriptions:
            subscription.dispose()
        self._subscriptions = []

        # Remove cursor from users' cursor manager
        self.editor.cursor_manager.cursor = None

        self.on_menu_closed()  # XXX: Fake it...
        self.editor.menu_manager.close_menu()

    def recall(self) -> bool:
        """
        Recall the last state used
        :return:
        """
        return self._machine.recall()

    def always_run_before(self, now: float, dt: float) -> None:
        super().always_run_before(now=now, dt=dt)

        # Pick GUI
        self._pick_gui()

    def run_nested(self, modal: bool, now: float, dt: float) -> None:
        # TODO: Recall used to be here and super() wasn't called when the recall was successful
        super().run_nested(modal=modal, now=now, dt=dt)

    def _pick_gui(self) -> None:
        hovered_objects: Dict[Object3D, Dict[LocalInputMethod, Vector3]] = {}
        for user in self.editor.users.values():
            for input_method in user.input_methods.values():
                if not input_method.picker_active:
                    continue

                picked_interactive, picked_location = input_method.picker.pick_gui()
                if picked_interactive:
                    # Different input methods can hover the same object
                    hover_location_by_input = hovered_objects.get(picked_interactive)
                    if not hover_location_by_input:
                        hover_location_by_input: Dict[LocalInputMethod, Vector3] = {}
                        hovered_objects[picked_interactive] = hover_location_by_input
                    hover_location_by_input[input_method] = picked_location

        def hover_traversal(obj: Object3D) -> None:
            hover_location_by_input = hovered_objects.get(obj)
            # TODO: Handle more than one input?
            if hover_location_by_input:
                for input_method, hover_location in hover_location_by_input.items():
                    down = self._selected_by_input_method.get(input_method)
                    if down is None:
                        down = False
                    obj.set_hover(True, input_method, hover_location, down)
            else:
                obj.set_hover(False)

        self.editor.gui_layer.traverse(hover_traversal)

    def _map_input(self, user: LocalEISUser) -> List[Disposable]:
        """
        Map inputs for the given user
        This method should return a list of disposable from the subscription of input events.

        :param user: LocalEISUser
        :return: List[Disposable]
        """
        return_list = []
        for sublist in self._inputs_mapping:
            if not hasattr(user, sublist[0]):
                logger.warning("The subject " + sublist[0] + " does not exist in user.")
                continue
            if not hasattr(self, sublist[1]):
                logger.warning("The method " + sublist[1] + " does not exist in this state.")
                continue
            return_list.append(getattr(user, sublist[0]).subscribe(getattr(self, sublist[1])))

        return return_list

    def _select_menu(self, args: Tuple[LocalInputMethod, bool]) -> None:
        input: LocalInputMethod = args[0]
        down: bool = args[1]
        self._selected_by_input_method[input] = down
        # Make sure the "click" is seen if framerate is slow
        # If we click (down + up) fast, both events can happen inside one frame
        # and the change is not registered.
        self._pick_gui()

    def _toggle_menu(self, args: Tuple[LocalInputMethod, bool]) -> None:
        # This callback is executed for both down and up events,
        # but the `toggle_menu()` method is equipped to deal with this
        input: LocalInputMethod = args[0]
        down: bool = args[1]
        self.editor.menu_manager.toggle_menu(input, down, is_submenu=not down)

    def _help(self, args: Tuple[LocalInputMethod, bool]) -> None:
        input = args[0]
        if args[1]:
            if input.cursor is not None:
                input.cursor.state_mapping = self._inputs_mapping
                input.cursor.show_help()
        else:
            if input.cursor is not None:
                input.cursor.hide_help()

    def on_menu_opened(self, menu: Menu):
        if self._parent is not None:
            self._parent.on_menu_opened(menu)

    def on_menu_closed(self):
        if self._parent is not None:
            self._parent.on_menu_closed()
