import os
from datetime import datetime
from typing import Any, List, Tuple

from transitions.core import EventData  # type: ignore

from eis.client.input import LocalInputMethod
from eis.client.states.base_eis_state import BaseEISState
from eis.commands.animation import StartTimelineCommand, StopTimelineCommand
from eis.commands.engine import SaveSceneFileCommand, LoadSceneFileCommand, LoadSceneCommand
from eis.display.components.cursor import Cursor
from eis.display.components.menus.editor_radial_menu import EditorRadialMenu
from eis.display.components.menus.radial.radial_button import RadialButton
from eis.graph.light import Light
from eis.graph.object_3d import Object3D
from eis.graph.scene import Scene
from eis.graph.sound_object import SoundObject
from eis.graph.sound_objects.video_sound_object import VideoSoundObject
from satnet.commands.actions import RedoCommand, UndoCommand


class WelcomingMenu(EditorRadialMenu):
    def __init__(self) -> None:
        super().__init__(name="Welcoming Menu")

        # States Menu
        def _config(target: RadialButton, input: LocalInputMethod) -> None:
            assert(self._manager is not None)
            self._manager.open_menu(input, ConfigurationMenu())

        self.add_item("Configure", _config)

        def _scene_file(target: RadialButton, input: LocalInputMethod) -> None:
            assert(self._manager is not None)
            self._manager.open_menu(input, SceneFileMenu())

        self.add_item("Save/Load", _scene_file)

        def _present(target: RadialButton, input: LocalInputMethod) -> None:
            assert(self.editor is not None)
            self.editor.machine.present()
            self.close()

        self.add_item("Present", _present)

        def _edit_scene(target: RadialButton, input: LocalInputMethod) -> None:
            assert(self.editor is not None)
            self.editor.machine.edit_scene()
            self.close()

        self.add_item("Edit", _edit_scene)

    def on_opened(self, input: LocalInputMethod) -> None:
        super().on_opened(input)
        input.picker_active = True


class SceneFileMenu(EditorRadialMenu):
    def __init__(self) -> None:
        super().__init__(name="Scene File Menu", sections=2)

        def _save_scene(target: RadialButton, input: LocalInputMethod) -> None:
            assert(self.scene is not None)
            filename = f"{self.scene.name or 'unnamed'}_{datetime.now().strftime('%d-%b-%Y_%H:%M:%S')}.eis".replace(' ', '_')
            self.editor.machine.client.session.command(SaveSceneFileCommand(path=filename))
            self.close()

        self.add_item("Save scene", _save_scene, section=0)

        def _load_scene(target: RadialButton, input: LocalInputMethod) -> None:
            def open_scenes(scenes: List[str]):
                assert(self._manager is not None)
                self._manager.open_menu(input, LoadSceneMenu(scenes))

            self.editor.list_scenes(open_scenes)

        self.add_item("Load scene", _load_scene, section=0)

        def _export_gltf(target: RadialButton, input: LocalInputMethod) -> None:
            scene = self.editor.scene
            if scene:
                filename = f"{self.scene.name or 'unnamed'}_{datetime.now().strftime('%d-%b-%Y_%H:%M:%S')}.gltf".replace(' ', '_')
                self.editor.machine.client.session.command(SaveSceneFileCommand(path=filename))
            self.close()

        self.add_item("Export to GLTF", _export_gltf, section=1)

        def _import_gltf(target: RadialButton, input: LocalInputMethod) -> None:
            def import_scenes(scenes: List[str]):
                assert(self._manager is not None)
                self._manager.open_menu(input, ImportGLTFMenu(scenes))
            self.editor.list_gltf_scenes(import_scenes)
            self.close()

        self.add_item("Import from GLTF", _import_gltf, section=1)

        def _reset_scene(target: RadialButton, input: LocalInputMethod) -> None:
            assert(self._manager is not None)
            self._manager.open_menu(input, ResetSceneMenu())

        self.add_item("Reset scene", _reset_scene, section=1)


class ResetSceneMenu(EditorRadialMenu):
    def __init__(self) -> None:
        super().__init__(name="Reset Scene Menu")

        def _confirm(target: RadialButton, input: LocalInputMethod) -> None:
            self.editor.machine.client.session.command(LoadSceneCommand(Scene()))
            self.close()

        self.add_item("Confirm", _confirm)

        def _cancel(target: RadialButton, input: LocalInputMethod) -> None:
            self.close()

        self.add_item("Cancel", _cancel)


class ConfigurationMenu(EditorRadialMenu):
    def __init__(self) -> None:
        super().__init__(name="Configuration Menu")

        def _calibrate(target: RadialButton, input: LocalInputMethod) -> None:
            assert(self.editor is not None)
            self.editor.machine.calibrate()
            self.close()

        self.add_item("Calibrate", _calibrate)

        def _mouse_picker(target: RadialButton, input: LocalInputMethod) -> None:
            for input_method in input.user.input_methods.values():
                if str(type(input_method)) == "eis.engines.render_pipeline.input.RenderPipelineMovementController":
                    input_method.picker_active = not input_method.picker_active
            self.close()

        self.add_item("Toggle mouse picker", _mouse_picker)

        def _deactivate_picker_input(target: RadialButton, input: LocalInputMethod) -> None:
            input.picker_active = False
            self.close()

        self.add_item("Hide picker", _deactivate_picker_input)

        def _make_sound_light_invisible(target: RadialButton, input: LocalInputMethod) -> None:
            assert(self.editor is not None)
            for child in self.editor.scene.model.root.children:
                if child.name == "Added_objects_layer":
                    self._sound_layer = child
                    break

            visible = None
            for item in self._sound_layer.children:
                is_sound = False
                for child in item.children:
                    if isinstance(child, SoundObject) and not isinstance(child, VideoSoundObject):
                        is_sound = True
                if is_sound:
                    if item in self.editor.passive_objects:
                        self.editor.remove_passive_object(item)
                    if visible is None:
                        visible = not item.visible
                    item.visible = visible
                    item.pickable = visible

            def hide_child_lights(node: Object3D) -> None:
                for child in node.children:
                    if isinstance(child, Light):
                        child.visible = not child.visible
                    hide_child_lights(child)

            hide_child_lights(self.editor.scene.model.root)
            self.close()

        self.add_item("Sound/Light visibility", _make_sound_light_invisible)


class LoadSceneMenu(EditorRadialMenu):
    def __init__(self, scenes: List[str]) -> None:
        super().__init__(name="Load EiS scene")
        for s in scenes:
            self.add_item(s, self._load_scene, data=s)

    def _load_scene(self, target: RadialButton, input: LocalInputMethod) -> None:
        assert(self.editor is not None)
        self.editor.machine.client.session.command(LoadSceneFileCommand(target.data))
        self.close()


class ImportGLTFMenu(EditorRadialMenu):
    def __init__(self, scenes: List[str]):
        super().__init__(name="Load glTF scene")
        for s in scenes:
            self.add_item(os.path.basename(os.path.dirname(s)), self._load_scene, data=s)

    def _load_scene(self, target: RadialButton, input: LocalInputMethod) -> None:
        scene = self.editor.load_scene_from_file(path=target.data)
        if scene:
            self.editor.engine.show_status("LOADING SCENE")
            self.editor.machine.client.session.command(LoadSceneCommand(scene))
        self.close()


class WelcomingState(BaseEISState):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, name="welcoming", verb="welcome", recallable=True, **kwargs)

        # Cursor
        self._cursor = lambda: Cursor(
            selection_chevron=False,
            highlight_on_hover=False,
            label="Welcome",
            detail="",
            color=(0.15, 0.65, 0.95, 1.00),
        )

        # Menu
        self.menu = lambda: WelcomingMenu()

        # Syntax: ("user subject", "_local_method (without self.)", "help description (None disables the help)")
        self._inputs_mapping.extend([("navigate", "_navigate", "Navigate (while pressed)"),
                                     ("undo", "_undo", "Undo"),
                                     ("redo", "_redo", "Redo"),
                                     ("toggle_timeline", "_toggle_timeline", "Toggle timeline")])

    def should_enter(self, event: EventData) -> bool:
        should = super().should_enter(event)
        can = self.client.connected
        return should and can

    def exit(self, event: EventData) -> None:
        super().exit(event)
        self.editor.active_object = self.editor.last_active_object

    def _navigate(self, args: LocalInputMethod) -> None:
        if args[1]:
            if self.editor.synchronized_physical_body is not None:
                self.machine.walk()
            else:
                self.machine.fly()

    def _toggle_timeline(self, args: Tuple[LocalInputMethod, bool]) -> None:
        if not self.editor.menu_manager.menu_open:
            if args[1]:
                if self.editor.timeline.running:
                    self.editor.machine.client.session.command(StopTimelineCommand())
                    self.editor.timeline.stop()
                else:
                    self.editor.machine.client.session.command(StartTimelineCommand())
                    self.editor.timeline.start()

    def _undo(self, input: LocalInputMethod) -> None:
        if self._machine.state_object != self:
            return
        self._machine.client.session.command(UndoCommand())

    def _redo(self, input: LocalInputMethod) -> None:
        if self._machine.state_object != self:
            return
        self._machine.client.session.command(RedoCommand())
