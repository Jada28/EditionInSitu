import math
from typing import Any, Callable, Optional

from transitions.core import EventData  # type: ignore

from eis.client.input import LocalInputMethod
from eis.client.states.base_eis_state import BaseEISState
from eis.display.components.cursor import Cursor
from eis.display.components.menus.editor_radial_menu import EditorRadialMenu
from eis.graph.object_3d import Object3D
from eis.graph.primitives.shapes.arrow import Arrow
from satmath.matrix44 import Matrix44  # type: ignore
from satmath.quaternion import Quaternion  # type: ignore
from satmath.vector3 import Vector3  # type: ignore


class FlyMenu(EditorRadialMenu):
    def __init__(self) -> None:
        super().__init__(name="Fly Menu")

        def _back(target: Object3D, input: LocalInputMethod) -> None:
            self.editor.machine.state_object._on_exit()

        self.add_item("Return", _back)


class NavigatingCursor(Cursor):

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        diameter = self.radius * 2
        self.custom_cursor = Arrow(
            arrow_length=diameter,
            arrow_width=diameter,
            head_length_ratio=2.0 / 3.0,
            width=2
        )
        self.custom_cursor.x_rotation = math.pi / 4.0


class FlyState(BaseEISState):

    def __init__(self, *args: Any, on_exit: Optional[Callable[[], None]] = None, **kwargs: Any) -> None:
        super().__init__(*args, name="fly", verb="fly", recallable=False, **kwargs)

        self._on_exit = on_exit

        # Movement values
        self._movement = Vector3()
        self._velocity = Vector3()
        self._rotation_accumulator = Quaternion()
        self._speed_multiplier = 2
        self._smoothness = 10.0
        self._normal = Vector3()

        self._recall_state = False  # Whether it should recall to previous state right now or not

        # Cursor
        self._cursor = lambda: NavigatingCursor(
            selection_chevron=True,
            label="Navigation",
            color=(0.55, 0.76, 0.29, 1.00)
        )

        # Menu
        self.menu = lambda: FlyMenu()

        # Syntax: ("user subject", "_local_method (without self.)", "help description (None disables the help)")
        self._inputs_mapping.extend([("help", "_help", None),
                                     ("navigate", "_stop_navigating", "Stop navigating (on release)"),
                                     ("move", "_set_movement", None),
                                     ("reset", "_reset_position", "Reset position and navigating speed"),
                                     ("rotate", "_set_rotation", "Rotate camera (on hold)"),
                                     ("speed_up", "_speed_up", "Speed up movement"),
                                     ("slow_down", "_slow_down", "Slow down movement")])

    def should_enter(self, event: EventData) -> bool:
        should = super().should_enter(event)
        can = self.client.connected
        return should and can

    def enter(self, event: EventData) -> None:
        super().enter(event)

        # Reset values
        self._movement = Vector3()
        self._velocity = Vector3()
        self._rotation_accumulator = self.editor.rotation

    def exit(self, event: EventData) -> None:
        super().exit(event)

    def run(self, now: float, dt: float) -> None:
        super().run(now=now, dt=dt)
        self._calculate_movement(now, dt)

        if self._recall_state:
            self._back()

    def _back(self) -> None:
        self._recall_state = False
        self._on_exit()

    def _set_movement(self, movement: Vector3) -> None:
        """
        Movement setter for Rx subscription
        :return: None
        """
        # We don't set the location directly since it needs to be smoothed in `_calculate_movement`
        self._movement = movement

    def _set_rotation(self, rotation: Quaternion) -> None:
        """
        Rotation setter for Rx subscription
        :return: None
        """
        self._rotation_accumulator *= rotation
        self.editor.rotation = self._rotation_accumulator

    def _calculate_movement(self, now: float, dt: float) -> None:
        """
        Step method calculating camera/session movement from input values
        :param now: float
        :param dt: float
        :return: None
        """

        multiplier = dt * self._speed_multiplier

        # Horizontal plane movement first
        movement = Vector3([self._movement.x, self._movement.y, 0.]) * multiplier
        # Transform by the current rotation
        rotated_movement = self.machine.editor.rotation.mul_vector3(movement)
        # Z axis is independent of camera direction
        rotated_movement.z += self._movement.z * multiplier

        # This has nothing to do with proper physics velocity but is works for now
        self._velocity += rotated_movement

        self.machine.editor.location = self.machine.editor.location + self._velocity
        self._velocity = self._velocity * max(0.0, 1.0 - dt * 60.0 / max(0.01, self._smoothness))

    def _reset_position(self, input: LocalInputMethod) -> None:
        self._velocity = 0.0
        self._speed_multiplier = 2
        self._movement = Vector3()
        self._rotation_accumulator = Quaternion()
        new_matrix = Matrix44.identity()
        new_matrix.translation = self.editor.config['navigation.reset_position']
        self.editor.matrix = new_matrix

    def _speed_up(self, input: LocalInputMethod) -> None:
        self._speed_multiplier *= 2

    def _slow_down(self, input: LocalInputMethod) -> None:
        self._speed_multiplier /= 2

    def _stop_navigating(self, input: LocalInputMethod) -> None:
        # Using a boolean and recalling the state in "run" instead of here because if the client is laggy, it is possible
        # to reach a situation where we are still in the navigating state with the navigating button unpressed because the
        # previous state in state_machine hasn't updated yet. Calling the recall in run prevents this.
        if not input[1]:
            self._recall_state = True
