import logging

from typing import Any, List, Optional, Tuple

from transitions.core import EventData  # type: ignore

from eis.client.input import LocalInputMethod
from eis.client.states.base_eis_state import BaseEISState
from eis.display.components.cursor import Cursor
from eis.display.components.menus.editor_radial_menu import EditorRadialMenu
from eis.graph.object_3d import Object3D
from eis.graph.texture_video import TextureVideo
from satmath.euler import Euler

logger = logging.getLogger(__name__)


class EditingObjectMenu(EditorRadialMenu):
    def __init__(self):
        super().__init__(name="EditingObject Menu")
        self.currently_active_object: Optional[Object3D] = None

        def _assets(target: Object3D, input: LocalInputMethod) -> None:
            assert(self.editor)
            self.editor.machine.select_assets(
                input=input,
                zrot=Euler.from_matrix(self.matrix_world).z - Euler.from_matrix(self.editor.matrix).z,
                select_material=True,
                target_object=self.currently_active_object
            )

        self._asset_button = self.add_item("Asset", _assets)

        def _play_videos(target: Object3D, input: LocalInputMethod) -> None:
            video_textures = self._get_video_textures()
            for video_texture in video_textures:
                video_texture.video_playback = True

        self._video_play_button = self.add_item("Play videos", _play_videos)

        def _pause_videos(target: Object3D, input: LocalInputMethod) -> None:
            video_textures = self._get_video_textures()
            for video_texture in video_textures:
                video_texture.video_playback = False

        self._video_pause_button = self.add_item("Pause videos", _pause_videos)

        def _back(target: Object3D, input: LocalInputMethod) -> None:
            assert(self.editor)
            self.editor.machine.edit_scene()
            self.close()

        self.add_item("Back", _back)

    def _get_video_textures(self) -> List[TextureVideo]:
        assert(self._state)
        assert(self._state._root_object is not None)
        active_object = self._state._root_object
        if active_object is None or active_object.material is None or not active_object.material.material_textures:
            return []

        video_textures = []
        for material_texture in active_object.material.material_textures:
            if isinstance(material_texture.texture, TextureVideo):
                video_textures.append(material_texture.texture)

        return video_textures

    def on_opened(self, input: LocalInputMethod) -> None:
        super().on_opening(input)
        self.currently_active_object = self.editor.last_active_object
        self._asset_button.enabled = self.editor.last_active_object is not None

        self._video_play_button.enabled = self.editor.last_active_object is None
        self._video_pause_button.enabled = self.editor.last_active_object is None

    def on_closed(self) -> None:
        super().on_closed()
        self.currently_active_object = None


class EditingObjectState(BaseEISState):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, name="editing_object", verb="edit_object", recallable=True, **kwargs)

        # Cursor
        self._cursor = lambda: Cursor(
            selection_chevron=True,
            highlight_on_hover=False,
            label="select",
            detail="",
            color=(0.15, 0.65, 0.95, 1.00),
        )

        # Menu
        self.menu = lambda: EditingObjectMenu()

        # Quick transformations
        self._quick_translating = False
        self._quick_rotating = False
        self._quick_scaling = False

        # Variables
        self._root_object: Optional[Object3D] = None

        # Whether the previous state is navigating
        self.from_navigating = False

        # Syntax: ("user subject", "_local_method (without self.)", "help description (None disables the help)")
        self._inputs_mapping.extend(
            [("navigate", "_navigate", "Navigate (while pressed)"),
             ("select", "_select", "Select an object"),
             ("undo", "_undo", "Undo"),
             ("redo", "_redo", "Redo")]
        )

    def should_enter(self, event: EventData) -> bool:
        should = super().should_enter(event)
        if (event.state.name == "fly" or event.state.name == "walk"):
            self.from_navigating = True
        can = self.client.connected and (self.editor.active_object is not None or (
            self._root_object is not None and self.from_navigating))

        return should and can

    def enter(self, event: EventData) -> None:
        # Should happen before super to be available for the cursor in super()
        super().enter(event)
        if self.from_navigating:
            self.from_navigating = False
        else:
            self._root_object = self.editor.last_active_object

    def exit(self, event: EventData) -> None:
        super().exit(event)
        if (event.event.name == "fly" or event.event.name == "walk"):
            return
        self.editor.active_object = self.editor.last_active_object

    def run(self, now: float, dt: float) -> None:
        super().run(now=now, dt=dt)

    def _navigate(self, args: Tuple[LocalInputMethod, bool]) -> None:
        if args[1]:
            if self.editor.synchronized_physical_body is not None:
                self.machine.walk()
            else:
                self.machine.fly()

    def _select(self, args: Tuple[LocalInputMethod, bool]) -> None:
        # If the button is unpressed
        if not args[1]:
            return

        assert(self._root_object is not None)
        result = args[0].picker.pick_in(self._root_object, False)
        if result is not None:
            active_object = self.editor.active_object
            object3D = result[0]
            if active_object is object3D:
                self.editor.active_object = None
            else:
                self.editor.active_object = object3D

    def _undo(self, input: LocalInputMethod) -> None:
        pass

    def _redo(self, input: LocalInputMethod) -> None:
        pass
