# Transforming idle state
import os
from typing import Any

from transitions.core import EventData  # type: ignore

from eis.client.input import LocalInputMethod
from eis.client.states.base_eis_state import BaseEISState
from eis.display.components.cursor import Cursor
from eis.display.components.menus.editor_radial_menu import EditorRadialMenu
from eis.graph.object_3d import Object3D
from enum import Enum
from satnet.commands.actions import RedoCommand, UndoCommand


class Transformations(Enum):
    TRANSLATING = 0
    ROTATING = 1
    SCALING = 2


class TransformingMenu(EditorRadialMenu):
    def __init__(self) -> None:
        super().__init__(name="Transforming Menu")

        # States Menu
        def _change_transformation(target: Object3D, input: LocalInputMethod) -> None:
            self._manager.open_menu(input, ChangeTransformationMenu())

        self.add_item("Change transformation", _change_transformation)

        def _exit(target: Object3D, input: LocalInputMethod) -> None:
            self.editor.machine.edit_scene()
            self.close()

        self.add_item("Exit", _exit)

    def on_opened(self, input: LocalInputMethod) -> None:
        super().on_opened(input)
        self.editor.machine.state_object.transform_lock = True

    def on_closed(self) -> None:
        super().on_closed()
        self.editor.machine.state_object.transform_lock = False


class ChangeTransformationMenu(EditorRadialMenu):
    def __init__(self) -> None:
        super().__init__(name="Change Transformation Menu")

        def _translate(target: Object3D, input: LocalInputMethod) -> None:
            self.editor.machine.state_object.enabled_transformation = Transformations.TRANSLATING
            self.close()

        self._translate_button = self.add_item("Translate", _translate)

        def _rotate(target: Object3D, input: LocalInputMethod) -> None:
            self.editor.machine.state_object.enabled_transformation = Transformations.ROTATING
            self.close()

        self._rotate_button = self.add_item("Rotate", _rotate)

        def _scale(target: Object3D, input: LocalInputMethod) -> None:
            self.editor.machine.state_object.enabled_transformation = Transformations.SCALING
            self.close()

        self._scale_button = self.add_item("Scale", _scale)

    def on_opened(self, input: LocalInputMethod) -> None:
        super().on_opened(input)
        self.editor.machine.state_object.transform_lock = True
        self._translate_button.enabled = Transformations(
            self.editor.machine.state_object.enabled_transformation) != Transformations.TRANSLATING
        self._rotate_button.enabled = Transformations(
            self.editor.machine.state_object.enabled_transformation) != Transformations.ROTATING
        self._scale_button.enabled = Transformations(
            self.editor.machine.state_object.enabled_transformation) != Transformations.SCALING

    def on_closed(self) -> None:
        super().on_closed()
        self.editor.machine.state_object.transform_lock = False


class TransformingState(BaseEISState):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, name="transforming", verb="transform", recallable=True, **kwargs)

        # Cursor
        self._cursor = lambda: Cursor(
            selection_chevron=False,
            highlight_on_hover=False,
            label="",
            detail="",
            color=(0.15, 0.65, 0.95, 1.00),
        )

        # Menu
        self.menu = lambda: TransformingMenu()

        # Syntax: ("user subject", "_local_method (without self.)", "help description (None disables the help)")
        self._inputs_mapping.extend([("navigate", "_navigate", "Navigate (while pressed)"),
                                     ("select", "_start_transforming", "Modify the object"),
                                     ("undo", "_undo", "Undo"),
                                     ("redo", "_redo", "Redo")])

        self._enabled_transformation = None
        self._transform_lock = False

    def enter(self, event: EventData) -> None:
        super().enter(event)
        transformation = event.kwargs.get('transformation')
        if transformation is not None:
            self._enabled_transformation = Transformations(transformation)

    def should_enter(self, event: EventData) -> bool:
        should = super().should_enter(event)
        can = self.client.connected
        return should and can

    @property
    def enabled_transformation(self) -> str:
        return self._enabled_transformation.value

    @enabled_transformation.setter
    def enabled_transformation(self, value: str) -> None:
        if self._enabled_transformation is not Transformations(value):
            self._enabled_transformation = Transformations(value)

    @property
    def transform_lock(self) -> bool:
        return self.transform_lock

    @transform_lock.setter
    def transform_lock(self, value: bool) -> None:
        if self._transform_lock != value:
            self._transform_lock = value

    def _navigate(self, args: LocalInputMethod) -> None:
        if args[1]:
            if self.editor.synchronized_physical_body is not None:
                self.machine.walk()
            else:
                self.machine.fly()

    def _undo(self, input: LocalInputMethod) -> None:
        if self._machine.state_object != self:
            return
        self._machine.client.session.command(UndoCommand())

    def _redo(self, input: LocalInputMethod) -> None:
        if self._machine.state_object != self:
            return
        self._machine.client.session.command(RedoCommand())

    def _start_transforming(self, args: LocalInputMethod) -> None:
        if args[1] and not self._transform_lock:
            if self._enabled_transformation == Transformations.TRANSLATING:
                self.machine.translate(input=args[0], quick=False)
            elif self._enabled_transformation == Transformations.ROTATING:
                self.machine.rotate(input=args[0], quick=False)
            elif self._enabled_transformation == Transformations.SCALING:
                self.machine.scale(input=args[0], quick=False)
