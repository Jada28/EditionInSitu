import logging
import math

from typing import Any, Callable, List, Optional, Tuple

from transitions.core import EventData  # type: ignore

from eis.actions.engine import AddModelAction, AddObject3DAction, SetMaterialAction
from eis.client.input import LocalInputMethod
from eis.client.states.base_eis_state import BaseEISState
from eis.display.components.cursor import Cursor
from eis.graph.behaviors.highlight_behavior import HighlightBehavior
from eis.graph.geometry import Geometry
from eis.graph.mesh import Mesh
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3

logger = logging.getLogger(__name__)


class SelectingAssetsState(BaseEISState):
    def __init__(self, *args: Any, on_exit: Optional[Callable[[], None]] = None, **kwargs: Any) -> None:
        super().__init__(*args, name="selecting_assets", **kwargs)

        self._on_exit = on_exit

        self._highlighted_objects: List[Object3D] = []
        self._highlight_behavior_speed = 2.0

        # If True, the asset manager will assign the selected object material to the active object
        self._select_material = False
        self._target_object: Optional[Object3D] = None

        self._input_method = None

        # Movement values
        self._movement = Vector3()
        self._velocity = Vector3()
        self._rotation_accumulator = Quaternion()
        self._speed_multiplier = 2
        self._smoothness = 10.0
        self._normal = Vector3()

        # Cursor
        self._cursor = lambda: Cursor(
            selection_chevron=False,
            highlight_on_hover=True,
            label="Select asset to add",
            detail="",
            color=(0.15, 0.65, 0.95, 1.00),
        )

        # Syntax: ("user subject", "_local_method (without self.)", "help description (None disables the help)")
        self._inputs_mapping.extend([("menu", "_back", "Go back"),
                                     ("move", "_set_movement", None),
                                     ("rotate", "_set_rotation", "Rotate camera (on hold)"),
                                     ("reset", "_reset_position", "Reset position and navigating speed"),
                                     ("select", "_select_asset", "Add asset to scene"),
                                     ("speed_up", "_speed_up", "Speed up movement"),
                                     ("slow_down", "_slow_down", "Slow down movement")])

        # Whether an asset has been added (relevant when exiting state)
        self._added_object = False

    def should_enter(self, event: EventData) -> bool:
        should = super().should_enter(event)
        return should

    def enter(self, event: EventData) -> None:
        super().enter(event)
        self._behavior = HighlightBehavior(color=(1.0, 0.6, 0.6, 1.0))
        self._behavior.managed = True

        input_method = event.kwargs.get('input')
        if input_method:
            self._input_method = input_method

        self._select_material = event.kwargs.get('select_material', False)
        self._target_object = event.kwargs.get('target_object', None)
        self.machine.editor.asset_library.rotation = Quaternion()
        self.machine.editor.asset_library.location = Vector3([0.0, 0.0, 0.0])

        # Reset values
        self._movement = Vector3()
        self._velocity = Vector3()
        self._rotation_accumulator = Quaternion.from_z_rotation(event.kwargs.get('zrot'))

        # Optional properties that govern properties when an asset is added.
        # Useful for replacing an object with an asset.
        self._matrix_when_added: Optional[Matrix44] = event.kwargs.get('matrix')
        self._parent_when_added: Optional[Object3D] = event.kwargs.get('parent')

        self._added_object = False

    def exit(self, event: EventData) -> None:
        for highlighted_object in self._highlighted_objects:
            highlighted_object.remove_behavior_by_type(HighlightBehavior)
        self.machine.editor.asset_library.deactivate()
        self._highlighted_objects.clear()
        if not self._added_object and self.machine.editor.delete_active_object:
            self.machine.editor.delete_active_object = False
        super().exit(event)

    def run(self, now: float, dt: float) -> None:
        super().run(now=now, dt=dt)
        # Highlight picked object
        picked_objects: List[Object3D] = []
        for user in self.editor.users.values():
            result = self._input_method.picker.pick_in(root=self.machine.editor.asset_layer, interactive_objects=True)
            if result:
                picked_object, position = result
                if picked_object is not None:
                    picked_objects.append(picked_object)

        if picked_objects:
            for user in self.editor.users.values():
                for input_method in user.input_methods.values():
                    input_method.cursor.label = "Select asset - {}".format(picked_objects[-1].name)
        else:
            for user in self.editor.users.values():
                for input_method in user.input_methods.values():
                    input_method.cursor.label = "Select an asset to add"

        for highlighted_object in self._highlighted_objects:
            if highlighted_object not in picked_objects:
                self.machine.editor.asset_library.remove_hovered_object3D(highlighted_object)
                highlighted_object.remove_behavior_by_type(HighlightBehavior)
                self._highlighted_objects.remove(highlighted_object)
                self._input_method.cursor.color = (0.05, 0.75, 1.0, 1.0)

        for picked_object in picked_objects:
            if picked_object not in self._highlighted_objects:
                self.machine.editor.asset_library.add_hovered_object3D(picked_object)
                picked_object.add_behavior(HighlightBehavior(speed=self._highlight_behavior_speed))
                self._highlighted_objects.append(picked_object)
                self._input_method.cursor.color = (1.0, 0.75, 0.05, 1.0)

        # Movement
        self._calculate_movement(now, dt)

    # Should only be called by _select_asset
    def _check_add_asset_validity(self, args: Tuple[LocalInputMethod, bool]) -> bool:
        if args[0] is not self._input_method:
            return False
        if not args[1]:
            return False
        if len(self._highlighted_objects) == 0:
            return False
        return True

    def _add_asset(self) -> None:
        """
        This method should be extended to take into consideration any specific asset needs.
        """

        # Get the last element of the highlighted objects (last highlighted object)
        # If there is more than 1 cursor, more than 1 object can be highlighted, so we want the last highlighted
        # We have to make sure we remove the highlight behavior before copying the object
        self._highlighted_objects[-1].remove_behavior_by_type(HighlightBehavior)
        object = self.machine.editor.asset_library.get_asset_copy(self._highlighted_objects[-1])
        self._highlighted_objects.pop()
        self._added_object = True
        if self._select_material and self._target_object is not None:
            if not self.editor.active_object:
                logger.warning("No object is active")
                return

            # We get the first material from the object, and apply it to the active object
            if not object.mesh or not object.mesh.geometries or object.mesh.geometries[0].material is None:
                return
            material = object.mesh.geometries[0].material
            # The material has to be bundled in a Model to be shipped with its textures, if any
            model = Model(root=Object3D(mesh=Mesh(geometries=[Geometry(material=material)])))
            self._machine.client.session.action(SetMaterialAction(object_uuid=self._target_object.uuid, model=model))

            self._machine.edit_object()
        elif not self._select_material:
            # We have to take the object's parent matrix into account (AssetLibrary._asset_root)
            if self._matrix_when_added is not None:
                object.matrix = self._matrix_when_added
                object.matrix_offset = Matrix44.identity()
            else:
                object.matrix_offset = self.machine.editor.matrix * self.machine.editor.asset_library.asset_root.matrix * object.matrix_offset
                object.apply_offset()
            if self._parent_when_added is not None:
                object.parent = self._parent_when_added
                self._machine.client.session.action(AddObject3DAction(object))
            else:
                self._machine.client.session.action(AddModelAction(model=Model(root=object)))
            self._machine.edit_scene()

    def _select_asset(self, args: Tuple[LocalInputMethod, bool]) -> None:
        if not self._check_add_asset_validity(args):
            return
        self._add_asset()

    def _back(self, input: LocalInputMethod) -> None:
        self._on_exit()

    def _toggle_shape(self, input: LocalInputMethod) -> None:
        self.machine.editor.asset_library.toggle_shape()

    def _set_movement(self, movement: Vector3) -> None:
        """
        Movement setter for Rx subscription
        :return: None
        """
        # We don't set the location directly since it needs to be smoothed in `_calculate_movement`
        self._movement = movement

    def _set_rotation(self, rotation: Quaternion) -> None:
        """
        Rotation setter for Rx subscription
        :return: None
        """
        rotation = self._rotation_accumulator * rotation
        rotation = rotation.mul_vector3(Vector3((0.0, 1.0, 0.0)))
        self._rotation_accumulator = Quaternion.from_z_rotation(
            theta=math.atan2(rotation.y, rotation.x) - math.pi / 2.0)

    def _calculate_movement(self, now: float, dt: float) -> None:
        """
        Step method calculating camera/session movement from input values
        :param now: float
        :param dt: float
        :return: None
        """

        multiplier = dt * self._speed_multiplier

        # Horizontal plane movement first
        movement = Vector3([self._movement.x, self._movement.y, 0.]) * multiplier
        # Transform by the current rotation
        rotated_movement = self._rotation_accumulator.mul_vector3(movement)
        # Z axis is independent of camera direction
        rotated_movement.z += self._movement.z * multiplier

        # This has nothing to do with proper physics velocity but is works for now
        self._velocity += rotated_movement

        self.machine.editor.asset_library.location -= rotated_movement
        self.machine.editor.asset_library.rotation = self._rotation_accumulator.inverse

    def _reset_position(self, input: LocalInputMethod) -> None:
        self._velocity = 0.0
        self._speed_multiplier = 2
        self._movement = Vector3()
        self._rotation_accumulator = Quaternion()
        new_matrix = Matrix44.identity()
        new_matrix.translation = self.editor.config['navigation.reset_position']
        self.editor.matrix = new_matrix

    def _speed_up(self, input: LocalInputMethod) -> None:
        self._speed_multiplier *= 2

    def _slow_down(self, input: LocalInputMethod) -> None:
        self._speed_multiplier /= 2
