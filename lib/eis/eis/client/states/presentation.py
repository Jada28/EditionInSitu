from typing import Any, Callable, Optional

from transitions.core import EventData  # type: ignore

from eis.client.input import LocalInputMethod
from eis.client.states.base_eis_state import BaseEISState
from eis.display.components.cursor import Cursor
from eis.display.components.menus.editor_radial_timeline_menu import EditorRadialTimelineMenu
from eis.graph.object_3d import Object3D


class PresentationMenu(EditorRadialTimelineMenu):
    def __init__(self):
        super().__init__(name="Presentation Menu")

        self._first_slide_button = self.add_item("First slide", self._first_slide)
        self._previous_slide_button = self.add_item("Previous slide", self._previous_slide)
        self._next_slide_button = self.add_item("Next slide", self._next_slide)
        self._last_slide_button = self.add_item("Last slide", self._last_slide)
        self.add_item("Hide picker", self._deactivate_picker_input)
        self.add_item("Back", self._back)

    def _back(self, target: Object3D, input: LocalInputMethod) -> None:
        self.editor.machine.welcome()
        self.close()

    def _first_slide(self, target: Object3D, input: LocalInputMethod) -> None:
        self.editor.presentation_manager.first_slide()

    def _previous_slide(self, target: Object3D, input: LocalInputMethod) -> None:
        self.editor.presentation_manager.previous_slide()

    def _next_slide(self, target: Object3D, input: LocalInputMethod) -> None:
        self.editor.presentation_manager.next_slide()

    def _last_slide(self, target: Object3D, input: LocalInputMethod) -> None:
        self.editor.presentation_manager.last_slide()

    def _deactivate_picker_input(self, target: Object3D, input: LocalInputMethod) -> None:
        input.picker_active = False
        self.close()

    def on_opened(self, input: LocalInputMethod) -> None:
        super().on_opened(input)
        input.picker_active = True
        timeline = self.editor.timeline
        self._previous_slide_button.enabled = timeline.time != 0.0 and not timeline.running
        self._next_slide_button.enabled = not timeline.running

    def update_menu_components(self) -> None:
        super().update_menu_components()
        timeline = self.editor.timeline
        self._previous_slide_button.enabled = timeline.time != 0.0 and not timeline.running
        self._next_slide_button.enabled = not timeline.running
        self._first_slide_button.enabled = not timeline.running
        self._last_slide_button.enabled = not timeline.running


class PresentationState(BaseEISState):
    def __init__(
        self,
        *args: Any,
        on_exit: Optional[Callable[[], None]] = None,
        **kwargs: Any
    ) -> None:
        super().__init__(*args, name="presenting", verb="present", recallable=True, **kwargs)

        self._on_exit = on_exit

        # Cursor
        self._cursor = lambda: Cursor(
            selection_chevron=False,
            highlight_on_hover=False,
            label="present",
            detail="",
            color=(0.15, 0.65, 0.95, 1.00),
        )

        # Menu
        self.menu = lambda: PresentationMenu()
        self._inputs_mapping.extend([("navigate", "_navigate", "Navigate (while pressed)"),
                                     ("slow_down", "_previous_slide", "Previous slide"),
                                     ("speed_up", "_next_slide", "Next slide")])

    def enter(self, event: EventData) -> None:
        super().enter(event)
        self.editor.presentation_manager.activate()

    def exit(self, event: EventData) -> None:
        super().exit(event)
        self.editor.presentation_manager.deactivate()

    def _previous_slide(self, input: LocalInputMethod) -> None:
        if self._machine.state_object != self or self.editor.timeline.running:
            return
        self.editor.presentation_manager.previous_slide()

    def _next_slide(self, input: LocalInputMethod) -> None:
        if self._machine.state_object != self or self.editor.timeline.running:
            return
        self.editor.presentation_manager.next_slide()

    def _navigate(self, args: LocalInputMethod) -> None:
        if args[1]:
            if self.editor.synchronized_physical_body is not None:
                self.machine.walk()
            else:
                self.machine.fly()
