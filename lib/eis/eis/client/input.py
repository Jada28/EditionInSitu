from typing import Any, Dict, Optional, TYPE_CHECKING
import time

from rx.subjects import Subject  # type: ignore

from eis.display.components.cursor import Cursor
from eis.input import InputMethod
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3

if TYPE_CHECKING:
    from eis.picker import Picker
    # noinspection PyUnresolvedReferences
    from eis.client.user import LocalEISUser


class LocalInputMethod(InputMethod['LocalEISUser']):
    """
    Client side input method
    Implement input methods on the client by extending this class.
    This is where the input event capturing events happen. In order to
    sync input data with the server, you also need to extend RemoteInputMethod.
    """

    def __init__(self, config: Dict[str, Any], mapping_config: Dict[str, Any], picker: 'Picker') -> None:
        super().__init__(mapping_config=mapping_config)
        self._config = config
        self._picker_active = True
        self._picker: Picker = picker
        self._picker.input = self
        self._cursor: Optional[Cursor] = None

        # To determine inactivity
        self._previous_location = Vector3()
        self._time_passed_since_activity = 0
        self._time_until_disabled = self._config.get("input.time_until_disabled")

        # Input method orientation. Has to be emulated if it does not exist natively (i.e. for mouse)
        self._primary_orientation = Quaternion()
        self._secondary_orientation = Quaternion()

    @property
    def cursor(self) -> Cursor:
        return self._cursor

    @cursor.setter
    def cursor(self, value: Cursor) -> None:
        if self._cursor != value:
            self._cursor = value
        if self._cursor:
            self._cursor.visible = self._picker_active

    @property
    def distance(self) -> float:
        """
        Return the distance value as measured by the input
        """
        return 0.0

    @property
    def picker(self) -> 'Picker':
        return self._picker

    @property
    def picker_active(self) -> bool:
        return self._picker_active

    @picker_active.setter
    def picker_active(self, active: bool) -> None:
        if self._cursor:
            self._cursor.visible = active
        self._picker_active = active

    @property
    def picker_subject(self) -> Optional[Subject]:
        return self._picker.picker_subject

    @property
    def primary_orientation(self) -> Quaternion:
        return self._primary_orientation.copy()

    @property
    def secondary_orientation(self) -> Quaternion:
        return self._secondary_orientation.copy()

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)

        if self._time_until_disabled is not None:
            self._check_for_inactivity()

        if self._picker_active:
            self._picker.step(now=now, dt=dt)

            if self._cursor:
                self._cursor.update_cursor(picker=self._picker)

    def _check_for_inactivity(self) -> None:
        current_location = self._picker.ray_vector
        if abs(current_location[0] - self._previous_location[0]) < 0.1 or abs(current_location[2] - self._previous_location[2]) < 0.1:
            if time.clock() - self._time_passed_since_activity > self._time_until_disabled and self._picker_active:
                self.picker_active = False
        else:
            self._previous_location = current_location
            self._time_passed_since_activity = time.clock()
