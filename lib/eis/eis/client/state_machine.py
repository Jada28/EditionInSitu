import logging
import os

from configparser import ConfigParser
from typing import Any, Dict, Optional

from transitions.extensions import LockedHierarchicalMachine as Machine  # type: ignore

from eis import BASE_PATH, LOCAL_CONFIG_FILENAME
from eis.client.client import EISClient
from eis.client.editor import ClientEditor
from eis.client.states.active import ActiveState
from eis.client.states.base_eis_state import BaseEISState
from eis.client.states.connecting import ConnectingState
from eis.client.states.calibrating import CalibratingState
from eis.client.states.editing_object import EditingObjectState
from eis.client.states.editing_scene import EditingSceneState
from eis.client.states.editing_presentation import EditingPresentationState
from eis.client.states.initializing import InitializingState
from eis.client.states.loading_scene import LoadingSceneState
from eis.client.states.fly import FlyState
from eis.client.states.walk import WalkState
from eis.client.states.presentation import PresentationState
from eis.client.states.assets.selecting_file_assets import SelectingFileAssetsState
from eis.client.states.assets.selecting_satie_plugins import SelectingSatiePluginsState
from eis.client.states.transform.translating import TranslatingState
from eis.client.states.transform.rotating import RotatingState
from eis.client.states.transform.scaling import ScalingState
from eis.client.states.transforming import TransformingState
from eis.client.states.welcoming import WelcomingState
from eis.client.user import LocalEISUser
from eis.constants import DEBUG_MACHINE
from eis.engine import Engine
from eis.engines.sound.engine import SoundEngine
from eis.inputs.htc_vive import HTCVive
from eis.inputs.support.vrpn import VRPN_AVAILABLE
from eis.message import EISMessageParser
from satlib.utils.logger import Logger
from satlib.utils.config_utils import save_json_config
from satnet.adapters.zmq.client import ZMQClient

if VRPN_AVAILABLE:
    from eis.inputs.htc_vive import HTCViveControllerVRPN

logger = logging.getLogger(__name__)


class StateMachine(Machine):  # type: ignore
    """
    EIS State Machine
    This is the state machine controlling the behavior of the EIS client editor.
    It itself owns the client, editor and engine and controls their state (duh, *state* machine).
    """

    def __init__(self, config: Dict[str, Any], config_local: Dict[str, Any]) -> None:
        super().__init__(
            states=['idle'],
            initial='idle',
            send_event=True,  # Send event instead of arguments to callbacks
            auto_transitions=False,  # Does not create `to_state()` automatic transition methods
            # ignore_invalid_triggers=True
        )

        # Keep the local config for saving purposes
        self._config_local = config_local
        self._config = config

        # Setup the engine
        engine_type = config['engine'].get('type', 'Panda3D')
        engine = Engine.instantiate(engine_type, config=config['engine'])
        if engine is None:
            engine = Engine.instantiate('Panda3D', config=config['engine'])
        self._engine = engine

        self._engine.initialize()

        # Setup the sound engine
        sound_engine_type = config['sound_engine'].get('type', 'Satie')
        sound_engine = Engine.instantiate(sound_engine_type, config=config['sound_engine'])
        if sound_engine is None:
            sound_engine = Engine.instantiate('Sound', config=config['sound_engine'])

        self._sound_engine = sound_engine
        self._sound_engine.initialize()

        # Setup the editor
        self._editor = ClientEditor(
            config=config['editor'],
            engine=self._engine,
            machine=self
        )

        # Setup networking
        self._client = EISClient(
            config=config['network'],
            adapter=ZMQClient(message_parser=EISMessageParser(), config=config['network.adapter']),
            editor=self._editor
        )

        # Setup the delegates
        self._editor.delegate = self._client
        self._engine.delegate = self._client

        # Default user
        user = LocalEISUser(config=self._editor.config.get('user', {}))

        # NOTE: This user initialization is only hardcoded for the default user
        #       The day we support multiple users and different input methods
        #       the code in the section should be moved elsewhere.

        # RenderPipeline Engine Mouse & Keyboard Input
        engine = self.engine  # it needs to be local for PyCharm to "cast" by using `isinstance`
        movement_controller = engine.get_movement_controller(
            config=self._editor.config,
            mapping_config=config['controls'],
            picker=engine.get_picker(self.editor)
        )
        if movement_controller is not None:
            movement_controller.picker_active = config['editor'].get('input.mouse_picker.enabled', False)
            user.add_input(movement_controller)

        # HTC Vive VRPN Input
        if VRPN_AVAILABLE and self._editor.config.get('input.vive.enabled'):
            vive = HTCVive(
                primary=HTCViveControllerVRPN(
                    host=self._editor.config.get('input.vive.vrpn.host'),
                    device=self._editor.config.get('input.vive.vrpn.primary.device'),
                    config=self._editor.config
                ),
                secondary=HTCViveControllerVRPN(
                    host=self._editor.config.get('input.vive.vrpn.host'),
                    device=self._editor.config.get('input.vive.vrpn.secondary.device'),
                    config=self._editor.config
                ),
                config=self._editor.config,
                mapping_config=config['controls'],
                picker=self.engine.get_picker(self.editor)
            )

            trackers_host = self._editor.config.get('input.vive.vrpn.host')
            trackers_names = [name.replace(' ', '')
                              for name in self._editor.config.get('input.vive.vrpn.trackers', '').split(",")]
            for name in trackers_names:
                tracker = HTCViveControllerVRPN(
                    host=trackers_host,
                    device=name,
                    config=self._editor.config,
                )
                vive.add_tracker(name, tracker)

            user.add_input(input=vive)

        # Add user now that we are connected, this will sync it with the rest of the world
        self._editor.add_user(user)

        self._current_state: Optional[str] = None
        self._last_state: Optional[str] = None
        self._state_object: Optional[BaseEISState] = None
        self._current_state_recallable = False

        if DEBUG_MACHINE:
            self._logger = Logger(self)

        # States
        active_state = ActiveState(machine=self)
        connecting_state = ConnectingState(machine=self)
        calibrating_state = CalibratingState(machine=self, on_exit=self.back_to_default_state)
        editing_scene_state = EditingSceneState(machine=self)
        editing_object_state = EditingObjectState(machine=self)
        editing_presentation_state = EditingPresentationState(machine=self)
        fly_state = FlyState(machine=self, on_exit=self.recall)
        initializing_state = InitializingState(machine=self)
        loading_scene_state = LoadingSceneState(machine=self)
        presentation_state = PresentationState(machine=self)
        selecting_file_assets = SelectingFileAssetsState(machine=self, on_exit=self.recall)
        selecting_satie_plugins = SelectingSatiePluginsState(machine=self, on_exit=self.recall)
        transforming_state = TransformingState(machine=self)
        walk_state = WalkState(machine=self, on_exit=self.recall)
        welcoming_state = WelcomingState(machine=self)

        # transformation states
        rotating_state = RotatingState(machine=self, on_exit=self.recall)
        scaling_state = ScalingState(machine=self, on_exit=self.recall)
        translating_state = TranslatingState(machine=self, on_exit=self.recall)

        self.add_states([
            active_state,
            connecting_state,
            calibrating_state,
            editing_scene_state,
            editing_object_state,
            editing_presentation_state,
            fly_state,
            initializing_state,
            loading_scene_state,
            presentation_state,
            rotating_state,
            scaling_state,
            selecting_file_assets,
            selecting_satie_plugins,
            transforming_state,
            translating_state,
            walk_state,
            welcoming_state,
        ])

        # Transitions
        transitions = {
            "activate": ("loading_scene", "welcoming", welcoming_state.should_enter),
            "calibrate": ("welcoming", "calibrating", calibrating_state.should_enter),
            "connect": ("idle", "connecting", connecting_state.should_enter),
            "edit_scene": (["welcoming", "fly", "walk", "editing_presentation", "selecting_file_assets", "selecting_satie_plugins", "transforming", "translating", "rotating", "scaling", "editing_object"], "editing_scene", [editing_scene_state.should_enter]),
            "edit_object": (["editing_scene", "fly", "selecting_file_assets", "walk"], "editing_object", editing_object_state.should_enter),
            "edit_presentation": (["editing_scene", "fly", "walk"], "editing_presentation", editing_presentation_state.should_enter),
            "fly": (["welcoming", "editing_scene", "editing_object", "editing_presentation", "transforming"], "fly", fly_state.should_enter),
            "initialize": ("connecting", "initializing", initializing_state.should_enter),
            "load_scene": ("*", "loading_scene", loading_scene_state.should_enter),
            "present": ("welcoming", "presenting", presentation_state.should_enter),
            "ready": ("*", "welcoming", welcoming_state.should_enter),
            "rotate": (["editing_scene", "transforming"], "rotating", rotating_state.should_enter),
            "scale": (["editing_scene", "transforming"], "scaling", scaling_state.should_enter),
            "select_assets": (["editing_scene", "editing_object"], "selecting_file_assets", selecting_file_assets.should_enter),
            "select_satie_plugins": (["editing_scene"], "selecting_satie_plugins", selecting_satie_plugins.should_enter),
            "stop_editing_scene": ("editing_scene", "welcoming", welcoming_state.should_enter),
            "transform": (["editing_scene", "fly", "walk", "translating", "rotating", "scaling"], "transforming", transforming_state.should_enter),
            "translate": (["editing_scene", "transforming"], "translating", translating_state.should_enter),
            "walk": (["welcoming", "editing_scene", "editing_object", "editing_presentation", "transforming"], "walk", walk_state.should_enter),
            "welcome": (["calibrating", "fly", "walk", "editing_scene", "presenting"], "welcoming", welcoming_state.should_enter),
        }

        for name, transition in transitions.items():
            source, dest, conditions = transition
            self.add_transition(trigger=name, source=source, dest=dest, conditions=conditions)

    @property
    def client(self) -> EISClient:
        """
        Client instance owned by the state machine
        :return: EISClient
        """
        return self._client

    @property
    def config(self) -> ConfigParser:
        """
        Local configuration parser
        :return: ConfigParser
        """
        return self._config

    @property
    def editor(self) -> ClientEditor:
        """
        Editor instance owned by the state machine
        :return: ClientEditor
        """
        return self._editor

    @property
    def engine(self) -> Engine:
        """
        Engine instance owned by the state machine
        :return: Engine
        """
        return self._engine

    @property
    def sound_engine(self) -> SoundEngine:
        """
        Sound engine instance owned by the state machine
        :return: SoundEngine
        """
        return self._sound_engine

    @property
    def state_object(self) -> BaseEISState:
        return self._state_object

    def back_to_default_state(self):
        self.welcome()

    def recall(self) -> bool:
        if self._last_state is None or self._last_state == self.state:
            return False
        return self.trigger_state(self._last_state, default_trigger="welcoming")

    def trigger_state(self, state_name, default_trigger=None):
        state = self.get_state(state_name)
        if state is None:
            trigger_name = default_trigger
        elif state.trigger is not None:
            trigger_name = state.trigger
        elif state.verb is not None:
            trigger_name = state.verb
        else:
            trigger_name = default_trigger
        if trigger_name is None:
            logger.warning("Engine: trigger_state has not found a trigger for", state_name, default_trigger)

        try:
            trigger = getattr(self, trigger_name)
        except AttributeError:
            raise NotImplementedError("Class `{}` does not implement `{}`".format(
                self.__class__.__name__, trigger_name))

        if trigger is not None:
            trigger()
            return True
        else:
            return False

    def save_config(self) -> None:
        """
        Save the local configuration
        :return: None
        """
        config_path = os.path.join(BASE_PATH, "config", LOCAL_CONFIG_FILENAME)
        save_json_config(config_path, self._config_local)

    def step(self, now: float, dt: float) -> None:
        """
        Run the state machine
        :param now: float
        :param dt: float
        :return: None
        """

        if DEBUG_MACHINE:
            self._logger.open("Machine Step")

        # NETWORK
        self._client.step(now, dt)

        # If we are in the idle state, either attempt to connect if we were previously connected
        # or stop here, nothing concerns an unconnected machine from here
        if self.state == "idle":
            if DEBUG_MACHINE:
                self._logger.log("Idle, connecting...")

            self.connect()

        # Get the current state, the machine doesn't provide the object,
        # only the string so we have to get the current state manually
        # Also, only allow recall between top-level states in here,
        # container states (like EditingState) manage their own recall memory.
        self._state_object = self.get_state(self.state)

        if self.state != self._current_state and self._state_object is not None:
            # State memory for recall method
            # self._last_state represents the last state that can be recalled to.
            # Non-recallable states will be registered as self._current_state but not as the next self._last_state
            if self._current_state_recallable:
                self._last_state = self._current_state
            self._current_state_recallable = self._state_object.recallable
            self._current_state = self.state

        # Run States
        if DEBUG_MACHINE:
            self._logger.open("Running state", self._state_object, self.state)

        if self._state_object:
            self._state_object.execute(now=now, dt=dt)

        # EDITOR
        self._editor.step(now, dt)

        if DEBUG_MACHINE:
            self._logger.close()
            self._logger.close()
