import logging
from typing import Optional, TYPE_CHECKING

from eis.graph.scene import Scene
from eis.request import RequestId, ResponseId, EISClientRequest, EISServerResponse
from satnet.request import request, response, ResponseCallback

if TYPE_CHECKING:
    from eis.client.session import EISLocalSession
    from eis.server.session import EISRemoteSession

logger = logging.getLogger(__name__)


@request(id=RequestId.OPEN_SCENE)
class OpenScene(EISClientRequest):
    """
    Open scene request
    Request that the servers opens a new scene
    """

    _fields = ['scene']

    def __init__(self, scene: Optional[Scene] = None) -> None:
        super().__init__()
        self.scene = scene

    def handle(self, session: 'EISRemoteSession', respond: ResponseCallback) -> None:
        logger.debug("Received open scene request from {}".format(session))

        def on_loaded(success: bool) -> None:
            respond(SceneOpened(success))

        if self.scene:
            session.editor.load_scene(self.scene, on_loaded=on_loaded)
        else:
            logger.warning("No scene data!")
            respond(SceneOpened(False))


@response(id=ResponseId.SCENE_OPENED)
class SceneOpened(EISServerResponse[OpenScene]):
    _fields = ['success']

    def __init__(self, success: bool = True) -> None:
        super().__init__()
        self.success = success

    def handle(self, request: OpenScene, session: 'EISLocalSession') -> None:
        assert isinstance(request, OpenScene)
        if self.success:
            logger.info("Scene opened successfully!")
        else:
            logger.error("Error while opening scene!")
