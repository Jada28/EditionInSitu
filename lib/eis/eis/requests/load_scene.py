import logging
import os
from typing import Callable, List, Optional, TYPE_CHECKING

from eis.graph.scene import Scene
from eis.request import RequestId, ResponseId, EISClientRequest, EISServerRequest, EISClientResponse, EISServerResponse
from eis.utils.paths import EISPath
from satnet.request import request, response, ResponseCallback

if TYPE_CHECKING:
    from eis.client.session import EISLocalSession
    from eis.server.session import EISRemoteSession

logger = logging.getLogger(__name__)


@request(id=RequestId.LOAD_SCENE)
class LoadScene(EISServerRequest):
    """
    Load scene request
    This server request is used to instruct clients to load the new scene that was opened on the server.
    """

    _fields = ['scene']

    def __init__(self, scene: Optional[Scene] = None) -> None:
        super().__init__()
        self.scene = scene

    def handle(self, session: 'EISLocalSession', respond: ResponseCallback) -> None:
        logger.debug("Received load scene request")

        def on_loaded(success: bool) -> None:
            respond(SceneLoaded(success))

        if self.scene:
            session.editor.machine.load_scene(scene=self.scene, on_loaded=on_loaded)
        else:
            logger.warning("No scene data!")
            respond(SceneLoaded(False))


@response(id=ResponseId.SCENE_LOADED)
class SceneLoaded(EISClientResponse[LoadScene]):
    _fields = ['success']

    def __init__(self, success: bool = True) -> None:
        super().__init__()
        self.success = success

    def handle(self, request: LoadScene, session: 'EISRemoteSession') -> None:
        assert isinstance(request, LoadScene)
        if self.success:
            logger.info("Scene opened successfully on client {}".format(session))
        else:
            logger.error("Error while opening scene on client {}".format(session))


@request(id=RequestId.GET_SCENES)
class ListScenes(EISClientRequest):
    """
    Get scenes request
    This client request is used to ask the server for the list of all saved scenes.
    """

    def __init__(self, callback: Optional[Callable] =None) -> None:
        super().__init__()
        self.callback = callback

    def handle(self, session: 'EISLocalSession', respond: ResponseCallback) -> None:
        logger.debug("Received get scenes request")

        scenes_path = EISPath.get_scenes_path()
        respond(ListedScenes([f for f in os.listdir(scenes_path) if os.path.isfile(os.path.join(scenes_path, f))]))


@response(id=ResponseId.GOT_SCENES)
class ListedScenes(EISServerResponse[ListScenes]):
    """
    Got scenes response
    This server response is used to give all the saved scenes to the requesting client and fill the scene loading menu.
    """

    _fields = ['scenes']

    def __init__(self, scenes: Optional[List[str]] = None) -> None:
        super().__init__()
        self.scenes = scenes

    def handle(self, request: ListScenes, session: 'EISLocalSession') -> None:
        assert isinstance(request, ListScenes)
        request.callback(self.scenes)


@request(id=RequestId.GET_GLTF_SCENES)
class ListGLTFScenes(EISClientRequest):
    """
    Get gltf scenes request
    This client request is used to ask the server for the list of all glTF scenes.
    """

    def __init__(self, callback: Optional[Callable] =None) -> None:
        super().__init__()
        self.callback = callback

    def handle(self, session: 'EISLocalSession', respond: ResponseCallback) -> None:
        logger.debug("Received get scenes request")

        scenes_path = EISPath.get_export_path()
        scenes = []  # List[str]
        subdirs = [p[0] for p in os.walk(scenes_path, followlinks=True)]
        for d in subdirs:
            for f in os.listdir(os.path.join(scenes_path, d)):
                if f.endswith('.gltf'):
                    scenes.append(os.path.join(scenes_path, d, f))
        respond(ListedGLTFScenes(scenes))


@response(id=ResponseId.GOT_GLTF_SCENES)
class ListedGLTFScenes(EISServerResponse[ListGLTFScenes]):
    """
    Got gltf scenes response
    This server response is used to give all the glTF scenes to the requesting client and fill the import menu.
    """

    _fields = ['scenes']

    def __init__(self, scenes: Optional[List[str]] = None) -> None:
        super().__init__()
        self.scenes = scenes

    def handle(self, request: ListGLTFScenes, session: 'EISLocalSession') -> None:
        assert isinstance(request, ListGLTFScenes)
        request.callback(self.scenes)
