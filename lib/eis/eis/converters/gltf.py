import base64
import concurrent.futures
import datetime
import json
import logging
import os
import shutil
import struct
from abc import ABCMeta, abstractmethod
from collections import OrderedDict
from multiprocessing import cpu_count
from typing import Any, Dict, List, Sequence, Optional, TYPE_CHECKING
from uuid import UUID

from PIL import Image
from math import pi, radians
from io import BytesIO

from eis.converters import FormatConverter, converter
from eis.graph import Color
from eis.graph.animation_curve import AnimationCurve
from eis.graph.behaviors.animation_behavior import AnimationBehavior
from eis.graph.camera import Camera
from eis.graph.geometry import Geometry
from eis.graph.light import Light
from eis.graph.material import Material, MaterialTexture
from eis.graph.mesh import Mesh
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from eis.graph.scene import Scene
from eis.graph.texture import Texture
from eis.graph.texture_video import TextureVideo
from eis.utils.paths import EISPath
from io_scene_gltf2.io.com import gltf2_io
from io_scene_gltf2.io.imp.gltf2_io_gltf import glTFImporter
from satmath.euler import Euler
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3
from satmath.vector4 import Vector4

if TYPE_CHECKING:
    # noinspection PyUnresolvedReferences
    from eis.graph import Normal, TexCoord, Vertex

logger = logging.getLogger(__name__)


def remove_none(d: dict) -> dict:
    """Helper function: Remove None values from dictionary."""
    return {key: val for key, val in d.items() if val is not None}


class KHRLightPunctual:
    """Extension for defining lights in gltf. Spec given in:
    https://github.com/KhronosGroup/glTF/blob/master/extensions/2.0/Khronos/KHR_lights_punctual/README.md
    """

    def __init__(self,
                 type: str,
                 name: Optional[str] = None,
                 color: Optional[Sequence[float]] = None,
                 intensity: Optional[float] = None,
                 range: float = None,
                 spot: Optional[Dict[str, float]] = None) -> None:
        self.type = type
        self.name = name or ""
        self.color = color or [1.0, 1.0, 1.0]
        self.intensity = intensity or 1.0
        self.range = range
        self.spot = spot

    @classmethod
    def from_eis_light(cls, light: Light) -> Optional['KHRLightPunctual']:
        """Convert an eis Light object to a gltf Light object."""
        if light.type == Light.LightType.POINT_LIGHT:
            light_type = 'point'
        elif light.type == Light.LightType.SPOTLIGHT:
            light_type = 'spot'
        elif light.type == Light.LightType.SUNLIGHT:
            light_type = 'directional'
        else:
            logger.warning(
                'Unsupported light type {} was found. '
                'Must be one of point, spot, or directional.'
                .format(light.type)
            )
            return None

        light_range = light.radius if light_type in ['point', 'spot'] else None

        if light_range is not None and light_range <= 0:
            logger.warning((
                'Light radius must be greater than zero. '
                'Radius {} was found.'.format(light_range)
            ))
            light_range = 0

        if light_type == 'spot':
            if light.spot_fov is None:
                angle = pi / 4.0
            else:
                angle = radians(light.spot_fov)
                if angle > pi / 2.0:
                    logger.warning((
                        'Spot light angle must be less than or equal to '
                        'PI / 2.0. Angle {} was found'.format(angle)
                    ))
                    angle = pi / 2.0
                elif angle < 0:
                    logger.warning((
                        'Spot light angle must be greater than or equal to '
                        '0. Angle {} was found.'.format(angle)
                    ))
                    angle = 0.0

            spot = {
                # EIS lights use a single angle for spot lights, so we leave
                # the inner cone angle as the default 0.
                'innerConeAngle': 0.0,
                'outerConeAngle': angle
            }
        else:
            spot = None

        color = light.color[0:3] if light.color is not None else None

        return cls(
            type=light_type,
            name=light.name,
            color=color,
            intensity=light.energy,
            range=light_range,
            spot=spot
        )

    def to_eis_light(self, node: gltf2_io.Node) -> Optional['Light']:
        """Convert a gltf Light to an EiS Light object.
        Returns None if gltf light is not valid.
        """
        if self.type == 'point':
            light_type = Light.LightType.POINT_LIGHT
        elif self.type == 'spot':
            light_type = Light.LightType.SPOTLIGHT
        elif self.type == 'directional':
            light_type = Light.LightType.SUNLIGHT
        else:
            logger.warning(
                'Unsupported light type {} was found. '
                'Must be one of point, spot, or directional.'.format(self.type)
            )
            return None

        radius = self.range if light_type in ['point', 'spot'] else None

        spot_fov = None
        if light_type == 'spot':
            if self.spot is not None:
                spot_fov = self.spot.get('outerConeAngle')
            else:
                logger.warning(
                    'Spot lights require a spot property. None was found.'
                )

        color = self.color.append(1.0) if self.color is not None else None

        return Light(
            type=light_type,
            radius=radius,
            energy=self.intensity,
            color=color,
            spot_fov=spot_fov,
            matrix=Matrix44(node.matrix),
            name=self.name
        )

    @classmethod
    def from_dict(cls, content: Dict) -> Optional['KHRLightPunctual']:
        light_type = content.get('type')
        name = content.get('name')
        color = content.get('color')
        intensity = content.get('intensity')
        range = content.get('range')
        spot = content.get('spot')

        if light_type is None:
            logger.warning((
                'GLTF Lights require a "type" property. '
                'Must be one of point, spot, directional. Found None.'
            ))
            return None

        elif light_type not in ['point', 'spot', 'directional']:
            logger.warning((
                'Light type must be one of point, spot, directional. '
                'Found {}.'.format(light_type)
            ))
            return None

        return cls(
            type=light_type,
            name=content.get('name'),
            color=content.get('color'),
            intensity=intensity,
            range=range,
            spot=spot
        )

    def to_dict(self) -> Dict[str, Any]:
        return self.__dict__


class GLTFExporter:
    def __init__(self, scene: Scene, filename: str) -> None:
        self._scene = scene
        self._model = scene.model
        self._materials: List[gltf2_io.Material] = []
        self._material_map: Dict[UUID, int] = dict()
        self._meshes: List[gltf2_io.Mesh] = []
        self._mesh_map: Dict[UUID, int] = dict()
        self._textures: List[gltf2_io.Texture] = []
        self._texture_map: Dict[UUID, int] = dict()
        self._nodes: List[gltf2_io.Node] = []
        self._buffers: List[gltf2_io.Buffer] = []
        self._bufferviews: List[gltf2_io.BufferView] = []
        self._images: List[gltf2_io.Image] = []
        self._accessors: List[gltf2_io.Accessor] = []
        self._lights: List[KHRLightPunctual] = []
        self._node_children: Dict[int, List[int]] = dict()
        self._animations: List[gltf2_io.Animation] = []
        self._animation_channels: List[gltf2_io.AnimationChannel] = []
        self._animation_samplers: List[gltf2_io.AnimationSampler] = []
        self._animation_map: Dict[UUID, int] = dict()
        self._cameras: List[gltf2_io.Camera] = []

        self._export_folder = os.path.join(EISPath.get_export_path(), filename)

        # Remove old exports and recreate the empty folder
        if os.path.exists(self._export_folder):
            shutil.rmtree(self._export_folder)
        os.makedirs(os.path.join(self._export_folder, 'res'))

        logger.debug("Preprocessing scene graph for glTF export...")
        self._preprocess_graph()
        logger.debug('...scene graph preprocessing done.')

    def _parse_node(self, eis_node: Object3D, node_index: int, parent_index: int,
                    animation_index: int, light_index: int, camera_index: int,
                    node_count: int) -> None:
        """Recursive function to parse all nodes in the scene graph using DFS.
        """
        if eis_node and eis_node.behaviors:
            behaviors = [behavior for behavior in eis_node.behaviors
                         if type(behavior) == AnimationBehavior]
        else:
            behaviors = []
        for animation_behavior in behaviors:
            channels: List[gltf2_io.AnimationChannel] = []
            samplers: List[gltf2_io.AnimationSampler] = []
            sampler_index = 0

            for curve_id in animation_behavior.curves_id:
                animation_index = self._animation_map[animation_behavior.curves_id[curve_id]]
                animation_channel = self._animation_channels[animation_index]
                animation_sampler = self._animation_samplers[animation_index]
                animation_channel.sampler = sampler_index
                sampler_index += 1

                animation_channel.target_node = node_index
                if curve_id == 'location':
                    animation_channel.target_path = 'translation'
                elif curve_id == 'rotation':
                    animation_channel.target_path = 'rotation'
                elif curve_id == 'scale':
                    animation_channel.target_path = 'scale'
                else:
                    animation_channel.target_path = curve_id

                channels.append(animation_channel)
                samplers.append(animation_sampler)

            animation = gltf2_io.Animation(
                channels=channels,
                extensions=None,
                extras=None,
                name=None,
                samplers=samplers
            )
            animation_index += 1
            self._animations.append(animation)

        # Add this node to it's parent's list of children.
        if parent_index != -1:
            node_children = self._node_children.get(parent_index)
            if node_children is not None:
                node_children.append(node_index)
            else:
                self._node_children[parent_index] = [node_index]

        for child_node in eis_node.children if eis_node else []:
            if isinstance(child_node, Light):
                # We need to make a KHRLightPunctual object, as well as a
                # node to reference the light.
                gltf_node = gltf2_io.Node(
                    camera=None,
                    children=None,
                    extensions={
                        'KHR_lights_punctual': {
                            'light': light_index
                        }
                    },
                    extras=None,
                    matrix=list(child_node.matrix.flatten()) if child_node.matrix is not None else None,
                    mesh=None,
                    name=child_node.name,
                    rotation=None,
                    scale=None,
                    skin=None,
                    translation=None,
                    weights=None
                )
                light_index += 1

                light = KHRLightPunctual.from_eis_light(child_node)
                if light is not None:
                    self._lights.append(light)

            elif isinstance(child_node, Camera):
                camera = gltf2_io.Camera(
                    extensions=None,
                    extras=None,
                    name=child_node.name,
                    orthographic=None,
                    perspective=None,
                    type='perspective'
                )
                camera_index += 1
                self._cameras.append(camera)

            else:
                if isinstance(child_node, Object3D) and child_node.mesh:
                    mesh_index = self._mesh_map[child_node.mesh.uuid]
                    gltf_node = gltf2_io.Node(
                        camera=None,
                        children=None,
                        extensions=None,
                        extras=None,
                        matrix=list(child_node.matrix.flatten()) if child_node.matrix is not None else None,
                        mesh=mesh_index,
                        name=child_node.name,
                        rotation=None,
                        scale=None,
                        skin=None,
                        translation=None,
                        weights=None
                    )
                else:
                    gltf_node = gltf2_io.Node(
                        camera=None,
                        children=None,
                        extensions=None,
                        extras=None,
                        matrix=list(child_node.matrix.flatten()) if child_node.matrix is not None else None,
                        mesh=None,
                        name=child_node.name,
                        rotation=None,
                        scale=None,
                        skin=None,
                        translation=None,
                        weights=None
                    )

            node_count += 1
            self._nodes.append(gltf_node)
            self._parse_node(
                child_node,
                # Increment node index.
                node_index=node_count,
                # Current node is the next node's parent.
                parent_index=node_index,
                animation_index=animation_index,
                light_index=light_index,
                camera_index=camera_index,
                node_count=node_count
            )

    def _preprocess_graph(self) -> None:
        """Process the entire scene graph by recursively calling _parse_node,
        starting at the root node.
        """
        self._parse_scene_model()

        # We provide a rotation matrix for the root because glTF uses a y-up
        # coordinate system and EiS uses a z-up one.
        root = self._model.root
        root_matrix = Matrix44.from_axis_rotation(
            Vector3((1.0, 0.0, 0.0)),
            pi / 2.0
        )
        root_node = gltf2_io.Node(
            camera=None,
            children=None,
            extensions=None,
            extras=None,
            matrix=list(root_matrix.flatten()),
            mesh=None,
            name=None,
            rotation=None,
            scale=None,
            skin=None,
            translation=None,
            weights=None
        )
        self._nodes.append(root_node)

        # After this call we have an ordered list of traversed nodes and the
        # associated indexes of each node's children.
        self._parse_node(
            root,
            node_index=0,
            parent_index=-1,
            animation_index=0,
            light_index=0,
            camera_index=0,
            node_count=0
        )

    def _parse_scene_model(self) -> None:
        logger.debug('Preprocessing scene_graph...')

        self._animation_map = {item.uuid: index for (item, index) in zip(
            self._model.animation_curves, range(len(self._model.animation_curves)))}

        self._mesh_map = {item.uuid: index for (item, index) in zip(self._model.meshes, range(len(self._model.meshes)))}

        self._texture_map = {item.uuid: index for (item, index) in zip(
            self._model.textures, range(len(self._model.textures)))}

        self._material_map = {item.uuid: index for (item, index) in zip(
            self._model.materials, range(len(self._model.materials)))}

        for mat in self._model.materials:
            base_color_texture_index: Optional[int] = None
            roughness_texture_index: Optional[int] = None
            normal_texture_index: Optional[int] = None
            for mat_tex in mat.material_textures:
                if mat_tex.texture_id is not None:
                    texture_index = self._texture_map[mat_tex.texture_id]
                    if mat_tex.usage == MaterialTexture.TextureUsage.BASE_COLOR:
                        base_color_texture_index = texture_index
                    elif mat_tex.usage == MaterialTexture.TextureUsage.NORMAL:
                        normal_texture_index = texture_index
                    elif mat_tex.usage == MaterialTexture.TextureUsage.ROUGHNESS:
                        roughness_texture_index = texture_index
                    else:
                        logger.warning(
                            'Unsupported material texture usage type {}'.format(mat_tex.usage)
                        )

            if mat.shading_model == Material.ShadingModel.EMISSIVE:
                emissive_factor = mat.color[0:3]
            else:
                emissive_factor = None

            if roughness_texture_index is not None:
                metallic_roughness_texture = gltf2_io.TextureInfo(
                    extensions=None,
                    extras=None,
                    index=roughness_texture_index,
                    tex_coord=None
                )
            else:
                metallic_roughness_texture = None

            if base_color_texture_index is not None:
                base_color_texture = gltf2_io.TextureInfo(
                    extensions=None,
                    extras=None,
                    index=base_color_texture_index,
                    tex_coord=None
                )
            else:
                base_color_texture = None

            if normal_texture_index is not None:
                normal_texture = gltf2_io.MaterialNormalTextureInfoClass(
                    extensions=None,
                    extras=None,
                    index=normal_texture_index,
                    scale=None,
                    tex_coord=None
                )
            else:
                normal_texture = None

            pbr_metallic_roughness = gltf2_io.MaterialPBRMetallicRoughness(
                base_color_factor=list(mat.color),
                base_color_texture=base_color_texture,
                extensions=None,
                extras=None,
                metallic_factor=mat.metallic,
                metallic_roughness_texture=metallic_roughness_texture,
                roughness_factor=mat.roughness
            )

            self._materials.append(gltf2_io.Material(
                alpha_cutoff=None,
                alpha_mode=None,
                double_sided=None,
                emissive_factor=emissive_factor,
                emissive_texture=None,
                extensions=None,
                extras=None,
                name=mat.name,
                normal_texture=normal_texture,
                occlusion_texture=None,
                pbr_metallic_roughness=pbr_metallic_roughness
            ))

        logger.debug('Dumping geometry and animation data to a bytes buffer...')
        self._create_buffer()
        logger.debug('... geometry and animation done.')

        logger.debug('Dumping textures to images...')
        with concurrent.futures.ThreadPoolExecutor(max_workers=cpu_count()) as executor:
            for tex in self._model.textures:
                mesh_index = self._texture_map[tex.uuid]
                uri = os.path.join('res', '{}.png'.format(tex.name or 'texture_{}'.format(mesh_index)))
                texture = gltf2_io.Texture(
                    extensions=None,
                    extras=None,
                    name=None,
                    sampler=None,
                    source=mesh_index
                )
                self._textures.append(texture)
                if isinstance(tex, TextureVideo):
                    uri = tex.video_uri
                else:
                    executor.submit(
                        fn=GLTFExporter._save_texture_image,
                        tex=tex,
                        path=os.path.join(self._export_folder, uri)
                    ).result()
                # We use the same index because there is a 1-1 relation between textures and images in our case
                #  since we use a bytes buffer for vertex/index/normal/texcoord information.
                self._images.append(gltf2_io.Image(
                    buffer_view=None,
                    extensions=None,
                    extras=None,
                    mime_type=None,
                    name=None,
                    uri=uri
                ))

        logger.debug('... textures saved.')

    def _create_buffer(self) -> None:
        color_index: Optional[int] = None
        texcoords_index: Optional[int] = None
        vertex_index: Optional[int] = None
        indices_index: Optional[int] = None
        normal_index: Optional[int] = None

        with open(os.path.join(self._export_folder, 'data_buffer.bin'), 'wb') as data_buffer:
            buffer_length = 0

            # Write buffer for the animation samplers
            for animation_curve in self._model.animation_curves:
                keyframe_count = len(animation_curve.keyframes)
                if keyframe_count > 0:
                    keyframe_length = len(animation_curve.keyframes[0][1])
                else:
                    keyframe_length = 0

                # Write animation timings
                timings_byte_length = 0
                for frame in animation_curve.keyframes:
                    timings_byte_length += data_buffer.write(struct.pack('f', frame[0]))

                timings_index = len(self._bufferviews)
                self._accessors.append(gltf2_io.Accessor(
                    buffer_view=timings_index,
                    byte_offset=0,
                    component_type=5126,
                    count=keyframe_count,
                    extensions=None,
                    extras=None,
                    max=None,
                    min=None,
                    name=None,
                    normalized=None,
                    sparse=None,
                    type='SCALAR'
                ))
                self._bufferviews.append(gltf2_io.BufferView(
                    buffer=0,
                    byte_length=timings_byte_length,
                    byte_offset=buffer_length,
                    byte_stride=None,
                    extensions=None,
                    extras=None,
                    name=None,
                    target=None
                ))

                buffer_length += timings_byte_length

                # Write animation keys
                keys_byte_length = 0
                for frame in animation_curve.keyframes:
                    keys_byte_length += data_buffer.write(struct.pack('f' * keyframe_length, *[0.0] * keyframe_length))
                    keys_byte_length += data_buffer.write(struct.pack('f' * keyframe_length, *frame[1]))
                    keys_byte_length += data_buffer.write(struct.pack('f' * keyframe_length, *[0.0] * keyframe_length))

                keys_index = len(self._bufferviews)
                data_type = ('VEC' + str(keyframe_length)
                             if keyframe_length > 1 else 'SCALAR')
                self._accessors.append(gltf2_io.Accessor(
                    buffer_view=keys_index,
                    byte_offset=0,
                    component_type=5126,
                    count=keyframe_count * keyframe_length,
                    extensions=None,
                    extras=None,
                    max=None,
                    min=None,
                    name=None,
                    normalized=None,
                    sparse=None,
                    type=data_type
                ))
                self._bufferviews.append(gltf2_io.BufferView(
                    buffer=0,
                    byte_length=keys_byte_length,
                    byte_offset=buffer_length,
                    byte_stride=None,
                    extensions=None,
                    extras=None,
                    name=None,
                    traget=None
                ))

                buffer_length += keys_byte_length

                # Create the animation objects
                if animation_curve.interpolation == AnimationCurve.InterpolationType.CONSTANT:
                    sampler_interpolation = 'STEP'
                elif animation_curve.interpolation == AnimationCurve.InterpolationType.LINEAR:
                    sampler_interpolation = 'LINEAR'
                elif animation_curve.interpolation == AnimationCurve.InterpolationType.SMOOTH:
                    sampler_interpolation = 'CUBICSPLINE'
                else:
                    logger.warning('Unsupported interpolation type {}'.format(animation_curve.interpolation))
                    sampler_interpolation = 'LINEAR'

                animation_sampler_export = gltf2_io.AnimationSampler(
                    extensions=None,
                    extras=None,
                    input=timings_index,
                    interpolation=sampler_interpolation,
                    output=keys_index
                )

                sampler_index = len(self._animation_samplers)
                self._animation_samplers.append(animation_sampler_export)

                animation_channel_export = gltf2_io.AnimationChannel(
                    extensions=None,
                    extras=None,
                    sampler=sampler_index,
                    target=None
                )
                self._animation_channels.append(animation_channel_export)

            # We are going to write the buffer file with the mesh information along with linking each mesh to its data
            for mesh in self._model.meshes:
                mesh_index = self._mesh_map[mesh.uuid]
                mesh_export = gltf2_io.Mesh(extensions=None, extras=None,
                                            name=None, primitives=[],
                                            weights=None)
                self._meshes.append(mesh_export)
                for geom in mesh.geometries:
                    # We have a 1-1 relationship between accessors and bufferviews in our case so we use the length
                    # of the bufferviews list for the accessors index too
                    if geom.vertex_count:
                        vertex_index = len(self._bufferviews)

                        vertex_byte_length = 0
                        for vertex in geom.vertices:
                            vertex_byte_length += data_buffer.write(struct.pack('fff', *vertex))

                        self._accessors.append(gltf2_io.Accessor(
                            buffer_view=vertex_index,
                            byte_offset=0,
                            component_type=5126,
                            count=geom.vertex_count,
                            extensions=None,
                            extras=None,
                            max=[max(v) for v in zip(*geom.vertices)],
                            min=[min(v) for v in zip(*geom.vertices)],
                            name=None,
                            normalized=None,
                            sparse=None,
                            type='VEC3'
                        ))
                        self._bufferviews.append(gltf2_io.BufferView(
                            buffer=0,
                            byte_length=vertex_byte_length,
                            byte_offset=buffer_length,
                            byte_stride=None,
                            extensions=None,
                            extras=None,
                            name=None,
                            target=None
                        ))

                        buffer_length += vertex_byte_length

                        # Now for the indices
                        indices_byte_length = 0
                        for indices in geom.primitives:
                            indices_byte_length += data_buffer.write(struct.pack('III', *indices))

                        indices_index = len(self._bufferviews)
                        self._accessors.append(gltf2_io.Accessor(
                            buffer_view=indices_index,
                            byte_offset=0,
                            component_type=5125,
                            count=len(geom.primitives) * 3,
                            extensions=None,
                            extras=None,
                            max=None,
                            min=None,
                            name=None,
                            normalized=None,
                            sparse=None,
                            type='SCALAR'
                        ))
                        self._bufferviews.append(gltf2_io.BufferView(
                            buffer=0,
                            byte_length=indices_byte_length,
                            byte_offset=buffer_length,
                            byte_stride=None,
                            extensions=None,
                            extras=None,
                            name=None,
                            target=None
                        ))

                        buffer_length += indices_byte_length

                    normals_count = len(geom.normals)
                    if normals_count:
                        normal_index = len(self._bufferviews)

                        normal_byte_length = 0
                        for normal in geom.normals:
                            normal_byte_length += data_buffer.write(struct.pack('fff', *normal))

                        self._accessors.append(gltf2_io.Accessor(
                            buffer_view=normal_index,
                            byte_offset=0,
                            component_type=5126,
                            count=normals_count,
                            extensions=None,
                            extras=None,
                            max=[max(v) for v in zip(*geom.normals)],
                            min=[min(v) for v in zip(*geom.normals)],
                            name=None,
                            normalized=None,
                            sparse=None,
                            type='VEC3'
                        ))
                        self._bufferviews.append(gltf2_io.BufferView(
                            buffer=0,
                            byte_length=normal_byte_length,
                            byte_offset=buffer_length,
                            byte_stride=None,
                            extensions=None,
                            extras=None,
                            name=None,
                            target=None
                        ))

                        buffer_length += normal_byte_length

                    material_index = self._material_map.get(geom.material_id)

                    texcoords_count = len(geom.texcoords)
                    if texcoords_count:
                        texcoords_index = len(self._bufferviews)

                        texcoords_byte_length = 0
                        for coord in geom.texcoords:
                            # We swap the lines vertically because it seems the tex
                            texcoords_byte_length += data_buffer.write(struct.pack('ff', *coord))

                        self._accessors.append(gltf2_io.Accessor(
                            buffer_view=texcoords_index,
                            byte_offset=0,
                            component_type=5126,
                            count=texcoords_count,
                            extensions=None,
                            extras=None,
                            max=None,
                            min=None,
                            name=None,
                            normalized=None,
                            sparse=None,
                            type='VEC2'
                        ))
                        self._bufferviews.append(gltf2_io.BufferView(
                            buffer=0,
                            byte_length=texcoords_byte_length,
                            byte_offset=buffer_length,
                            byte_stride=None,
                            extensions=None,
                            extras=None,
                            name=None,
                            target=None
                        ))

                        buffer_length += texcoords_byte_length

                    color_count = len(geom.colors)
                    if color_count:
                        color_index = len(self._bufferviews)

                        color_byte_length = 0
                        for color in geom.colors:
                            color_byte_length += data_buffer.write(struct.pack('fff', *color[0:3]))

                        self._accessors.append(gltf2_io.Accessor(
                            buffer_view=color_index,
                            byte_offset=0,
                            component_type=5126,
                            count=color_count,
                            extensions=None,
                            extras=None,
                            max=None,
                            min=None,
                            name=None,
                            normalized=None,
                            sparse=None,
                            type='VEC3'
                        ))
                        self._bufferviews.append(gltf2_io.BufferView(
                            buffer=0,
                            byte_length=color_byte_length,
                            byte_offset=buffer_length,
                            byte_stride=None,
                            extensions=None,
                            extras=None,
                            name=None,
                            target=None
                        ))

                        buffer_length += color_byte_length

                    # Built mesh primitive attribute dict
                    attributes = dict()
                    if color_index is not None:
                        attributes['COLOR_0'] = color_index
                    if vertex_index is not None:
                        attributes['POSITION'] = vertex_index
                    if normal_index is not None:
                        attributes['NORMAL'] = normal_index
                    if texcoords_index is not None:
                        attributes['TEXCOORD_0'] = texcoords_index
                    mesh_export.primitives.append(gltf2_io.MeshPrimitive(
                        attributes=attributes,
                        extensions=None,
                        extras={'name': geom.name},
                        indices=indices_index,
                        material=material_index,
                        # Default mode: triangles.
                        mode=4,
                        targets=None
                    ))
            self._buffers.append(gltf2_io.Buffer(
                byte_length=buffer_length,
                extensions=None,
                extras=None,
                name=None,
                uri='data_buffer.bin'
            ))

    def export(self) -> None:
        logger.debug("Starting glTF export...")
        content: Dict[str, Any] = dict()

        scene_name = self._scene.name.replace(' ', '_')
        content['scene'] = 0
        # We only have one root object in the scene
        content['scenes'] = [{'name': scene_name, 'nodes': [0]}]
        if self._animations:
            content['animations'] = []
            for animation in self._animations:
                content['animations'].append(remove_none(animation.to_dict()))

        if self._nodes:
            content['nodes'] = []
            for i, node in enumerate(self._nodes):
                node_content = remove_none(node.to_dict())
                node_children = self._node_children.get(i)
                if node_children is not None:
                    node_content['children'] = node_children
                content['nodes'].append(node_content)

        if self._lights:
            lights = [remove_none(light.to_dict()) for light in self._lights]
            content['extensions'] = {
                'KHR_lights_punctual': {
                    'lights': lights
                }
            }

        if self._cameras:
            content['cameras'] = []
            for cam in self._cameras:
                content['cameras'].append(remove_none(cam.to_dict()))

        if self._materials:
            content['materials'] = []
            for mat in self._materials:
                content['materials'].append(remove_none(mat.to_dict()))

        if self._meshes:
            content['meshes'] = []
            for mesh in self._meshes:
                content['meshes'].append(remove_none(mesh.to_dict()))

        if self._textures:
            content['textures'] = []
            for tex in self._textures:
                content['textures'].append(remove_none(tex.to_dict()))

        if self._buffers:
            content['buffers'] = []
            for buffer in self._buffers:
                content['buffers'].append(remove_none(buffer.to_dict()))

        if self._bufferviews:
            content['bufferViews'] = []
            for bufferView in self._bufferviews:
                content['bufferViews'].append(remove_none(bufferView.to_dict()))

        if self._images:
            content['images'] = []
            for image in self._images:
                content['images'].append(remove_none(image.to_dict()))

        if self._accessors:
            content['accessors'] = []
            for accessor in self._accessors:
                content['accessors'].append(remove_none(accessor.to_dict()))

        content['asset'] = {'version': '2.0'}

        # Set default gltf file name if model_name is empty.
        date = datetime.datetime.now()
        name = (
            scene_name
            or f'exported_gltf_scene_{date.day}_{date.month}_{date.year}'
        )
        with open(os.path.join(self._export_folder, '{}.gltf'.format(name)), 'w') as export_file:
            content_ordered = OrderedDict(
                [(key, content[key]) for key in sorted(content)]
            )

            export_file.write(json.dumps(
                content_ordered,
                indent=2,
                separators=(',', ': ')
            ))

        logger.debug("... glTF export done.")

    @staticmethod
    def _save_texture_image(tex: Texture, path: str) -> None:
        img = Image.new(tex.format.upper(), (tex.size_x, tex.size_y))
        img.frombytes(tex.data)
        img.save(path, 'PNG')


class GLTFImporter:
    def __init__(self, path: str) -> None:
        self._root_index: int = -1
        self._path = path
        self._dirpath = os.path.dirname(path)
        self._materials: List[gltf2_io.Material] = []
        self._eis_materials: List[Material] = []
        self._meshes: List[gltf2_io.Mesh] = []
        self._eis_meshes: List[Mesh] = []
        self._textures: List[gltf2_io.Texture] = []
        self._eis_textures: List[Texture] = []
        self._nodes: List[gltf2_io.Node] = []
        self._eis_objects: List[Object3D] = []
        self._buffers: List[gltf2_io.Buffer] = []
        self._buffer_data: Dict[str, bytes] = dict()
        self._bufferviews: List[gltf2_io.BufferView] = []
        self._images: List[gltf2_io.Image] = []
        self._lights: List[KHRLightPunctual] = []
        self._eis_lights: List[Light] = []
        self._accessors: List[gltf2_io.Accessor] = []
        self._animations: List[gltf2_io.Animation] = []
        self._cameras: List[gltf2_io.Camera] = []

    def _read_buffers(self, uri: str) -> None:
        buffer_data: Optional[bytes] = None

        if uri.startswith('data:application/octet-stream;base64'):
            buffer_data = base64.b64decode(uri[37:])
        elif uri.startswith('data:application'):
            logger.warning('Unsupported data buffer type {}'.format(uri[0:uri.find(';')]))
        else:
            try:
                with open(os.path.join(self._dirpath, uri), 'r+b') as f:
                    buffer_data = f.read()
            except IOError as e:
                logger.warning('Failed to read data from buffer at uri {}: {}'.format(uri, e))

        if buffer_data:
            self._buffer_data[uri] = buffer_data

    def _parse_buffer_data(self, data_format: str, accessor: gltf2_io.Accessor,
                           step: Optional[int] = None) -> Optional[List[Any]]:
        bufferview = self._bufferviews[accessor.buffer_view]
        list_buffer: List[Any] = []

        buffer_data = self._buffer_data.get(self._buffers[bufferview.buffer].uri)
        if not buffer_data:
            return None

        buffer_offset = (bufferview.byte_offset or 0) + (accessor.byte_offset or 0)
        byte_stride = bufferview.byte_stride
        # Default stride to data_format size.
        if byte_stride is None:
            byte_stride = struct.calcsize(data_format)

        for index in range(0, accessor.count, step or 1):
            list_buffer.append(struct.unpack_from(
                data_format,
                buffer_data,
                buffer_offset
            ))
            buffer_offset += byte_stride

        return list_buffer

    def _parse_buffer_image(self, bufferview: gltf2_io.BufferView) -> Optional[bytes]:
        buffer_data = self._buffer_data.get(self._buffers[bufferview.buffer].uri)
        if not buffer_data:
            return None
        return buffer_data[bufferview.byte_offset:bufferview.byte_offset + bufferview.byte_length]

    def _parse_json(self, gltf_data: gltf2_io.Gltf) -> Optional[str]:
        scene = gltf_data.scene or 0
        scenes = gltf_data.scenes

        if scenes is None or not len(scenes):
            logger.warning('Trying to import a glTF file with no scene.')
            return None

        # If there is no unique root, create a root Object3D and attach all the top-level objects to it.
        # We rotate to switch from y-up to z-up coordinates system
        scene_nodes = scenes[scene].nodes
        if scene_nodes and len(scene_nodes) == 1:
            self._root_index = scene_nodes[0]

        for node in gltf_data.nodes or []:
            if node:
                self._nodes.append(node)

        for img in gltf_data.images or []:
            if img:
                self._images.append(img)

        for tex in gltf_data.textures or []:
            if tex:
                self._textures.append(tex)

        for material in gltf_data.materials or []:
            if material:
                self._materials.append(material)

        for mesh in gltf_data.meshes or []:
            if mesh:
                self._meshes.append(mesh)

        for buffer in gltf_data.buffers or []:
            if buffer and buffer.uri:
                self._read_buffers(buffer.uri)
                self._buffers.append(buffer)

        for bufferview in gltf_data.buffer_views or []:
            if bufferview:
                self._bufferviews.append(bufferview)

        if gltf_data.extensions and gltf_data.extensions.get('KHR_lights_punctual'):
            lights = gltf_data.extensions.get('KHR_lights_punctual').get('lights')
            for content in lights or []:
                light = KHRLightPunctual.from_dict(content)
                if light:
                    self._lights.append(light)

        for accessor in gltf_data.accessors or []:
            if accessor:
                self._accessors.append(accessor)

        for animation in gltf_data.animations or []:
            if animation:
                self._animations.append(animation)

        for camera in gltf_data.cameras or []:
            if camera:
                self._cameras.append(camera)

        return scenes[scene].name or ''

    def _create_scene_graph(self) -> None:
        for tex in self._textures:
            if tex.source is None:
                continue

            image = None
            image_uri = self._images[tex.source].uri
            buffer_view = self._images[tex.source].buffer_view
            mime_type = self._images[tex.source].mime_type

            try:
                if image_uri is not None:
                    if image_uri.startswith('data:image/png;base64'):
                        image_buffer = base64.b64decode(image_uri[22:])
                        image = Image.open(BytesIO(image_buffer))
                    elif image_uri.startswith('data:image/jpeg;base64'):
                        image_buffer = base64.b64decode(image_uri[23:])
                        image = Image.open(BytesIO(image_buffer))
                    elif image_uri.startswith('data:image'):
                        logger.warning('Unsupported image buffer type {}'.format(image_uri[0:image_uri.find(';')]))
                        continue
                    else:
                        image = Image.open(os.path.join(self._dirpath, image_uri))
                elif buffer_view is not None and buffer_view < len(self._bufferviews):
                    image_buffer = self._parse_buffer_image(self._bufferviews[buffer_view])
                    if image_buffer is None:
                        continue
                    if mime_type in ['image/png', 'image/jpeg']:
                        image = Image.open(BytesIO(image_buffer))
                    else:
                        logger.warning(f'Unsupported image buffer type {mime_type}')
                        continue
            except FileNotFoundError:
                logger.warning(f'Could not find file {os.path.join(self._dirpath, image_uri)}')
            except OSError:
                logger.warning(f'Could not load file {os.path.join(self._dirpath, image_uri)}')

            if image is not None:
                image = image.convert('RGB') if image.mode == 'P' else image
                size_x, size_y = image.size
                self._eis_textures.append(Texture(
                    name=tex.name,
                    data=image.tobytes(),
                    format=image.mode.lower(),
                    size_x=size_x,
                    size_y=size_y
                ))

        for mat in self._materials:
            # Create an EiS Material.
            material: Material = Material(name=mat.name)

            # EiS Material property defaults.
            roughness = 1.0
            metallic = 0.0
            color = (1.0, 1.0, 1.0, 1.0)

            if mat.pbr_metallic_roughness is not None:
                pbr = mat.pbr_metallic_roughness

                if pbr.base_color_texture is not None:
                    texture = pbr.base_color_texture
                    if texture.index < len(self._eis_textures):
                        material.add_texture(MaterialTexture(
                            texture=self._eis_textures[texture.index],
                            usage=MaterialTexture.TextureUsage.BASE_COLOR
                        ))

                if pbr.metallic_roughness_texture is not None:
                    texture = pbr.metallic_roughness_texture
                    material.add_texture(MaterialTexture(
                        texture=self._eis_textures[texture.index],
                        usage=MaterialTexture.TextureUsage.ROUGHNESS
                    ))

                if pbr.base_color_factor is not None:
                    color = pbr.base_color_factor

                if pbr.roughness_factor is not None:
                    roughness = pbr.roughness_factor

                if pbr.metallic_factor is not None:
                    metallic = pbr.metallic_factor

            if mat.normal_texture is not None:
                material.add_texture(MaterialTexture(
                    texture=self._eis_textures[mat.normal_texture.index],
                    usage=MaterialTexture.TextureUsage.NORMAL
                ))

            # Set EiS Material properties.
            if (mat.emissive_factor is not None
                    and mat.emissive_factor != [0.0, 0.0, 0.0]):
                shading_model = Material.ShadingModel.EMISSIVE
                color = [*mat.emissive_factor[0:3], 1.0]
            else:
                shading_model = Material.ShadingModel.DEFAULT

            double_sided = mat.double_sided if mat.double_sided is not None else True

            material.color = color
            material.double_sided = double_sided
            material.roughness = roughness
            material.metallic = metallic
            material.shading_model = shading_model

            self._eis_materials.append(material)

        index = 0
        for mesh_gltf in self._meshes:
            geometries: List[Geometry] = []
            for prim in mesh_gltf.primitives:
                material = None
                if prim.material is not None:
                    material = self._eis_materials[prim.material]

                if prim.mode == 1:
                    mode = Geometry.PrimitiveType.POINT
                # Default mode is triangles.
                elif prim.mode == 4 or prim.mode is None:
                    mode = Geometry.PrimitiveType.TRIANGLE
                elif prim.mode == 5:
                    mode = Geometry.PrimitiveType.TRIANGLE_STRIP
                else:
                    logger.warning('Primitive mode {} not supported by EiS, ignoring primitive.'.format(prim.mode))
                    continue

                vertices: Optional[List[Vertex]] = []
                if (prim.attributes is not None
                        and prim.attributes.get('POSITION') is not None):
                    accessor = self._accessors[prim.attributes.get('POSITION')]
                    if accessor.type != 'VEC3':
                        logger.warning(
                            'Unsupported data type {} for vertex accessor, only VEC3 is valid.'.format(accessor.type))
                        continue
                    vertices = self._parse_buffer_data(data_format='fff', accessor=accessor)

                indices: Optional[List[List[int]]] = []
                if prim.indices is not None:
                    accessor = self._accessors[prim.indices]

                    if accessor.component_type == 5120:
                        data_format = 'bbb'
                    elif accessor.component_type == 5121:
                        data_format = 'BBB'
                    elif accessor.component_type == 5122:
                        data_format = 'hhh'
                    elif accessor.component_type == 5123:
                        data_format = 'HHH'
                    elif accessor.component_type == 5125:
                        data_format = 'III'
                    else:
                        logger.warning('Accessor component type {} is not an integer type, not supported by glTF'.format(
                            accessor.component_type))
                        continue

                    indices = self._parse_buffer_data(data_format=data_format, accessor=accessor, step=3)
                else:
                    indices = [[v * 3, v * 3 + 1, v * 3 + 2] for v in range(0, len(vertices) // 3)]

                normals: Optional[List[Normal]] = []
                if (prim.attributes is not None
                        and prim.attributes.get('NORMAL') is not None):
                    accessor = self._accessors[prim.attributes.get('NORMAL')]
                    if accessor.type != 'VEC3':
                        logger.warning(
                            'Unsupported data type {} for normals accessor, only VEC3 is valid.'.format(
                                accessor.type))
                    normals = self._parse_buffer_data(data_format='fff', accessor=accessor)

                texcoords: Optional[List[TexCoord]] = []
                if (prim.attributes is not None
                        and prim.attributes.get('TEXCOORD_0') is not None):
                    accessor = self._accessors[prim.attributes.get('TEXCOORD_0')]
                    if accessor.type != 'VEC2':
                        logger.warning(
                            'Unsupported data type {} for texcoords accessor, only VEC2 is valid.'.format(accessor.type))
                        continue
                    texcoords = self._parse_buffer_data(data_format='ff', accessor=accessor)

                colors: Optional[List[Color]] = []
                if (prim.attributes is not None
                        and prim.attributes.get('COLOR_0') is not None):
                    accessor = self._accessors[prim.attributes.get('COLOR_0')]
                    if accessor.type == 'VEC3':
                        colors_vec3 = self._parse_buffer_data(data_format='fff', accessor=accessor)
                        colors = [(color[0], color[1], color[2], 1.0) for color in colors_vec3]
                    elif accessor.type == 'VEC4':
                        colors_vec4 = self._parse_buffer_data(data_format='ffff', accessor=accessor)
                        colors = [(color[0], color[1], color[2], color[3]) for color in colors_vec4]
                    else:
                        logger.warning(
                            'Unsupported data type {} for color accessor, only VEC3 is valid.'.format(accessor.type))
                        continue

                name = prim.extras.get('name') if prim.extras else None
                geometries.append(
                    Geometry(material=material, primitive_type=mode,
                             vertices=vertices or [], primitives=indices or [],
                             texcoords=texcoords or [], colors=colors or [],
                             normals=normals or [], name=name)
                )

            self._eis_meshes.append(Mesh(name=mesh_gltf.name, geometries=geometries))
            index += 1

        for i, node in enumerate(self._nodes):
            # If the node does not have a matrix, compose from TRS components.
            # (Default is the identity matrix.)
            if node.matrix is None:
                t = node.translation or [0.0, 0.0, 0.0]
                r = node.rotation or [0.0, 0.0, 0.0, 1.0]
                s = node.scale or [1.0, 1.0, 1.0]

                eis_matrix = (
                    Matrix44.from_translation(Vector3(x=t[0], y=t[1], z=t[2]))
                    * Matrix44.from_quaternion(Quaternion(x=r[0], y=r[1], z=r[2], w=r[3]))
                    * Matrix44.from_scale(Vector3(x=s[0], y=s[1], z=s[2]))
                )
            else:
                eis_matrix = Matrix44(node.matrix)

            if node.mesh is not None:
                mesh = self._eis_meshes[node.mesh]
                self._eis_objects.append(Object3D(name=node.name,
                                                  matrix=eis_matrix,
                                                  mesh=mesh))
            elif node.camera is not None:
                camera = Camera(name=node.name, matrix=eis_matrix)
                self._eis_objects.append(camera)
            elif node.extensions and node.extensions.get('KHR_lights_punctual'):
                # Find the light referenced by this node.
                for light_index in node.extensions.get('KHR_lights_punctual').values():
                    light = self._lights[light_index]
                    eis_light = light.to_eis_light(node)
                    if eis_light:
                        # Append light to EiS scene graph.
                        self._eis_objects.append(eis_light)

            else:
                self._eis_objects.append(Object3D(
                    name=node.name,
                    matrix=eis_matrix
                ))

        index = 0
        for animation_gltf in self._animations:
            for channel in animation_gltf.channels:
                if channel.target is None or channel.target.node is None:
                    continue

                target_object = self._eis_objects[channel.target.node]
                if channel.target.path == 'translation':
                    target_attr = 'location'
                elif channel.target.path == 'rotation':
                    target_attr = 'rotation'
                elif channel.target.path == 'scale':
                    target_attr = 'scale'
                else:
                    logger.warning('Unsupported animation path {}.'.format(channel.target.path))
                    continue

                target_object_behavior = target_object.get_behavior_by_type(AnimationBehavior)
                if target_object_behavior is not None:
                    animation_behavior = target_object_behavior
                else:
                    animation_behavior = AnimationBehavior()
                    target_object.add_behavior(animation_behavior)

                sampler = animation_gltf.samplers[channel.sampler]
                timings = self._parse_buffer_data(data_format='f', accessor=self._accessors[sampler.input])

                value_accessor = self._accessors[sampler.output]
                if value_accessor.type == 'VEC3':
                    values_vec3 = self._parse_buffer_data(data_format='fff', accessor=value_accessor)
                    values = [Vector3((value[0], value[1], value[2])) for value in values_vec3]
                elif value_accessor.type == 'VEC4':
                    values_vec4 = self._parse_buffer_data(data_format='ffff', accessor=value_accessor)
                    values = [Vector4((value[0], value[1], value[2], value[3])) for value in values_vec4]
                else:
                    logger.warning('Unsupported data type {} for animation value accessor.'.format(
                        value_accessor.type))
                    continue

                if target_attr == 'rotation':
                    values = [Euler(value) for value in values]

                for index in range(len(timings)):
                    animation_behavior.add_timed_keyframe_with_value(
                        attribute=target_attr, time=timings[index][0], value=values[index * 3 + 1])

                if sampler.interpolation == 'LINEAR':
                    animation_behavior.set_interpolation_for_attribute(
                        attribute=target_attr, interpolation_type=AnimationCurve.InterpolationType.LINEAR)
                elif sampler.interpolation == 'STEP':
                    animation_behavior.set_interpolation_for_attribute(
                        attribute=target_attr, interpolation_type=AnimationCurve.InterpolationType.CONSTANT)
                elif sampler.interpolation == 'CATMULLROMSPLINE':
                    animation_behavior.set_interpolation_for_attribute(
                        attribute=target_attr, interpolation_type=AnimationCurve.InterpolationType.SMOOTH)
                elif sampler.interpolation == 'CUBICSPLINE':
                    animation_behavior.set_interpolation_for_attribute(
                        attribute=target_attr, interpolation_type=AnimationCurve.InterpolationType.SMOOTH)
                else:
                    logger.warning('Unknown interpolation type {} for attribute {} of object {}.'.format(
                        sampler.interpolation, target_attr, target_object.name))

        # Recreate the hierarchy by using the fact that the imported glTF objects
        # and the eis objects are ordered identically
        for i, node in enumerate(self._nodes):
            if node.children is not None:
                for child in node.children:
                    self._eis_objects[i].add_child(self._eis_objects[child])

    def import_gltf(self) -> Optional[Scene]:
        gltf_file_path: Optional[str] = None

        if not os.path.exists(self._path):
            logger.warning('Provided gltf path does not exist.')
            return None

        if not self._path.endswith('.gltf'):
            for f in os.listdir(self._path):
                if f.endswith('.gltf'):
                    gltf_file_path = f
                    break
        else:
            gltf_file_path = self._path

        if not gltf_file_path:
            logger.warning('GLTF file not found at path: {}'.format(self._path))
            return None

        with open(os.path.join(self._path, gltf_file_path), 'r') as export_file:
            gltf_importer = glTFImporter(filename=os.path.join(self._path, gltf_file_path),
                                         import_settings={})
            success, txt = gltf_importer.read()
            gltf_data = gltf_importer.data

        scene_name = self._parse_json(gltf_data)

        if scene_name is None:
            return None

        self._create_scene_graph()

        root = Object3D() if self._root_index == -1 else self._eis_objects[self._root_index]
        root.matrix *= Matrix44.from_axis_rotation(Vector3((1.0, 0.0, 0.0)), -pi / 2.0)
        for obj in self._eis_objects:
            if obj != root and obj.parent is None:
                root.add_child(obj)

        # The base root must not have any transformations, so we add a new root
        # if needed.
        if root.matrix != Matrix44.identity():
            scene_root: Object3D = Object3D()
            scene_root.add_child(root)
        else:
            scene_root = root

        model = Model(root=scene_root,
                      meshes=self._eis_meshes,
                      materials=self._eis_materials,
                      textures=self._eis_textures)

        date = datetime.datetime.now()

        name = (
            scene_name
            or 'imported_gltf_scene_{}_{}_{}'.format(date.day, date.month, date.year)
        )
        return Scene(name=name, model=model)


@converter
class GLTFConverter(FormatConverter):
    @staticmethod
    def handles(extension: str) -> bool:
        return extension in ['gltf']

    @classmethod
    def import_path(cls, path: str) -> Optional[Scene]:
        return GLTFImporter(path=path).import_gltf()

    @staticmethod
    def import_data(data: bytes) -> None:
        logger.error('GLTF only supports import by path, not by data.')
        raise NotImplementedError

    @classmethod
    def export_path(cls, scene: Scene, path: str) -> None:
        GLTFExporter(scene=scene, filename=path).export()

    @staticmethod
    def export_data(scene: Scene, path: str) -> bytes:
        logger.error('GLTF only supports exporting to a specific path.')
        raise NotImplementedError
