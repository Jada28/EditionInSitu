import logging
import math
import os
from abc import ABCMeta, abstractmethod
from typing import Any, Dict, Optional, TYPE_CHECKING
from uuid import UUID

from rx.subjects import Subject  # type: ignore

from eis import EISEntityId, BASE_PATH
from eis.client.input import LocalInputMethod
from eis.constants import SERVER, CLIENT
from eis.editor import defaultEditorConfig
from eis.engine import Engine
from eis.graph.material import Material
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from eis.graph.primitives.shape import Shape
from eis.input import PeerInputMethod
from eis.inputs.support.buttons import ButtonState
from eis.inputs.support.vrpn import VRPN_AVAILABLE
from eis.notification import EISClientNotification, EISServerNotification, NotificationId
from eis.picker import Picker
from eis.server.input import RemoteInputMethod
from eis.user import EISUser
from satmath.euler import Euler  # type: ignore
from satmath.matrix44 import Matrix44  # type: ignore
from satmath.quaternion import Quaternion  # type: ignore
from satmath.vector3 import Vector3  # type: ignore
from satmath.vector4 import Vector4  # type: ignore
from satnet.commands import SubscribeCommand, UnsubscribeCommand
from satnet.entity import entity
from satnet.notification import notification

if TYPE_CHECKING:
    from eis.client.session import EISLocalSession
    from eis.server.session import EISRemoteSession
    from eis.server.user import RemoteEISUser

if VRPN_AVAILABLE:
    from eis.inputs.support.vrpn import VRPNAnalog, VRPNTracker, VRPNButton

logger = logging.getLogger(__name__)

defaultEditorConfig['input.vive.enabled'] = True
defaultEditorConfig['input.vive.vrpn.host'] = "localhost"
defaultEditorConfig['input.vive.vrpn.primary.device'] = "openvr/controller/1"
defaultEditorConfig['input.vive.vrpn.secondary.device'] = "openvr/controller/2"
defaultEditorConfig['input.vive.offset.tracker_rotation'] = [0.0, 0.0, 0.0]


class HTCViveController(metaclass=ABCMeta):
    """
    HTC Vive Controller base class
    Represents a controller in an HTC Vive setup
    Extend this class in order to provide support for different methods of getting
    the controller information (VRPN, OpenVR, etc.)
    """

    MENU_BUTTON = 1
    GRAB_BUTTON = 2
    TRACKPAD_BUTTON = 32
    TRIGGER_BUTTON = 33
    TRACKPAD_X = 0
    TRACKPAD_Y = 1
    TRIGGER_ANALOG = 2

    BUTTON_NAMES = {
        MENU_BUTTON: "menu",
        GRAB_BUTTON: "grab",
        TRACKPAD_BUTTON: "trackpad",
        TRIGGER_BUTTON: "trigger"
    }

    def __init__(self, config: Dict[str, Any]) -> None:
        super().__init__()

        self._config = config

        self.vive: Optional[HTCVive] = None

        self._location = Vector3()
        self._rotation = Quaternion()
        self._matrix = Matrix44().identity()

        offset_location = Vector3(self._config.get('input.vive.offset.location', [0.0, 0.0, 0.0]))
        offset_rotation = Euler(
            [angle * math.pi / 180 for angle in self._config.get('input.vive.offset.rotation', [0.0, 0.0, 0.0])])
        tracker_rotation = Euler(
            [angle * math.pi / 180.0 for angle in self._config.get('input.vive.offset.tracker_rotation', [0.0, 0.0, 0.0])])
        self._tracking_space_offset = Matrix44.from_translation(
            offset_location.inverse) * Matrix44.from_euler(offset_rotation)
        self._tracker_rotation = Matrix44.from_euler(tracker_rotation)  # NOTE: This might be configurable

        # Subjects
        self._button_subject = Subject()  # Useless as is, a subclass needs to either replace this or inject values into it

        self._analog_subject = Subject()  # Useless as is, a subclass needs to either replace this or inject values into it

        self._tracker_subject = Subject()

    @property
    def tracking_space_offset(self) -> Matrix44:
        return self._tracking_space_offset.copy()

    @tracking_space_offset.setter
    def tracking_space_offset(self, offset: Matrix44) -> None:
        self._tracking_space_offset = offset.copy()

    @property
    def tracker_rotation(self) -> Euler:
        return Euler.from_matrix(self._tracker_rotation)

    @tracker_rotation.setter
    def tracker_rotation(self, rotation: Euler) -> None:
        self._tracker_rotation = Matrix44.from_euler(rotation)

    @property
    def location(self) -> Vector3:
        return self._location.copy()

    @property
    def rotation(self) -> Quaternion:
        return self._rotation.copy()

    @property
    def matrix(self) -> Matrix44:
        return self._matrix.copy()

    @property
    def button_subject(self) -> Subject:
        return self._button_subject

    @property
    def analog_subject(self) -> Subject:
        return self._analog_subject

    @property
    def tracker_subject(self) -> Subject:
        return self._tracker_subject

    @property
    def trigger_button(self) -> ButtonState:
        return self.get_button(HTCViveController.TRIGGER_BUTTON)

    @property
    def trackpad_button(self) -> ButtonState:
        return self.get_button(HTCViveController.TRACKPAD_BUTTON)

    @property
    def menu_button(self) -> ButtonState:
        return self.get_button(HTCViveController.MENU_BUTTON)

    @property
    def grab_button(self) -> ButtonState:
        return self.get_button(HTCViveController.GRAB_BUTTON)

    @property
    def trackpad_x(self) -> float:
        return self.get_analog(HTCViveController.TRACKPAD_X)

    @property
    def trackpad_y(self) -> float:
        return self.get_analog(HTCViveController.TRACKPAD_Y)

    @property
    def trigger_analog(self) -> float:
        return self.get_analog(HTCViveController.TRIGGER_ANALOG)

    def step(self, now: float, dt: float) -> None:
        old_matrix = self._matrix

        self._matrix = self._tracking_space_offset \
            * Matrix44.from_translation(self.location) \
            * Matrix44.from_quaternion(self.rotation) \
            * self._tracker_rotation

        if self._matrix != old_matrix:
            self._tracker_subject.on_next(self._matrix)

    @abstractmethod
    def get_button(self, button_id: int) -> ButtonState:
        pass

    @abstractmethod
    def get_analog(self, channel_id: int) -> float:
        pass


if VRPN_AVAILABLE:
    class HTCViveControllerVRPN(HTCViveController):
        """
        HTC Vive Controller derived class supporting VRPN protocol.
        """

        def __init__(self, host: str, device: str, config: Dict[str, Any]) -> None:
            super().__init__(config=config)

            self._host = host
            self._device = device

            self._connected = False

            self._tracker = VRPNTracker()
            self._analog = VRPNAnalog()
            self._button = VRPNButton()

            self._tracker.connect(self._host, self._device)
            self._analog.connect(self._host, self._device)
            self._button.connect(self._host, self._device)

            self._connected = True

        @property
        def button_subject(self) -> Subject:
            """
            Override, using the vrpn button subject instead
            :return: Subject
            """
            return self._button.button_subject

        @property
        def analog_subject(self) -> Subject:
            """
            Override, using the vrpn analog subject instead
            :return: Subject
            """
            return self._analog.analog_subject

        def get_button(self, button_id: int) -> ButtonState:
            button = self._button.buttons.get(button_id)
            return button if button is not None else ButtonState()

        def get_analog(self, channel_id: int) -> float:
            if channel_id < len(self._analog.channels):
                channel = self._analog.channels[channel_id]
                return channel if channel is not None else 0.0
            else:
                return 0.0

        def step(self, now: float, dt: float) -> None:
            if not self._connected:
                return

            self._tracker.step(now=now, dt=dt)
            self._analog.step(now=now, dt=dt)
            self._button.step(now=now, dt=dt)

            loc = self._tracker.location.get(0)
            if loc is not None:
                self._location = loc.copy()
            else:
                self._location = Vector3()

            rot = self._tracker.rotation.get(0)
            if rot is not None:
                self._rotation = rot.copy()
            else:
                self._rotation = Quaternion()

            super().step(now=now, dt=dt)


class ViveModels:
    """
    Helper class to help with the instantiation and update
    of the Vive controllers 3d model.
    """

    def __init__(self) -> None:
        self.c1: Optional[Object3D] = None
        self.c2: Optional[Object3D] = None
        self._visible = True

    def add(self, engine: Engine) -> None:
        path = os.path.realpath(os.path.join(BASE_PATH, "res/models/controllers/vive/vive_controller.bam"))

        def add_controllers(model: Optional[Model]) -> None:
            if model:
                model.root.add_child(Shape(
                    points=[(0.0, 0.0, 0.0), (0.0, 1000.0, 0.0)],
                    width=2.0,
                    material=Material(color=(1.0, 0.0, 0.0, 1.0), shading_model=Material.ShadingModel.EMISSIVE))
                )
                model.root.layer = 1

                c1_model = model.copy()
                self.c1 = c1_model.root
                engine.add_model(model=c1_model)

                c2_model = model.copy()
                self.c2 = c2_model.root
                engine.add_model(model=c2_model)

        engine.read_model_from_file_async(path=path, callback=add_controllers)

    @property
    def visible(self) -> bool:
        return self._visible

    @visible.setter
    def visible(self, visible: bool) -> None:
        self._visible = visible
        if self.c1 is not None:
            self.c1.visible = visible
        if self.c2 is not None:
            self.c2.visible = visible

    def remove(self) -> None:
        if self.c1:
            self.c1.remove()
            self.c1.dispose()
            self.c1 = None
        if self.c2:
            self.c2.remove()
            self.c2.dispose()
            self.c2 = None

    def update(self, primary_matrix: Matrix44, secondary_matrix: Matrix44) -> None:
        if self.c1:
            self.c1.matrix = primary_matrix
        if self.c2:
            self.c2.matrix = secondary_matrix


if CLIENT:

    @entity(id=EISEntityId.VIVE_INPUT)
    class HTCVive(LocalInputMethod):
        """
        Client-side HTC Vive Controllers
        This input method joins two controllers into one input method
        """

        _fields = ['_primary_matrix', '_secondary_matrix', '_trackers_matrices']

        def __init__(self, primary: HTCViveController, secondary: HTCViveController, config: Dict[str, Any], mapping_config: Dict[str, Any], picker: Picker) -> None:
            super().__init__(config=config, mapping_config=mapping_config, picker=picker)

            # Base class member indicating that we want
            # this input method synced with the server
            self._sync = True

            # Primary controller
            self._primary = primary
            self._primary.vive = self
            primary_button_subject = self._primary.button_subject.map(lambda button_state: (
                "vive_primary_" + HTCViveController.BUTTON_NAMES[button_state[0]], button_state[1]))
            primary_analog_subject = self._primary.analog_subject.map(
                lambda analog_state: ('vive_primary', *analog_state))
            primary_tracker_subject = self._primary.tracker_subject.map(
                lambda tracker_state: ('vive_primary', tracker_state))
            self._primary_matrix = Matrix44.identity()
            self._last_primary_matrix = Matrix44.identity()

            # Secondary controller
            self._secondary = secondary
            self._secondary.vive = self
            secondary_button_subject = self._secondary.button_subject.map(lambda button_state: (
                "vive_secondary_" + HTCViveController.BUTTON_NAMES[button_state[0]], button_state[1]))
            secondary_analog_subject = self._secondary.analog_subject.map(
                lambda analog_state: ('vive_secondary', *analog_state))
            secondary_tracker_subject = self._secondary.tracker_subject.map(
                lambda tracker_state: ('vive_secondary', tracker_state))

            # Trackers
            # Vive Trackers are also HTCViveControllers, stripped from their buttons
            self._trackers: Dict[str, HTCViveController] = {}
            self._trackers_tracker_subjects: Dict[str, Subject] = {}

            # Matrices
            self._secondary_matrix = Matrix44.identity()
            self._last_secondary_matrix = Matrix44.identity()
            self._trackers_matrices: Dict[str, Matrix44] = {}
            self._trackers_last_matrices: Dict[str, Matrix44] = {}

            # Merged button subject from both controllers
            self._button_subject = primary_button_subject.merge(secondary_button_subject)

            # Merged analog subject from both controllers
            self._analog_subject = primary_analog_subject.merge(secondary_analog_subject)

            # Merged tracker subject from both controllers
            self._tracker_subject = primary_tracker_subject.merge(secondary_tracker_subject)

            # Helper class for instantiating and updating the preview 3d models
            self._vive_models = ViveModels()

            self._mapping_dict['free_move_forward'] = ('map_movement', ['vive_primary_trigger'])
            self._mapping_dict['free_move_backward'] = ('map_movement', ['vive_secondary_trigger'])
            self._mapping_dict['constrained_move_forward'] = None
            self._mapping_dict['constrained_move_backward'] = None
            self._mapping_dict['constrained_move_left'] = None
            self._mapping_dict['constrained_move_right'] = None
            self._mapping_dict['constrained_move_up'] = None
            self._mapping_dict['constrained_move_down'] = None
            self._mapping_dict['rotate'] = ('map_vive_head_rotation', ['vive_secondary_grab'])
            self._mapping_dict['select'] = ('map_toggle_combination_without_action', [
                                            ['vive_primary_trigger', 'vive_secondary_trigger']])
            self._mapping_dict['hold'] = ('map_toggle_action', ['vive_secondary_trigger'])
            self._mapping_dict['confirm'] = None
            self._mapping_dict['help'] = ('map_toggle_action', ['vive_secondary_menu'])
            self._mapping_dict['recall'] = None
            self._mapping_dict['alternative_select'] = ('map_toggle_combination_with_action', [
                                                        ['vive_primary_trigger', 'vive_secondary_trigger']])
            self._mapping_dict['quick_translate'] = ('map_long_toggle_action', ['vive_primary_trigger'])
            self._mapping_dict['quick_rotate'] = ('map_long_toggle_action', ['vive_secondary_grab'])
            self._mapping_dict['quick_scale'] = ('map_long_toggle_action', ['vive_secondary_trigger'])
            self._mapping_dict['menu'] = ('map_toggle_action', ['vive_primary_menu'])
            self._mapping_dict['undo'] = ('map_pressed_htc_section', ['vive_secondary_trackpad_left'])
            self._mapping_dict['redo'] = ('map_pressed_htc_section', ['vive_secondary_trackpad_right'])
            self._mapping_dict['reset'] = ('map_pressed_htc_section', ['vive_secondary_trackpad'])
            self._mapping_dict['slow_down'] = ('map_pressed_htc_section', ['vive_primary_trackpad_down'])
            self._mapping_dict['speed_up'] = ('map_pressed_htc_section', ['vive_primary_trackpad_up'])
            self._mapping_dict['navigate'] = ('map_toggle_action', ['vive_primary_grab'])
            self._mapping_dict['axis_x'] = ('map_htc_horizontal', ['vive_primary'])
            self._mapping_dict['axis_y'] = ('map_htc_vertical', ['vive_primary'])
            self._mapping_dict['axis_z'] = ('map_htc_horizontal', ['vive_secondary'])
            self._mapping_dict['axis_w'] = ('map_htc_vertical', ['vive_secondary'])
            self._mapping_dict['engage'] = None
            self._mapping_dict['hands'] = None
            self._mapping_dict['drag'] = ('map_toggle_action', ['vive_secondary_grab'])
            self._mapping_dict['distance'] = None
            self._mapping_dict['toggle_timeline'] = ('map_double_click_action', ['vive_secondary_grab'])

            self._apply_config()

        @property
        def distance(self) -> float:
            return (self._primary_matrix.translation - self._secondary_matrix.translation).length

        @property
        def primary_matrix(self) -> Matrix44:
            return self._primary_matrix.copy()

        @property
        def secondary_matrix(self) -> Matrix44:
            return self._secondary_matrix.copy()

        @property
        def trackers_matrices(self) -> Dict[str, Matrix44]:
            return self._trackers_matrices

        @property
        def primary(self) -> HTCViveController:
            return self._primary

        @property
        def secondary(self) -> HTCViveController:
            return self._secondary

        @property
        def trackers(self) -> Dict[str, HTCViveController]:
            return self._trackers

        def add_tracker(self, name: str, tracker: HTCViveController) -> None:
            """
            Add a new tracker
            :param name: str - Tracker's name
            :return: None
            """
            self._trackers[name] = tracker
            self._trackers_tracker_subjects[name] = tracker.tracker_subject.map(
                lambda tracker_state: ('vive_tracker_' + name, tracker_state))
            self._trackers_matrices[name] = Matrix44.identity()
            self._trackers_last_matrices[name] = Matrix44.identity()

        def remove_tracker(self, name: str) -> None:
            """
            Remove a tracker if it exists
            :param name: str - Tracker's name
            :return: None
            """
            self._trackers.pop(name)
            self._trackers_tracker_subjects.pop(name)
            self._trackers_matrices.pop(name)
            self._trackers_last_matrices.pop(name)

        def ready(self) -> None:
            super().ready()

            if not self._user or not self._user.editor:
                logger.warning("Ready called on HTCVive without a user or editor")
                return

            self._vive_models.add(self._user.editor.engine)

        def step(self, now: float, dt: float) -> None:
            super().step(now=now, dt=dt)

            self._primary.step(now=now, dt=dt)
            self._secondary.step(now=now, dt=dt)
            for tracker in self._trackers.items():
                tracker[1].step(now=now, dt=dt)

            if not self._user or not self._user.editor:
                return

            self._primary_matrix = self._user.editor.matrix * self._primary.matrix
            self._secondary_matrix = self._user.editor.matrix * self._secondary.matrix

            self._primary_orientation = Quaternion.from_matrix(self._primary_matrix)
            self._secondary_orientation = Quaternion.from_matrix(self._secondary_matrix)

            for trackers in self._trackers.items():
                self._trackers_matrices[trackers[0]] = self._user.editor.matrix * trackers[1].matrix

            # Update 3D Models
            if self._picker_active is not self._vive_models.visible:
                self._vive_models.visible = self._picker_active
            if self._picker_active:
                self._vive_models.update(self._primary_matrix, self._secondary_matrix)

            # Set the origin and direction of the picking ray
            self._picker.origin = Vector3((self._primary_matrix.mul_vector4(Vector4([0., 0., 0., 1.]))).xyz)
            self._picker.direction = Vector3((self._primary_matrix.mul_vector4(Vector4([0., 1., 0., 0.]))).xyz)

            # Set common controls
            self._common_subject.on_next(('distance', self.distance))

        def notify(self) -> None:
            if not self._user or not self._user.editor:
                return

            trackers_updated = False
            for name in self._trackers:
                current_matrix = self._trackers_matrices[name]
                last_matrix = self._trackers_last_matrices[name]
                if current_matrix != last_matrix:
                    trackers_updated = True
                    last_matrix = current_matrix

            if self._last_primary_matrix != self._primary_matrix or self._last_secondary_matrix != self._secondary_matrix:
                # TODO: Optimize network by only sending the changed matrix
                self._user.session.notify(ViveStateClient(user=self._user, vive=self))
                self._last_primary_matrix = self.primary_matrix
                self._last_secondary_matrix = self.secondary_matrix
            elif trackers_updated:
                self._user.session.notify(ViveStateClient(user=self._user, vive=self))

if SERVER:

    @entity(id=EISEntityId.VIVE_INPUT)  # type: ignore
    class HTCVive(RemoteInputMethod):
        """
        Server-side HTC Vive Controllers
        """

        _fields = ['_primary_matrix', '_secondary_matrix', '_trackers_matrices']

        def __init__(self) -> None:
            super().__init__()
            self._primary_matrix = Matrix44.identity()
            self._last_primary_matrix = Matrix44.identity()
            self._secondary_matrix = Matrix44.identity()
            self._last_secondary_matrix = Matrix44.identity()
            self._trackers_matrices = {}  # type: Dict[str, Matrix44]
            self._trackers_last_matrices = {}  # type: Dict[str, Matrix44]

        def ready(self) -> None:
            super().ready()

            if not self._user:
                logger.warning("Ready called on HTCVive without a user")
                return

            # We have to subscribe to the updates from the client, it's not hard-coded
            self._user.session.command(SubscribeCommand(topic=NotificationId.VIVE_STATE_CLIENT))

        def dispose(self) -> None:
            if self._user:
                # NOTE: This will try to send to a non-existing session when the client disconnects
                self._user.session.command(UnsubscribeCommand(topic=NotificationId.VIVE_STATE_CLIENT))
            super().dispose()

        @property
        def primary_matrix(self) -> Matrix44:
            return self._primary_matrix.copy()

        @property
        def secondary_matrix(self) -> Matrix44:
            return self._secondary_matrix.copy()

        @property
        def trackers_matrices(self) -> Dict[str, Matrix44]:
            return self._trackers_matrices

        def step(self, now: float, dt: float) -> None:
            super().step(now=now, dt=dt)

        def notify(self) -> None:
            if not self._user or not self._user.session:
                return

            trackers_updated = False
            for name in self._trackers_matrices:
                current_matrix = self._trackers_matrices[name]
                last_matrix = self._trackers_last_matrices.get(name, None)
                if last_matrix is None or self._trackers_matrices[name] != last_matrix:
                    trackers_updated = True
                    self._trackers_last_matrices[name] = current_matrix

            if self._last_primary_matrix != self._primary_matrix or self._last_secondary_matrix != self._secondary_matrix:
                # TODO: Optimize network by only sending the changed matrix
                self._user.session.notify_others(ViveStatePeer(user=self._user, vive=self))
                self._last_primary_matrix = self.primary_matrix
                self._last_secondary_matrix = self.secondary_matrix
            elif trackers_updated:
                self._user.session.notify_others(ViveStatePeer(user=self._user, vive=self))

        def to_peer(self) -> Optional['PeerHTCVive']:
            return PeerHTCVive(
                uuid=self.uuid,
                primary_matrix=self._primary_matrix,
                secondary_matrix=self._secondary_matrix,
                trackers_matrices=self._trackers_matrices
            )


@entity(id=EISEntityId.VIVE_PEER_INPUT)
class PeerHTCVive(PeerInputMethod):
    """
    Client-side Peer HTC Vive Controllers
    Used to represent another user's Vive Controllers from another session.
    """

    _fields = ['_primary_matrix', '_secondary_matrix', '_trackers_matrices']

    def __init__(
            self,
            uuid: Optional[UUID] = None,
            primary_matrix: Optional[Matrix44] = None,
            secondary_matrix: Optional[Matrix44] = None,
            trackers_matrices: Optional[Dict[str, Matrix44]] = None
    ) -> None:
        super().__init__()

        if uuid is not None:
            self._uuid = uuid
        self._primary_matrix = primary_matrix if primary_matrix is not None else Matrix44.identity()
        self._secondary_matrix = secondary_matrix if secondary_matrix is not None else Matrix44.identity()
        self._trackers_matrices = trackers_matrices if trackers_matrices is not None else {}
        self._vive_models = ViveModels()

    def ready(self) -> None:
        super().ready()

        if not self._user:
            logger.warning("Ready called on PeerHTCVive without a user")
            return

        self._vive_models.add(self._user.peer.session.editor.engine)
        # We have to subscribe to the updates from the server, it's not hard-coded
        self._user.peer.session.command(SubscribeCommand(topic=NotificationId.VIVE_STATE_PEER))

    def dispose(self) -> None:
        if self._user:
            self._user.peer.session.command(UnsubscribeCommand(topic=NotificationId.VIVE_STATE_PEER))
        self._vive_models.remove()
        super().dispose()

    @property
    def primary_matrix(self) -> Matrix44:
        return self._primary_matrix.copy()

    @property
    def secondary_matrix(self) -> Matrix44:
        return self._secondary_matrix.copy()

    @property
    def trackers_matrices(self) -> Dict[str, Matrix44]:
        return self._trackers_matrices

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)
        # Update 3D Models
        self._vive_models.update(self._primary_matrix, self._secondary_matrix)


@notification(id=NotificationId.VIVE_STATE_CLIENT)
class ViveStateClient(EISClientNotification):
    """
    Client-to-server notification to sync the vive controller status to the server.
    """

    _fields = ['uuid', '_primary_matrix', '_secondary_matrix', '_trackers_matrices']

    def __init__(self, user: Optional[EISUser] = None, vive: Optional['HTCVive'] = None) -> None:
        super().__init__(context=user.uuid if user else None)
        self.uuid = vive.uuid if vive else None
        self._primary_matrix = vive.primary_matrix if vive else Matrix44.identity()
        self._secondary_matrix = vive.secondary_matrix if vive else Matrix44.identity()
        self._trackers_matrices = {}  # type: Dict[str, Matrix44]
        if vive is not None:
            for name in vive.trackers_matrices:
                self._trackers_matrices[name] = vive.trackers_matrices[name].copy()

    def handle(self, session: 'EISRemoteSession') -> None:
        if self.context is None:
            logger.warning("Notification has no context")
            return

        user = session.editor.users.get(self.context)
        if user is None:
            logger.warning("User \"{}\" not found".format(self.context))
            return

        if not self.uuid:
            logger.warning("No input method uuid in notification")
            return

        input_method = user.input_methods.get(self.uuid)
        if input_method is None:
            logger.warning("Input method \"{}\" not found".format(self.uuid))
            return

        assert isinstance(input_method, HTCVive)

        input_method._primary_matrix = self._primary_matrix
        input_method._secondary_matrix = self._secondary_matrix
        input_method._trackers_matrices = self._trackers_matrices


@notification(id=NotificationId.VIVE_STATE_PEER)
class ViveStatePeer(EISServerNotification):
    """
    Server-to-clients notification to share the vive controller status
    with all the other sessions.
    """

    _fields = ['uuid', 'session', '_primary_matrix', '_secondary_matrix', '_trackers_matrices']

    def __init__(self, user: Optional['RemoteEISUser'] = None, vive: Optional['HTCVive'] = None) -> None:
        super().__init__(context=user.uuid if user else None)

        self.session = user.session.id if user and user.session else None
        self.uuid = vive.uuid if vive else None
        self._primary_matrix = vive.primary_matrix if vive else Matrix44.identity()
        self._secondary_matrix = vive.secondary_matrix if vive else Matrix44.identity()
        self._trackers_matrices = {}  # type: Dict[str, Matrix44]
        if vive is not None:
            for name in vive.trackers_matrices:
                self._trackers_matrices[name] = vive.trackers_matrices[name].copy()

    def handle(self, session: 'EISLocalSession') -> None:
        if self.context is None:
            logger.warning("Received a ViveStatePeer notification without a context from {}".format(session))
            return

        if self.session is None:
            logger.warning("Received a ViveStatePeer notification without a session from {}".format(session))
            return

        if self.uuid is None:
            logger.warning("Received a ViveStatePeer notification without an input state uuid from {}".format(session))
            return

        peer = session.peers.get(self.session)
        if peer is None:
            logger.warning("Peer \"{}\" not found".format(peer))
            return

        user = peer.users.get(self.context)
        if user is None:
            logger.warning("User \"{}\" not found".format(self.context))
            return

        input_method = user.input_methods.get(self.uuid)
        if input_method is None:
            logger.warning("Input method \"{}\" not found".format(self.uuid))
            return

        assert isinstance(input_method, PeerHTCVive)

        input_method._primary_matrix = self._primary_matrix
        input_method._secondary_matrix = self._secondary_matrix
        input_method._trackers_matrices = self._trackers_matrices
