from enum import IntEnum, unique


@unique
class ButtonStateEnum(IntEnum):
    UP = 0x00
    TOUCHED = 0x01
    TOUCHING = 0x02
    PRESSED = 0x03
    DOWN = 0x04
    RELEASED = 0x05
    UNTOUCHED = 0x06


class ButtonState:
    """
    Class that holds button states
    """

    def __init__(self) -> None:
        self.up = True
        self.touched = False
        self.touching = False
        self.untouched = False
        self.pressed = False
        self.down = False
        self.released = False

    def __str__(self) -> str:
        return "Up: " + str(self.up) + ", " + \
               "Touched: " + str(self.touched) + ", " + \
               "Touching: " + str(self.touching) + ", " + \
               "Untouched: " + str(self.untouched) + ", " + \
               "Pressed: " + str(self.pressed) + ", " + \
               "Down: " + str(self.down) + ", " + \
               "Released: " + str(self.released)
