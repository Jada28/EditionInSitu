from typing import Optional
from satnet.serialization import serializable
from satnet.entity import Entity, entity
from eis.entity import Sync, SyncEntity


@entity(id=0xFF)
class MockSyncEntity(SyncEntity):
    test = Sync[Optional[str]]('_test', None)


@entity(id=0xFE)
class MockSyncEntityA(MockSyncEntity):
    test_a = Sync[Optional[str]]('_test_a', None)


@entity(id=0xFD)
class MockSyncEntityB(MockSyncEntity):
    test_b = Sync[Optional[str]]('_test_b', None)
