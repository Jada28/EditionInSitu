import math
from unittest import TestCase

# Important to set the value of CLIENT here
# It needs to be define before the other imports
import eis.constants
eis.constants.CLIENT = True

from eis.client.state_machine import StateMachine
from eis.editor_support.asset_library import AssetLibrary
from eis.editor_support.cursor_manager import CursorManager
from eis.editor_support.menu_manager import MenuManager
from eis.editor_support.presentation_manager import PresentationManager
from eis.engines.panda3d.engine import Panda3DEngine
from eis.graph.camera import Camera
from eis.graph.object_3d import Object3D
from eis.graph.primitives.cylinder import Cylinder
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3


class TestClientEditor(TestCase):

    @classmethod
    def setUpClass(cls):
        basic_config = {
            "engine": {
                "type": "Panda3D"
            },
            "sound_engine": {
                "type": "Satie"
            },
            "editor": {
                "camera.model": "res/models/camera/camera.gltf",
                "switcher.jack.driver": "dummy"
            },
            "network": {},
            "network.adapter": {},
            "controls": {}
        }
        cls.machine = StateMachine(config=basic_config, config_local={})
        cls.editor = cls.machine.editor

    @classmethod
    def tearDownClass(cls):
        cls.editor = None
        cls.machine = None

    def test_local_physical_body(self):
        self.assertTrue(isinstance(TestClientEditor.editor.local_physical_body, Object3D))
        self.assertTrue(TestClientEditor.editor.local_physical_body is TestClientEditor.editor._local_physical_body)

        new_body = Cylinder(name="body", matrix=Matrix44.from_x_rotation(
            theta=-math.pi / 2), height=0.1)
        TestClientEditor.editor.local_physical_body = new_body
        self.assertEqual(TestClientEditor.editor.local_physical_body, new_body)
        self.assertTrue(TestClientEditor.editor.local_physical_body is new_body)

    def test_synchronized_physical_body(self):
        self.assertIsNone(TestClientEditor.editor.synchronized_physical_body)

        new_body = Cylinder(name="body", matrix=Matrix44.from_x_rotation(
            theta=-math.pi / 2), height=0.1)
        TestClientEditor.editor.synchronized_physical_body = new_body
        self.assertTrue(TestClientEditor.editor.synchronized_physical_body is new_body)

    def test_active_object(self):
        object3d = Object3D()
        other_object3d = Object3D()
        self.assertIsNone(self.editor.active_object)
        self.editor.active_object = object3d
        self.assertEqual(self.editor.active_object, object3d)
        self.assertIsNone(self.editor.last_active_object)
        self.editor.active_object = other_object3d
        self.assertEqual(self.editor.active_object, other_object3d)
        self.assertEqual(self.editor.last_active_object, object3d)
        self.editor.active_object = None
        self.assertIsNone(self.editor.active_object)
        self.assertEqual(self.editor.last_active_object, other_object3d)

    def test_camera(self):
        # Move the editor, we will test that the editor's location changed to the camera's location
        a_matrix = Matrix44.from_x_rotation(math.pi / 2.0) * Matrix44.from_translation([1.0, 2.0, 3.0])
        TestClientEditor.editor.matrix = a_matrix

        # The editor's scene needs to be aware of its editor
        TestClientEditor.editor.scene.editor = TestClientEditor.editor

        cam = Camera(name="a_camera")

        # Test the setter
        # Multiple configurations to test
        # 1. when Camera is None and a new_camera is added
        cam.scene = TestClientEditor.editor.scene
        TestClientEditor.editor.camera = cam
        # Test the getter
        self.assertTrue(TestClientEditor.editor.camera is TestClientEditor.editor._camera)
        self.assertTrue(isinstance(TestClientEditor.editor.camera, Object3D))
        self.assertEqual(TestClientEditor.editor.camera, cam)
        # check that the location and rotation of the editor has been changed to
        # the camera's location
        self.assertEqual(TestClientEditor.editor.location, cam.location)
        self.assertEqual(TestClientEditor.editor.rotation, cam.rotation)
        self.assertTrue(TestClientEditor.editor._dirty_matrix)

        # 2. Second case: when editor has a camera, and a new_camera is added
        new_cam = Camera(
            name="new_camera",
            matrix=Matrix44.from_x_rotation(math.pi / 3.0) * Matrix44.from_translation([4.0, 5.0, 6.0])
        )
        new_cam.scene = TestClientEditor.editor.scene
        TestClientEditor.editor.camera = new_cam
        self.assertEqual(TestClientEditor.editor.camera, new_cam)
        # check that the location and rotation of the editor has been changed to
        # the new camera's location
        self.assertEqual(TestClientEditor.editor.location, new_cam.location)
        self.assertEqual(TestClientEditor.editor.rotation, new_cam.rotation)

        # 3. Third case: when editor has a camera, and it is being detach (new_camera is None)
        TestClientEditor.editor.camera = None
        self.assertEqual(TestClientEditor.editor.camera, None)

        # In this case, the location of the camera remains the same
        self.assertEqual(TestClientEditor.editor.location, new_cam.location)
        self.assertEqual(TestClientEditor.editor.rotation, new_cam.rotation)

    def test_machine(self):
        self.assertTrue(TestClientEditor.editor.machine is TestClientEditor.editor._machine)

    def test_cursor_manager(self):
        self.assertTrue(isinstance(TestClientEditor.editor.cursor_manager, CursorManager))
        self.assertTrue(TestClientEditor.editor.cursor_manager is TestClientEditor.editor._cursor_manager)

    def test_menu_manager(self):
        self.assertTrue(isinstance(TestClientEditor.editor.menu_manager, MenuManager))
        self.assertTrue(TestClientEditor.editor.menu_manager is TestClientEditor.editor._menu_manager)

    def test_asset_library(self):
        self.assertTrue(isinstance(TestClientEditor.editor.asset_library, AssetLibrary))
        self.assertTrue(TestClientEditor.editor.asset_library is TestClientEditor.editor._asset_library)

    def test_presentation_manager(self):
        self.assertTrue(isinstance(TestClientEditor.editor.presentation_manager, PresentationManager))
        self.assertTrue(TestClientEditor.editor.presentation_manager is TestClientEditor.editor._presentation_manager)

    def test_asset_layer(self):
        self.assertTrue(isinstance(TestClientEditor.editor.asset_layer, Object3D))
        self.assertTrue(TestClientEditor.editor.asset_layer is TestClientEditor.editor._asset_layer)

    def test_helpers(self):
        self.assertTrue(isinstance(TestClientEditor.editor.helpers_layer, Object3D))
        self.assertTrue(TestClientEditor.editor.helpers_layer is TestClientEditor.editor._helpers_layer)

    def test_gui(self):
        self.assertTrue(isinstance(TestClientEditor.editor.gui_layer, Object3D))
        self.assertTrue(TestClientEditor.editor.gui_layer is TestClientEditor.editor._gui_layer)

    def test_physical_offset(self):
        self.assertTrue(TestClientEditor.editor.physical_offset is not TestClientEditor.editor._physical_offset)

        TestClientEditor.editor._physical_offset = Vector3([1.0, 2.0, 3.0])
        self.assertEqual(TestClientEditor.editor.physical_offset, Vector3([1.0, 2.0, 3.0]))

    def test_location(self):
        self.assertTrue(TestClientEditor.editor.location is not TestClientEditor.editor._location)

        a_location = Vector3([1.0, 2.0, 3.0])
        TestClientEditor.editor.location = a_location
        self.assertEqual(TestClientEditor.editor.location, a_location)
        self.assertTrue(TestClientEditor.editor._location is not a_location)
        self.assertTrue(TestClientEditor.editor._dirty_matrix)

        TestClientEditor.editor._dirty_matrix = False
        TestClientEditor.editor.location = a_location
        self.assertFalse(TestClientEditor.editor._dirty_matrix)

        TestClientEditor.editor._dirty_matrix = True
        TestClientEditor.editor.step(now=0.0, dt=0.0)

    def test_rotation(self):
        self.assertTrue(TestClientEditor.editor.rotation is not TestClientEditor.editor._rotation)

        a_rotation = Quaternion.from_x_rotation(math.pi)
        TestClientEditor.editor.rotation = a_rotation
        self.assertEqual(TestClientEditor.editor.rotation, a_rotation)
        self.assertTrue(TestClientEditor.editor._rotation is not a_rotation)
        self.assertTrue(TestClientEditor.editor._dirty_matrix)

        TestClientEditor.editor._dirty_matrix = False
        TestClientEditor.editor.rotation = a_rotation
        self.assertFalse(TestClientEditor.editor._dirty_matrix)

    def test_matrix(self):
        self.assertTrue(TestClientEditor.editor.matrix is not TestClientEditor.editor._matrix)

        a_matrix = Matrix44.from_x_rotation(math.pi / 2.0) * Matrix44.from_translation([1.0, 2.0, 3.0])
        TestClientEditor.editor.matrix = a_matrix
        self.assertTrue(TestClientEditor.editor._matrix is not a_matrix)
        self.assertEqual(TestClientEditor.editor.matrix, a_matrix)

        self.assertFalse(TestClientEditor.editor._dirty_matrix)
        self.assertTrue(TestClientEditor.editor._tilted_matrix)
        self.assertEqual(TestClientEditor.editor.location, a_matrix.translation)
        self.assertEqual(TestClientEditor.editor.rotation, Quaternion.from_x_rotation(math.pi / 2.0))

    def test_rotation_offset(self):
        self.assertTrue(TestClientEditor.editor.rotation_offset is not TestClientEditor.editor._rotation_offset)

        a_rotation = Quaternion.from_x_rotation(math.pi)
        TestClientEditor.editor.rotation_offset = a_rotation
        self.assertEqual(TestClientEditor.editor.rotation_offset, a_rotation)
        self.assertTrue(TestClientEditor.editor._rotation_offset is not a_rotation)

        self.assertTrue(isinstance(TestClientEditor.editor.rotation_offset, Quaternion))
