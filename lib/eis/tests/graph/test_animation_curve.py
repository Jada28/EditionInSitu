import sys
from unittest import TestCase

from eis.graph.animation_curve import AnimationCurve


class TestAnimationCurve(TestCase):

    def setUp(self):
        self.curve = AnimationCurve()
        self.curve.add_keyframe(0.0, 0.0)
        self.curve.add_keyframe(1.0, 1.0)
        self.curve.add_keyframe(2.0, 2.0)

    def test_add_keyframe(self):
        self.assertEqual(self.curve.keyframes, [(0.0, 0.0), (1.0, 1.0), (2.0, 2.0)])
        # try adding a keyframe with the same time than another keyframe
        self.curve.add_keyframe(0.0, 10.0)
        self.assertNotIn((0.0, 0.0), self.curve.keyframes)
        self.assertIn((0.0, 10.0), self.curve.keyframes)

    def test_get_value_at(self):
        self.assertEqual(self.curve.get_value_at(0.5), 0.0)
        self.assertEqual(self.curve.get_value_at(1.5), 1.0)
        self.assertEqual(self.curve.get_value_at(2.5), 2.0)

        self.curve.interpolation = AnimationCurve.InterpolationType.LINEAR
        self.assertEqual(self.curve.get_value_at(0.0), 0.0)
        self.assertEqual(self.curve.get_value_at(0.5), 0.5)
        self.assertEqual(self.curve.get_value_at(1.0), 1.0)
        self.assertEqual(self.curve.get_value_at(1.5), 1.5)
        self.assertEqual(self.curve.get_value_at(2.0), 2.0)
        self.assertEqual(self.curve.get_value_at(2.5), 2.0)

        self.curve.interpolation = AnimationCurve.InterpolationType.SMOOTH
        self.assertTrue(abs(0.0 - self.curve.get_value_at(0.0)) < sys.float_info.epsilon)
        self.assertTrue(abs(0.5 - self.curve.get_value_at(0.5)) < sys.float_info.epsilon)
        self.assertTrue(abs(1.0 - self.curve.get_value_at(1.0)) < sys.float_info.epsilon)
        self.assertTrue(abs(1.5 - self.curve.get_value_at(1.5)) < sys.float_info.epsilon)
        self.assertTrue(abs(2.0 - self.curve.get_value_at(2.0)) < sys.float_info.epsilon)

    def test_get_keyframe_at(self):
        self.assertEqual((1.0, 1.0), self.curve.get_keyframe_at(1.0))
        self.assertEqual(None, self.curve.get_keyframe_at(1.5))

    def test_get_enclosing_keyframes(self):
        self.assertEqual(self.curve.get_enclosing_keyframes(1.3), ((1.0, 1.0), (2.0, 2.0)))
        self.assertEqual(self.curve.get_enclosing_keyframes(2.3), ((2.0, 2.0), (None)))
        self.assertEqual(self.curve.get_enclosing_keyframes(0.0), ((None), (1.0, 1.0)))
        self.assertEqual(self.curve.get_enclosing_keyframes(2.0), ((1.0, 1.0), (None)))

    def test_remove_keyframe(self):
        self.assertTrue(self.curve.remove_keyframe(1.0))
        self.assertNotIn((1.0, 1.0), self.curve.keyframes)

    def test_copy_shared(self):
        original = AnimationCurve(
                interpolation=AnimationCurve.InterpolationType.LINEAR,
                keyframes=[(0.0, "blah"), (0.5, 8.0)]
                )

        copy = original.copy_shared()

        self.assertEqual(original.interpolation, copy.interpolation)
        self.assertEqual(original.keyframes, copy.keyframes)

