from unittest import TestCase, mock

from eis.graph.base import GraphBase, UniqueGraphEntity, SharedGraphEntity
from eis.graph.model import Model


class DummyGraphEntity(GraphBase):
    def _copy(self):
        pass

    def _copy_shared(self):
        pass


class DummyUniqueEntity(UniqueGraphEntity):
    def _copy(self):
        pass

    def _copy_shared(self):
        pass


class DummySharedEntity(SharedGraphEntity):
    def _copy(self):
        pass

    def _copy_shared(self):
        pass


class TestGraphEntity(TestCase):
    def test_managed(self):
        entity = DummyGraphEntity()
        self.assertFalse(entity.managed)
        self.assertTrue(entity._serialize)
        entity.managed = True
        self.assertTrue(entity.managed)
        self.assertFalse(entity._serialize)


class TestUniqueGraphEntity(TestCase):
    def test_setting_model(self):
        model = Model()
        uniq = DummyUniqueEntity()
        with mock.patch.object(uniq, "added_to_model") as added, \
                mock.patch.object(uniq, "removed_from_model") as removed:
            uniq.set_model(model)
            added.assert_called_once_with(False)
            removed.assert_not_called()

    def test_switching_model(self):
        model1 = Model()
        model2 = Model()
        uniq = DummyUniqueEntity()
        uniq.set_model(model1)
        with mock.patch.object(uniq, "added_to_model") as added, \
                mock.patch.object(uniq, "removed_from_model") as removed:
            uniq.set_model(model2)
            added.assert_called_once_with(False)
            removed.assert_called_once_with(False)

    def test_removing_model(self):
        model1 = Model()
        uniq = DummyUniqueEntity()
        uniq.set_model(model1)
        with mock.patch.object(uniq, "added_to_model") as added, \
                mock.patch.object(uniq, "removed_from_model") as removed:
            uniq.set_model(None)
            added.assert_not_called()
            removed.assert_called_once_with(False)


class TestSharedGraphEntity(TestCase):
    def test_owners_not_static(self):
        # This is a stupid check because of the way the owners are initialized
        # An unfortunate coder could easily break it
        model1 = Model()
        shared1 = DummySharedEntity()
        shared1.add_owner(model1)

        model2 = Model()
        shared2 = DummySharedEntity()
        shared2.add_owner(model2)

        self.assertNotEqual(shared1.owners, shared2.owners)
        self.assertEqual(shared1.owners, {model1: 1})
        self.assertEqual(shared2.owners, {model2: 1})

    def test_owner_count(self):
        model = Model()
        shared = DummySharedEntity()
        shared.add_owner(model)
        self.assertIsNotNone(shared.owners.get(model))
        self.assertEqual(shared.owners[model], 1)
        shared.add_owner(model)
        self.assertIsNotNone(shared.owners.get(model))
        self.assertEqual(shared.owners[model], 2)
        removed = shared.remove_owner(model)
        self.assertFalse(removed)
        self.assertIsNotNone(shared.owners.get(model))
        self.assertEqual(shared.owners[model], 1)
        removed = shared.remove_owner(model)
        self.assertTrue(removed)
        self.assertIsNone(shared.owners.get(model))
