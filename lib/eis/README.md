# EIS Shared Library

## Rationale
EIS is the shared library for code pertaining to the EIS editor and engine.


## Type Hinting and Generics
Since this library heavily relies on generics, it uses type hinting in order to do static type checking with [Mypy](http://mypy-lang.org/). Otherwise it would be pretty difficult to detect bugs in the usage of generics.


## SATNet Implementations
Most of the abstract and generic classes from SATNet are extended in EIS in order to provide the right type parameters once and simplify the usage of these classes without having to specify the types in every subclass.

Such implementations include:

* `eis.client.client.EISClient`
* `eis.client.session.EISLocalSession`
* `eis.server.server.EISServer`
* `eis.server.session.EISRemoteSession`
* `eis.session.EISSession`
* `eis.action.EISAction`
* `eis.command.EISCommand`
* `eis.notification.EISNotification`
* `eis.request.EISRequest`

When working on EIS, you should extend these instead of their SATNet counterparts.

## Entities
SATNet has the concept of an `Entity`, which is a `Serializable` with a `UUID`. In EIS we extend the the `Entity` to create the `SyncEntity`. The `SyncEntity` add support for `Sync` fields that are similar to `@property` properties but also trigger a callback when the value changes.

### Usage Sample

```python
from eis.entity import Sync, SyncEntity

class Book(SyncEntity):
    title = Sync[str]('_title', "Untitled")
```

`Sync`'s are generic and take the type of the value they hold as a type parameter. A `Sync` is initialized by passing the name of the private field that will hold the value and the default value. A third optional argument is `on_changed` which can be a callback for when the value changes. Its signature is the following

```python
Callable[[SyncEntity, Sync[T], T, T], None]
```

Once a `Sync` is initialized, it's getter and setter can be accessed through `self.title` (in our example above). The private field holding the value is `self._title`.

### Syncing Values
The `SyncEntity`s have a property `on_sync_changed` that can be used to attach a callback with the signature:

```python
Callable[[SyncEntity, Sync[T], T, T], None]
```

that is triggered when the value of a `Sync` changes. This is used by `Model`s that in turn notify their delegates and then the delegate, if needed, sends a `PropertyChangedCommand` to the other parties.

## Graph
All the scene graph entities share the same base `GraphBase` which in turn extends `SyncEntity`. There are then four ways to create a new graph entity type;

* [weird case, fix] If the entity is not to be represented by a `Proxy` in the `Engine`, just extend `SyncEntity`. Example: `Behavior`
* If the entity is owned by it's parent (ie: a `Geometry` is owned by its parent `Mesh`), and will never have to "travel" alone (ie: we add meshes containing geometries to objects, but never geometries to meshes - across the network that is) then extend `GraphBase`.
* If the entity is "standalone" as in: it appears only once in the scene graph, like an `Object3D`, it should extend `UniqueGraphEntity`.
* If the entity can be shared, like a `Material` being applied to multiple `Object3D`, it should extend `SharedGraphEntity`.

### Model
The `Model` should be seen as the container, the repository, containing every dependency of a graph. The `Scene` has a model, files are loaded as models, and when sending a branch of the scene graph through the network, it is packaged inside a `Model`.

When adding an object to the scene graph (through an `Object3D`'s `add_child` method for example), a series of "events" happen that result in all the dependencies of that graph branch being added and/or shared in the model.

#### Addition and Removal Chain of events
Addition and removal flow are pretty similar with equvalent add/remove methods being called. So only the addition flow is described here.

##### `UniqueGraphEntity`
With a child `Object3D` `leaf` being added to a parent `branch`, only considering the EIS graph and ignoring the proxies for now.

1. `branch.add_child(leaf)`
*  In `add_child()`, `leaf` gets it's `parent` property set to `branch`.
*  In `leaf.parent` setter, `leaf.added_to_parent()` will be called.
*  In `leaf.added_to_parent()`, the parent's `model` is retrieved. If the parent has a model, because it is "rooted", then `set_model` is called on the leaf with the model.
*  In `leaf.set_model()`, depending on the entity type, a call to `set_model()` of all the entities owned by `leaf` is made to propagate the model through the children.
*  Then `super().set_model()` is called on the `Object3D`'s parent class `UniqueGraphEntity`.
*  In `UniqueGraphEntity`, this triggers `self.added_to_model()`, which is the abstract method that graph entities should override in order to react to their addition into a rooted tree.
*  In the case of `Object3D`, `added_to_model()` calls `model.add_object()` to notify the model that the object is now under its "control"
*  In the `Model`, `add_object()` registers the object in various maps and calls `on_object_added()` on its delegate, if it has one.
*  The active scene's model, passes the client or server as a delegate. The delegate code on the server's side is tasked with sending a `Command` to notify all clients that a object was added to the model.

##### `SharedGraphEntity`
With a `Material` `brick` being added to an `Object3D` `object`, only considering the EIS graph and ignoring the proxies for now.

1. `object.material = brick`
*  If the object is "rooted" (part of a model), the object calls `material.added_to_model()`
* In the `Material`, `added_to_model()` calls `model.add_material()`
* In the `Model`, `add_material` calls the material's `add_owner()` method in order to add the model to the list of owners of the material (since it can be shared across models). It is done here instead of in the materials's `added_to_model()` method because a material can be added directly to a model using `add_material()` and the owners still need to be counted.
* Since the material is shared, the reference kept in the object is not serialized, only its UUID will be serialized.


## Configuration
Configuration files are located in the `config` directory. A set of configuration files are loaded in layers to make the final application configuration.

* `eis.json` is loaded first and contains values shared by both the server and client.
* `server.json` or `client.json` is then loaded on top of `eis.json` depending on the application being used.
* Then if a config file is passed in the command line it is loaded.
* `local.json` is loaded on top of everything to provide local overrides and saved values (`local.json` is not shared in the repository).


## `eis.application.EISApplication`
This is a base for creating EIS applications, it imports all the serializable classes that might be needed (actions, commands, notifications and requests), includes the base for implementing/running a REPL and reading config files. You should extend this in your main class in order to create either a client or server application.


## `eis.editor.Editor`

### Description
The editor is the entry point for the scene manipulations. The various network messages should never directly access the 3d engine, instead they should call methods on the editor, which is the only class allowed to use the engine. This creates a clear separation between network messages (calling the editor), the editor (calling the engine), and the corresponding notifications to sync the state to all clients (editor delegate calling network/session methods).

The `Editor` owns the current `Scene`, the `User`s and references to the configuration and engine.

### Usage
An editor is created using this initializer:

```python
def __init__(
    self,
    engine: Engine,
    delegate: Optional[EditorDelegate] = None,
    config: Optional[Dict[str, Any]] = None
):
```

Here is a quick explanation of the different arguments:

|Argument|Type|Description|
|--------|----|-----------|
|engine|`Engine`|Concrete implementation of an `Engine` that is to be used in order to compute/render the scene graph.|
|delegate|`Optional[EditorDelegate]`|Concrete implementation of a `EditorDelegate` that will receive callbacks from the editor, use this to communicate with the network in response to editor events, for example.|
|config|`Optional[Dict[str, Any]]`|Relevant section of a `Dict[str, Any]` configuration|

And an example editor being created:

```python
from eis.engines.render_pipeline.engine import RenderPipelineEngine
from eis.editor import Editor

engine = RenderPipelineEngine()
editor = Editor(engine=engine)
```

In reality two types of editors are already implemented: `ClientEditor` and `ServerEditor`. They should be used in the right context instead of directly using `Editor`. They currently contain any overrides and specific code for the client and server.

### Control Scheme
An `Editor` has many `User`s each having their own `InputMethod`s. For example, there is a default user with a mouse and keyboard input method (provided by the RenderPipeline engine). Adding a **Vive** kit to the user is just another `InputMethod`. If we were to use two **Vive** kits, we would need to add a second user with its own **Vive** `InputMethod`.

On the server side, every session has it's own list of remote users and remote input methods that are replicated to the other clients as `PeerUser`s having `PeerInputMethod`s. This way, all client have access to a subset of the information about users and their input methods in order to draw avatars and pointing devices in 3D.


#### `eis.user.EISUser`
Base class for EIS users (implemented as `LocalEISUser` ans `RemoteEISUser` on the client and server respectively). Since it was decided that we wanted to support multiple users per machine, sessions and users were introduced. A session has one or more users, each having their own input methods for controlling the editor.

##### `LocalEISUser`
The local user is used mostly for mapping input methods to rx streams for various application events like moving, selecting, contextual menu, etc.

##### `RemoteEISUser`
_Not much going on here for the moment._


#### `eis.input.InputMethod`
Abstract base class for implementing input methods. Just like with the `EISUser`, input methods both have a `LocalInputMethod` and a `RemoteInputMethod`, since some input method properties can be shared on the network (the position and orientation of Vive trackers for example).

##### `LocalInputMethod`
On the client side, each `LocalInputMethod` owns a picker, for selecting objects in 3D space.

##### `RemoteInputMethod`
_Not much going on here for the moment._


#### `eis.engine.Engine`
Abstract base class for implementing engines. Anything that need to be manipulated in the engine should go through the base methods defined here and never directly to concrete engine implementations. This allows for engines to be implemented and changed as long as they follow the `Engine` protocol/API.

## Picking
Picking in the scene is done through the `Picker` base class, which has to be extended by an engine-specific class (see `RenderPipelinePicker`). The picker does not pick automatically and one of its `pick` methods has to be called for it to happen: the reason for this is that scene traversal can be hungry.

Also, by default the picker will only return `Object3Ds` which satisfy the `object.layer == 0` condition. For the picker to search in another layer, its `layer` attribute has to be set to the corresponding layer: `picker.layer = 1`. Some picking method does not always consider the `layer` attribute of the objects: this is the case for `pick_in`, `pick_object` and `pick_gui`.

### Proxies
In order for the graph entities to be rendered by a specific `Engine` implementation, a `Proxy` must be registered for the specific entity.

#### Declaring a Proxy
The `@proxy` decorator registers the class as a proxy for `Material` entities in the `RenderPipelineEngine` implementation. For every graph entity that can have a proxy, a proxy base class exists that should be inherited from, in this case `MaterialProxy`. Also, the engine can provide it's own version of a base proxy class. `RenderPipelineGraphProxyBase` in the case of the `RenderPipelineEngine`, takes a type argument that specifies the type of panda node that the proxy creates, this is all engine implementation dependant.

```python
from eis.engine import proxy
from eis.engines.render_pipeline.engine import RenderPipelineEngine
from eis.engines.render_pipeline.proxies.base import RenderPipelineGraphProxyBase
from eis.graph.material import Material, MaterialProxy

@proxy(RenderPipelineEngine, Material)
class RenderPipelineMaterialProxy(MaterialProxy, RenderPipelineGraphProxyBase[core.Material]):
```

#### Ensuring the engine's graph is in sync with EiS graph
The structure of the engine's scene graph can be different than that of EiS. This may imply that updating the proxy of an EiS object is not sufficient for the engine's scene graph to be up to date. To handle these issues, one can extend the method `update_graph` of the base class `GraphProxyBase`. This method is called whenever the EiS scene graph is modified. An example can be found in the `RenderPipelineGeometryProxy` class.

### `eis.engines.render_pipeline.engine.RenderPipelineEngine`
Engine based on the Panda3D RenderPipeline 3D engine.

### `eis.engines.panda3d.engine.Panda3DEngine`
Engine based on vanilla Panda3D.

### `eis.engines.dummy.engine.DummyEngine`
Dummy engine which does nothing.
