from abc import abstractmethod
from typing import TypeVar, Generic

T = TypeVar('T')


class Meta(type):
    """
    Meta class that allows us to call `initialize()` after the `__init__()` has been called.
    This allows us to initialize the class from the deserialized values if need be.
    """

    def __call__(cls, *args, **kwargs):
        if args or kwargs:
            print(args, kwargs)
        obj = type.__call__(cls, *args, **kwargs)
        obj.initialize()
        return obj


class Base(metaclass=Meta):
    def initialize(self):
        print("initialize")

    @abstractmethod
    def allo(self):
        pass


class Intermediate(Base):
    pass


class Final(Generic[T], Intermediate):
    def __init__(self, pouet=None):
        print("ALLO", pouet)

    def allo(self):
        pass


print("---F1---")
f1 = Final()
print(f1)
print("---F2---")
f2 = Final(pouet="poupou")
print(f2)
print("---f3---")
f3 = Final.__new__(Final)
print(f3)
