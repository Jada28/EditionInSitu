#!/usr/bin/env bash

set -e

source_directory="$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)"
SUDO=''

check_sudo()
{
    if ((${EUID} != 0)); then
        SUDO='sudo'
    fi
}

install_system_packages()
{
    echo Installing EiS system dependencies

    ${SUDO} apt -y install git git-lfs cmake python3 python3-pip python3-setuptools python3-venv python3-pyqt5 libsndfile1-dev
}

install_python_packages()
{
    echo Installing EiS Python dependencies

    pip install wheel
    pip install -r requirements.txt
}

update_submodules()
{
    echo Fetching submodules and LFS

    # Make sure git lfs is up to date
    git lfs fetch && git lfs pull
    git submodule sync --recursive
    git submodule update --init --recursive

    # Required to use gltf addon outside of blender.
    file=../extlib/glTF-Blender-IO/addons/io_scene_gltf2/__init__.py
    if [ -f "$file" ]; then
      rm -f $file
    fi
}

setup_render_pipeline()
{
    echo Setting up render pipeline

    pushd extlib/render_pipeline
    if [[ -z ${DISPLAY} ]]; then
        yes | python3 setup.py --ci-build
    else
        yes | python3 setup.py
    fi
    popd
}

setup_satie()
{
    echo Setting up PySATIE

    pushd extlib/PySATIE
    ${SUDO} apt install -y liblo-dev
    pip3 install -e .
    popd
}

install_shmdata()
{
    echo Installing Shmdata

    pushd extlib
    if [ ! -d 'shmdata' ]; then git clone https://gitlab.com/sat-metalab/shmdata; fi
    cd shmdata
    git checkout master && git pull origin master
    ${SUDO} apt -y -qq install cmake build-essential
    ${SUDO} apt -y -qq install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev python3-dev
    mkdir -p build && cd build && rm -rf *
    cmake ..
    make -j$(nproc) && ${SUDO} make install
    ${SUDO} ldconfig
    popd
}

install_switcher()
{
    echo Installing Switcher

    pushd extlib
    if [ ! -d 'switcher' ]; then git clone --recursive https://gitlab.com/sat-metalab/switcher; fi
    cd switcher
    git checkout master && git pull origin master
    ${SUDO} apt install -y -qq gcc-8 g++-8 cmake build-essential libglib2.0-dev libgstreamer1.0-dev \
        libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev libjson-glib-dev libcgsi-gsoap-dev \
        gstreamer1.0-libav gstreamer1.0-plugins-bad gstreamer1.0-plugins-base gstreamer1.0-plugins-good \
        gstreamer1.0-plugins-ugly liblo-dev linux-libc-dev libpulse-dev libportmidi-dev libvncserver-dev \
        uuid-dev libssl-dev swh-plugins libgl1-mesa-dev libglu1-mesa-dev freeglut3-dev mesa-common-dev \
        libltc-dev libcurl4-gnutls-dev gsoap wah-plugins libxrandr-dev libxinerama-dev \
        libxcursor-dev libsamplerate0-dev libjack-jackd2-dev libxxf86vm-dev libsoup2.4-dev
    ${SUDO} apt-get install --yes --quiet --no-install-recommends jackd2 libxi-dev

    mkdir -p build && cd build && rm -rf *
    CC=gcc-8 CXX=g++-8 cmake -DPLUGIN_GSOAP=OFF -DENABLE_GPL=ON ..
    make -j$(nproc) && ${SUDO} make install
    ${SUDO} ldconfig
    popd
}

replace_default_cubemap()
{
    if [ -n "$DISPLAY" ]; then
        # Replace the default cubemap
        pushd res/cubemap
        python3 filter.py
        cp cubemap.txo.pz "${source_directory}"/extlib/render_pipeline/data/default_cubemap
        popd
    fi
}

copy_renderpipeline_config()
{
    # Copy RenderPipeline configuration
    cp res/rp/* extlib/render_pipeline/config/
}

setup_virtual_env()
{
    echo Create and activate the Python virtual environment

    if [ ! -d env ]; then
        python3 -m venv env
    fi
    source ./env/bin/activate
}

exit_virtual_env()
{
    echo Deactivate the Python virtual environment

    deactivate
}

remove_gltf_init()
{
  ./remove_gltf_init.sh
}

check_sudo
install_system_packages
setup_virtual_env
install_python_packages
update_submodules
install_shmdata
install_switcher
setup_render_pipeline
setup_satie
replace_default_cubemap
copy_renderpipeline_config
remove_gltf_init
exit_virtual_env
